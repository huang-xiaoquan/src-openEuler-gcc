From 41a4945886631a1b2898ae957389d5db18a07141 Mon Sep 17 00:00:00 2001
From: Xi Ruoyao <xry111@xry111.site>
Date: Fri, 4 Nov 2022 15:12:22 +0800
Subject: [PATCH 025/124] LoongArch: Add fcopysign instructions

Add fcopysign.{s,d} with the names copysign{sf,df}3 so GCC will expand
__builtin_copysign{f,} to a single instruction.

Link: https://sourceware.org/pipermail/libc-alpha/2022-November/143177.html

gcc/ChangeLog:

	* config/loongarch/loongarch.md (UNSPEC_FCOPYSIGN): New unspec.
	(type): Add fcopysign.
	(copysign<mode>3): New instruction template.

gcc/testsuite/ChangeLog:

	* gcc.target/loongarch/fcopysign.c: New test.

Signed-off-by: Peng Fan <fanpeng@loongson.cn>
Signed-off-by: ticat_fp <fanpeng@loongson.cn>
---
 gcc/config/loongarch/loongarch.md             | 22 ++++++++++++++++++-
 .../gcc.target/loongarch/fcopysign.c          | 16 ++++++++++++++
 2 files changed, 37 insertions(+), 1 deletion(-)
 create mode 100644 gcc/testsuite/gcc.target/loongarch/fcopysign.c

diff --git a/gcc/config/loongarch/loongarch.md b/gcc/config/loongarch/loongarch.md
index 214b14bdd..bda34d0f3 100644
--- a/gcc/config/loongarch/loongarch.md
+++ b/gcc/config/loongarch/loongarch.md
@@ -37,6 +37,7 @@
   UNSPEC_FCLASS
   UNSPEC_FMAX
   UNSPEC_FMIN
+  UNSPEC_FCOPYSIGN
 
   ;; Override return address for exception handling.
   UNSPEC_EH_RETURN
@@ -214,6 +215,7 @@
 ;; fabs		floating point absolute value
 ;; fneg		floating point negation
 ;; fcmp		floating point compare
+;; fcopysign	floating point copysign
 ;; fcvt		floating point convert
 ;; fsqrt	floating point square root
 ;; frsqrt       floating point reciprocal square root
@@ -226,7 +228,7 @@
   "unknown,branch,jump,call,load,fpload,fpidxload,store,fpstore,fpidxstore,
    prefetch,prefetchx,condmove,mgtf,mftg,const,arith,logical,
    shift,slt,signext,clz,trap,imul,idiv,move,
-   fmove,fadd,fmul,fmadd,fdiv,frdiv,fabs,fneg,fcmp,fcvt,fsqrt,
+   fmove,fadd,fmul,fmadd,fdiv,frdiv,fabs,fneg,fcmp,fcopysign,fcvt,fsqrt,
    frsqrt,accext,accmod,multi,atomic,syncloop,nop,ghost"
   (cond [(eq_attr "jirl" "!unset") (const_string "call")
 	 (eq_attr "got" "load") (const_string "load")
@@ -976,6 +978,24 @@
    (set_attr "mode" "<UNITMODE>")])
 
 ;;
+;;  ....................
+;;
+;;	FLOATING POINT COPYSIGN
+;;
+;;  ....................
+
+(define_insn "copysign<mode>3"
+  [(set (match_operand:ANYF 0 "register_operand" "=f")
+	(unspec:ANYF [(match_operand:ANYF 1 "register_operand" "f")
+		      (match_operand:ANYF 2 "register_operand" "f")]
+		     UNSPEC_FCOPYSIGN))]
+  "TARGET_HARD_FLOAT"
+  "fcopysign.<fmt>\t%0,%1,%2"
+  [(set_attr "type" "fcopysign")
+   (set_attr "mode" "<UNITMODE>")])
+
+
+;;
 ;;  ...................
 ;;
 ;;  Count leading zeroes.
diff --git a/gcc/testsuite/gcc.target/loongarch/fcopysign.c b/gcc/testsuite/gcc.target/loongarch/fcopysign.c
new file mode 100644
index 000000000..058ba2cf5
--- /dev/null
+++ b/gcc/testsuite/gcc.target/loongarch/fcopysign.c
@@ -0,0 +1,16 @@
+/* { dg-do compile } */
+/* { dg-options "-mdouble-float" } */
+/* { dg-final { scan-assembler "fcopysign\\.s" } } */
+/* { dg-final { scan-assembler "fcopysign\\.d" } } */
+
+double
+my_copysign (double a, double b)
+{
+  return __builtin_copysign (a, b);
+}
+
+float
+my_copysignf (float a, float b)
+{
+  return __builtin_copysignf (a, b);
+}
-- 
2.33.0

