From c0aaf329d9c547b249ac120a8d1995d8546a1edb Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Sat, 11 Nov 2023 17:28:57 +0000
Subject: [PATCH 038/157] [Backport][SME] mode-switching: Tweak entry/exit
 handling

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=e59ec35276599805cdc6c3979d8a167b027d286e

An entity isn't transparent in a block that requires a specific mode.
optimize_mode_switching took that into account for normal insns,
but didn't for the exit block.  Later patches misbehaved because
of this.

In contrast, an entity was correctly marked as non-transparent
in the entry block, but the reasoning seemed a bit convoluted.
It also referred to a function that no longer exists.
Since KILL = ~TRANSP, the entity is by definition not transparent
in a block that defines the entity, so I think we can make it so
without comment.

Finally, the exit handling was nested in the entry handling,
but that doesn't seem necessary.  A target could say that an
entity is undefined on entry but must be defined on return,
on a "be liberal in what you accept, be conservative in what
you do" principle.

gcc/
	* mode-switching.cc (optimize_mode_switching): Mark the exit
	block as nontransparent if it requires a specific mode.
	Handle the entry and exit mode as sibling rather than nested
	concepts.  Remove outdated comment.
---
 gcc/mode-switching.cc | 34 +++++++++++++++-------------------
 1 file changed, 15 insertions(+), 19 deletions(-)

diff --git a/gcc/mode-switching.cc b/gcc/mode-switching.cc
index 4d2b9e284..4761c2ff0 100644
--- a/gcc/mode-switching.cc
+++ b/gcc/mode-switching.cc
@@ -649,34 +649,30 @@ optimize_mode_switching (void)
 	}
       if (targetm.mode_switching.entry && targetm.mode_switching.exit)
 	{
-	  int mode = targetm.mode_switching.entry (e);
-
 	  info[post_entry->index].mode_out =
 	    info[post_entry->index].mode_in = no_mode;
-	  if (pre_exit)
-	    {
-	      info[pre_exit->index].mode_out =
-		info[pre_exit->index].mode_in = no_mode;
-	    }
 
+	  int mode = targetm.mode_switching.entry (e);
 	  if (mode != no_mode)
 	    {
-	      bb = post_entry;
-
-	      /* By always making this nontransparent, we save
-		 an extra check in make_preds_opaque.  We also
-		 need this to avoid confusing pre_edge_lcm when
-		 antic is cleared but transp and comp are set.  */
-	      bitmap_clear_bit (transp_all, bb->index);
-
 	      /* Insert a fake computing definition of MODE into entry
 		 blocks which compute no mode. This represents the mode on
 		 entry.  */
-	      info[bb->index].computing = mode;
+	      info[post_entry->index].computing = mode;
+	      bitmap_clear_bit (transp_all, post_entry->index);
+	    }
 
-	      if (pre_exit)
-		info[pre_exit->index].seginfo->mode =
-		  targetm.mode_switching.exit (e);
+	  if (pre_exit)
+	    {
+	      info[pre_exit->index].mode_out =
+		info[pre_exit->index].mode_in = no_mode;
+
+	      int mode = targetm.mode_switching.exit (e);
+	      if (mode != no_mode)
+		{
+		  info[pre_exit->index].seginfo->mode = mode;
+		  bitmap_clear_bit (transp_all, pre_exit->index);
+		}
 	    }
 	}
 
-- 
2.33.0

