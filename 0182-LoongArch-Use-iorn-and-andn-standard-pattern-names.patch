From 64560e75b4d020b6c47e07592595ceed663541af Mon Sep 17 00:00:00 2001
From: Lulu Cheng <chenglulu@loongson.cn>
Date: Thu, 1 Aug 2024 16:07:25 +0800
Subject: [PATCH 182/188] LoongArch: Use iorn and andn standard pattern names.

R15-1890 introduced new optabs iorc and andc, and its corresponding
internal functions BIT_{ANDC,IORC}, and if targets defines such optabs
for vector modes.  And in r15-2258 the iorc and andc were renamed to
iorn and andn.
So we changed the andn and iorn implementation templates to the standard
template names.

gcc/ChangeLog:

	* config/loongarch/lasx.md (xvandn<mode>3): Rename to ...
	(andn<mode>3): This.
	(xvorn<mode>3): Rename to ...
	(iorn<mode>3): This.
	* config/loongarch/loongarch-builtins.cc
	(CODE_FOR_lsx_vandn_v): Defined as the modified name.
	(CODE_FOR_lsx_vorn_v): Likewise.
	(CODE_FOR_lasx_xvandn_v): Likewise.
	(CODE_FOR_lasx_xvorn_v): Likewise.
	(loongarch_expand_builtin_insn): When the builtin function to be
	called is __builtin_lasx_xvandn or __builtin_lsx_vandn, swap the
	two operands.
	* config/loongarch/loongarch.md (<optab>n<mode>): Rename to ...
	(<optab>n<mode>3): This.
	* config/loongarch/lsx.md (vandn<mode>3): Rename to ...
	(andn<mode>3): This.
	(vorn<mode>3): Rename to ...
	(iorn<mode>3): This.

gcc/testsuite/ChangeLog:

	* gcc.target/loongarch/lasx-andn-iorn.c: New test.
	* gcc.target/loongarch/lsx-andn-iorn.c: New test.
---
 gcc/config/loongarch/lasx.md                  | 10 +++----
 gcc/config/loongarch/loongarch-builtins.cc    | 10 ++++---
 gcc/config/loongarch/loongarch.md             |  8 +++---
 gcc/config/loongarch/lsx.md                   | 10 +++----
 .../gcc.target/loongarch/lasx-andn-iorn.c     | 11 ++++++++
 .../gcc.target/loongarch/lsx-andn-iorn.c      | 28 +++++++++++++++++++
 6 files changed, 59 insertions(+), 18 deletions(-)
 create mode 100644 gcc/testsuite/gcc.target/loongarch/lasx-andn-iorn.c
 create mode 100644 gcc/testsuite/gcc.target/loongarch/lsx-andn-iorn.c

diff --git a/gcc/config/loongarch/lasx.md b/gcc/config/loongarch/lasx.md
index 44a7d58ff..3775155ca 100644
--- a/gcc/config/loongarch/lasx.md
+++ b/gcc/config/loongarch/lasx.md
@@ -2716,12 +2716,12 @@
    (set_attr "mode" "V4DI")])
 
 ;; Extend loongson-sx to loongson-asx.
-(define_insn "xvandn<mode>3"
+(define_insn "andn<mode>3"
   [(set (match_operand:LASX 0 "register_operand" "=f")
-	(and:LASX (not:LASX (match_operand:LASX 1 "register_operand" "f"))
-			    (match_operand:LASX 2 "register_operand" "f")))]
+	(and:LASX (not:LASX (match_operand:LASX 2 "register_operand" "f"))
+			    (match_operand:LASX 1 "register_operand" "f")))]
   "ISA_HAS_LASX"
-  "xvandn.v\t%u0,%u1,%u2"
+  "xvandn.v\t%u0,%u2,%u1"
   [(set_attr "type" "simd_logic")
    (set_attr "mode" "<MODE>")])
 
@@ -4637,7 +4637,7 @@
   [(set_attr "type" "simd_int_arith")
    (set_attr "mode" "<MODE>")])
 
-(define_insn "xvorn<mode>3"
+(define_insn "iorn<mode>3"
   [(set (match_operand:ILASX 0 "register_operand" "=f")
 	(ior:ILASX (not:ILASX (match_operand:ILASX 2 "register_operand" "f"))
 		   (match_operand:ILASX 1 "register_operand" "f")))]
diff --git a/gcc/config/loongarch/loongarch-builtins.cc b/gcc/config/loongarch/loongarch-builtins.cc
index 51abba007..f9ff85d2e 100644
--- a/gcc/config/loongarch/loongarch-builtins.cc
+++ b/gcc/config/loongarch/loongarch-builtins.cc
@@ -458,8 +458,8 @@ AVAIL_ALL (lasx_frecipe, ISA_HAS_LASX && ISA_HAS_FRECIPE)
 #define CODE_FOR_lsx_vabsd_du CODE_FOR_lsx_vabsd_u_du
 #define CODE_FOR_lsx_vftint_wu_s CODE_FOR_lsx_vftint_u_wu_s
 #define CODE_FOR_lsx_vftint_lu_d CODE_FOR_lsx_vftint_u_lu_d
-#define CODE_FOR_lsx_vandn_v CODE_FOR_vandnv16qi3
-#define CODE_FOR_lsx_vorn_v CODE_FOR_vornv16qi3
+#define CODE_FOR_lsx_vandn_v CODE_FOR_andnv16qi3
+#define CODE_FOR_lsx_vorn_v CODE_FOR_iornv16qi3
 #define CODE_FOR_lsx_vneg_b CODE_FOR_vnegv16qi2
 #define CODE_FOR_lsx_vneg_h CODE_FOR_vnegv8hi2
 #define CODE_FOR_lsx_vneg_w CODE_FOR_vnegv4si2
@@ -692,8 +692,8 @@ AVAIL_ALL (lasx_frecipe, ISA_HAS_LASX && ISA_HAS_FRECIPE)
 #define CODE_FOR_lasx_xvrepli_w CODE_FOR_lasx_xvrepliv8si
 #define CODE_FOR_lasx_xvrepli_d CODE_FOR_lasx_xvrepliv4di
 
-#define CODE_FOR_lasx_xvandn_v CODE_FOR_xvandnv32qi3
-#define CODE_FOR_lasx_xvorn_v CODE_FOR_xvornv32qi3
+#define CODE_FOR_lasx_xvandn_v CODE_FOR_andnv32qi3
+#define CODE_FOR_lasx_xvorn_v CODE_FOR_iornv32qi3
 #define CODE_FOR_lasx_xvneg_b CODE_FOR_negv32qi2
 #define CODE_FOR_lasx_xvneg_h CODE_FOR_negv16hi2
 #define CODE_FOR_lasx_xvneg_w CODE_FOR_negv8si2
@@ -2853,6 +2853,7 @@ loongarch_expand_builtin_insn (enum insn_code icode, unsigned int nops,
     case CODE_FOR_lsx_vpickod_b:
     case CODE_FOR_lsx_vpickod_h:
     case CODE_FOR_lsx_vpickod_w:
+    case CODE_FOR_lsx_vandn_v:
     case CODE_FOR_lasx_xvilvh_b:
     case CODE_FOR_lasx_xvilvh_h:
     case CODE_FOR_lasx_xvilvh_w:
@@ -2873,6 +2874,7 @@ loongarch_expand_builtin_insn (enum insn_code icode, unsigned int nops,
     case CODE_FOR_lasx_xvpickod_b:
     case CODE_FOR_lasx_xvpickod_h:
     case CODE_FOR_lasx_xvpickod_w:
+    case CODE_FOR_lasx_xvandn_v:
       /* Swap the operands 1 and 2 for interleave operations.  Built-ins follow
 	 convention of ISA, which have op1 as higher component and op2 as lower
 	 component.  However, the VEC_PERM op in tree and vec_concat in RTL
diff --git a/gcc/config/loongarch/loongarch.md b/gcc/config/loongarch/loongarch.md
index b1c828dba..58c8f28ed 100644
--- a/gcc/config/loongarch/loongarch.md
+++ b/gcc/config/loongarch/loongarch.md
@@ -1701,13 +1701,13 @@
   [(set_attr "type" "logical")
    (set_attr "mode" "SI")])
 
-(define_insn "<optab>n<mode>"
+(define_insn "<optab>n<mode>3"
   [(set (match_operand:X 0 "register_operand" "=r")
 	(neg_bitwise:X
-	    (not:X (match_operand:X 1 "register_operand" "r"))
-	    (match_operand:X 2 "register_operand" "r")))]
+	    (not:X (match_operand:X 2 "register_operand" "r"))
+	    (match_operand:X 1 "register_operand" "r")))]
   ""
-  "<insn>n\t%0,%2,%1"
+  "<insn>n\t%0,%1,%2"
   [(set_attr "type" "logical")
    (set_attr "mode" "<MODE>")])
 
diff --git a/gcc/config/loongarch/lsx.md b/gcc/config/loongarch/lsx.md
index 2eac11473..c7480aafd 100644
--- a/gcc/config/loongarch/lsx.md
+++ b/gcc/config/loongarch/lsx.md
@@ -2344,12 +2344,12 @@
 }
   [(set_attr "mode" "V4SF")])
 
-(define_insn "vandn<mode>3"
+(define_insn "andn<mode>3"
   [(set (match_operand:LSX 0 "register_operand" "=f")
-	(and:LSX (not:LSX (match_operand:LSX 1 "register_operand" "f"))
-		 (match_operand:LSX 2 "register_operand" "f")))]
+	(and:LSX (not:LSX (match_operand:LSX 2 "register_operand" "f"))
+		 (match_operand:LSX 1 "register_operand" "f")))]
   "ISA_HAS_LSX"
-  "vandn.v\t%w0,%w1,%w2"
+  "vandn.v\t%w0,%w2,%w1"
   [(set_attr "type" "simd_logic")
    (set_attr "mode" "<MODE>")])
 
@@ -3028,7 +3028,7 @@
   [(set_attr "type" "simd_int_arith")
    (set_attr "mode" "<MODE>")])
 
-(define_insn "vorn<mode>3"
+(define_insn "iorn<mode>3"
   [(set (match_operand:ILSX 0 "register_operand" "=f")
 	(ior:ILSX (not:ILSX (match_operand:ILSX 2 "register_operand" "f"))
 		  (match_operand:ILSX 1 "register_operand" "f")))]
diff --git a/gcc/testsuite/gcc.target/loongarch/lasx-andn-iorn.c b/gcc/testsuite/gcc.target/loongarch/lasx-andn-iorn.c
new file mode 100644
index 000000000..4aa5f19a6
--- /dev/null
+++ b/gcc/testsuite/gcc.target/loongarch/lasx-andn-iorn.c
@@ -0,0 +1,11 @@
+#define N 8
+
+#include "./lsx-andn-iorn.c"
+
+/* { dg-do compile } */
+/* { dg-options "-O2 -mlasx -ftree-vectorize" } */
+
+/* We should produce a BIT_ANDC and BIT_IORC here.  */
+
+/* { dg-final { scan-tree-dump ".BIT_ANDN " "optimized" } } */
+/* { dg-final { scan-tree-dump ".BIT_IORN " "optimized" } } */
diff --git a/gcc/testsuite/gcc.target/loongarch/lsx-andn-iorn.c b/gcc/testsuite/gcc.target/loongarch/lsx-andn-iorn.c
new file mode 100644
index 000000000..7bceccd37
--- /dev/null
+++ b/gcc/testsuite/gcc.target/loongarch/lsx-andn-iorn.c
@@ -0,0 +1,28 @@
+/* { dg-do compile } */
+/* { dg-options "-O2 -mlsx -ftree-vectorize" } */
+
+#ifndef N
+#define N 4
+#endif
+
+extern float a[N], b[N];
+extern int c[N], d[N];
+
+void
+bar1 (void)
+{
+  for (int i = 0; i < N; i++)
+    d[i] = a[i] > b[i] ? 0 : c[i];
+}
+
+void
+bar2 (void)
+{
+  for (int i = 0; i < N; i++)
+    d[i] = a[i] > b[i] ? c[i]: -1;
+}
+
+/* We should produce a BIT_ANDC and BIT_IORC here.  */
+
+/* { dg-final { scan-tree-dump ".BIT_ANDN " "optimized" } } */
+/* { dg-final { scan-tree-dump ".BIT_IORN " "optimized" } } */
-- 
2.43.0

