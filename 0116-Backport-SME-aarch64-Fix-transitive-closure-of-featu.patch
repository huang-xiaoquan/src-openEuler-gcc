From c6698a5feb07fc0cda89a54a0ee4006295ac6dbe Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Thu, 29 Sep 2022 11:32:53 +0100
Subject: [PATCH 017/157] [Backport][SME] aarch64: Fix transitive closure of
 features

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=b754d32d3053a4ba2a82361ac0f2739797a811f1

aarch64-option-extensions.def requires us to maintain the transitive
closure of options by hand.  This patch fixes a few cases where a
flag was missed.

+noaes and +nosha2 now disable +crypto, which IMO makes more
sense and is consistent with the Clang behaviour.

gcc/
	* config/aarch64/aarch64-option-extensions.def (dotprod): Depend
	on fp as well as simd.
	(sha3): Likewise.
	(aes): Likewise.  Make +noaes disable crypto.
	(sha2): Likewise +nosha2.  Also make +nosha2 disable sha3 and
	sve2-sha3.
	(sve2-sha3): Depend on sha2 as well as sha3.

gcc/testsuite/
	* gcc.target/aarch64/options_set_6.c: Expect +crypto+nosha2 to
	disable crypto but keep aes.
	* gcc.target/aarch64/pragma_cpp_predefs_4.c: New test.
---
 .../aarch64/aarch64-option-extensions.def     | 16 ++++---
 .../gcc.target/aarch64/options_set_6.c        |  5 +-
 .../gcc.target/aarch64/pragma_cpp_predefs_4.c | 47 +++++++++++++++++++
 3 files changed, 58 insertions(+), 10 deletions(-)
 create mode 100644 gcc/testsuite/gcc.target/aarch64/pragma_cpp_predefs_4.c

diff --git a/gcc/config/aarch64/aarch64-option-extensions.def b/gcc/config/aarch64/aarch64-option-extensions.def
index b4d0ac8b6..b98008127 100644
--- a/gcc/config/aarch64/aarch64-option-extensions.def
+++ b/gcc/config/aarch64/aarch64-option-extensions.def
@@ -113,28 +113,29 @@ AARCH64_OPT_EXTENSION("rdma", AARCH64_FL_RDMA, \
 
 /* Enabling "dotprod" also enables "simd".
    Disabling "dotprod" only disables "dotprod".  */
-AARCH64_OPT_EXTENSION("dotprod", AARCH64_FL_DOTPROD, AARCH64_FL_SIMD, 0, \
+AARCH64_OPT_EXTENSION("dotprod", AARCH64_FL_DOTPROD, AARCH64_FL_FPSIMD, 0, \
 		      false, "asimddp")
 
 /* Enabling "aes" also enables "simd".
    Disabling "aes" disables "aes" and "sve2-aes'.  */
-AARCH64_OPT_EXTENSION("aes", AARCH64_FL_AES, AARCH64_FL_SIMD, \
-		      AARCH64_FL_SVE2_AES, false, "aes")
+AARCH64_OPT_EXTENSION("aes", AARCH64_FL_AES, AARCH64_FL_FPSIMD, \
+		      AARCH64_FL_SVE2_AES | AARCH64_FL_CRYPTO, false, "aes")
 
 /* Enabling "sha2" also enables "simd".
    Disabling "sha2" just disables "sha2".  */
-AARCH64_OPT_EXTENSION("sha2", AARCH64_FL_SHA2, AARCH64_FL_SIMD, 0, false, \
-		      "sha1 sha2")
+AARCH64_OPT_EXTENSION("sha2", AARCH64_FL_SHA2, AARCH64_FL_FPSIMD, \
+		      AARCH64_FL_CRYPTO | AARCH64_FL_SHA3 | \
+		      AARCH64_FL_SVE2_SHA3, false, "sha1 sha2")
 
 /* Enabling "sha3" enables "simd" and "sha2".
    Disabling "sha3" disables "sha3" and "sve2-sha3".  */
-AARCH64_OPT_EXTENSION("sha3", AARCH64_FL_SHA3, AARCH64_FL_SIMD | \
+AARCH64_OPT_EXTENSION("sha3", AARCH64_FL_SHA3, AARCH64_FL_FPSIMD | \
 		      AARCH64_FL_SHA2, AARCH64_FL_SVE2_SHA3, false, \
 		      "sha3 sha512")
 
 /* Enabling "sm4" also enables "simd".
    Disabling "sm4" disables "sm4" and "sve2-sm4".  */
-AARCH64_OPT_EXTENSION("sm4", AARCH64_FL_SM4, AARCH64_FL_SIMD, \
+AARCH64_OPT_EXTENSION("sm4", AARCH64_FL_SM4, AARCH64_FL_FPSIMD, \
 		      AARCH64_FL_SVE2_SM4, false, "sm3 sm4")
 
 /* Enabling "fp16fml" also enables "fp" and "fp16".
@@ -192,6 +193,7 @@ AARCH64_OPT_EXTENSION("sve2-aes", AARCH64_FL_SVE2_AES, AARCH64_FL_AES | \
 /* Enabling "sve2-sha3" also enables "sha3", "simd", "fp16", "fp", "sve", and
    "sve2". Disabling "sve2-sha3" just disables "sve2-sha3".  */
 AARCH64_OPT_EXTENSION("sve2-sha3", AARCH64_FL_SVE2_SHA3, AARCH64_FL_SHA3 | \
+		      AARCH64_FL_SHA2 | \
 		      AARCH64_FL_SIMD | AARCH64_FL_F16 | AARCH64_FL_FP | \
 		      AARCH64_FL_SVE | AARCH64_FL_SVE2, 0, false, "svesha3")
 
diff --git a/gcc/testsuite/gcc.target/aarch64/options_set_6.c b/gcc/testsuite/gcc.target/aarch64/options_set_6.c
index 90a055928..2a1d7fe5b 100644
--- a/gcc/testsuite/gcc.target/aarch64/options_set_6.c
+++ b/gcc/testsuite/gcc.target/aarch64/options_set_6.c
@@ -6,7 +6,6 @@ int main ()
   return 0;
 }
 
-/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crypto\+crc} 1 } } */
+/* { dg-final { scan-assembler-times {\.arch armv8\.2\-a\+crc\+aes} 1 } } */
 
-/* Group as a whole was requested to be turned on, crypto itself is a bit and so
-   just turning off one feature can't turn it off.   */
+/* +crypto turns on +aes and +sha2, but +nosha2 disables +crypto.   */
diff --git a/gcc/testsuite/gcc.target/aarch64/pragma_cpp_predefs_4.c b/gcc/testsuite/gcc.target/aarch64/pragma_cpp_predefs_4.c
new file mode 100644
index 000000000..0e6461fa4
--- /dev/null
+++ b/gcc/testsuite/gcc.target/aarch64/pragma_cpp_predefs_4.c
@@ -0,0 +1,47 @@
+#pragma GCC target "+nothing+dotprod"
+#ifndef __ARM_FEATURE_FMA
+#error Foo
+#endif
+
+#pragma GCC target "+nothing+aes"
+#ifndef __ARM_FEATURE_FMA
+#error Foo
+#endif
+
+#pragma GCC target "+nothing+sha2"
+#ifndef __ARM_FEATURE_FMA
+#error Foo
+#endif
+
+#pragma GCC target "+nothing+sha3"
+#ifndef __ARM_FEATURE_FMA
+#error Foo
+#endif
+
+#pragma GCC target "+nothing+sm4"
+#ifndef __ARM_FEATURE_FMA
+#error Foo
+#endif
+
+#pragma GCC target "+crypto+noaes"
+#ifdef __ARM_FEATURE_CRYPTO
+#error Foo
+#endif
+
+#pragma GCC target "+crypto+nosha2"
+#ifdef __ARM_FEATURE_CRYPTO
+#error Foo
+#endif
+
+#pragma GCC target "+nothing+sve2-sha3"
+#ifndef __ARM_FEATURE_SHA2
+#error Foo
+#endif
+
+#pragma GCC target "+sve2-sha3+nosha2"
+#ifdef __ARM_FEATURE_SHA3
+#error Foo
+#endif
+#ifdef __ARM_FEATURE_SVE2_SHA3
+#error Foo
+#endif
-- 
2.33.0

