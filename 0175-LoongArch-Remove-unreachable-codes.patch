From a75609d90f506709e4af26701aa4fb6adce00700 Mon Sep 17 00:00:00 2001
From: Lulu Cheng <chenglulu@loongson.cn>
Date: Thu, 4 Jul 2024 15:00:40 +0800
Subject: [PATCH 175/188] LoongArch: Remove unreachable codes.

gcc/ChangeLog:

	* config/loongarch/loongarch.cc
	(loongarch_split_move): Delete.
	(loongarch_hard_regno_mode_ok_uncached): Likewise.
	* config/loongarch/loongarch.md
	(move_doubleword_fpr<mode>): Likewise.
	(load_low<mode>): Likewise.
	(load_high<mode>): Likewise.
	(store_word<mode>): Likewise.
	(movgr2frh<mode>): Likewise.
	(movfrh2gr<mode>): Likewise.
---
 gcc/config/loongarch/loongarch.cc |  47 +++----------
 gcc/config/loongarch/loongarch.md | 109 ------------------------------
 2 files changed, 8 insertions(+), 148 deletions(-)

diff --git a/gcc/config/loongarch/loongarch.cc b/gcc/config/loongarch/loongarch.cc
index b78512e0e..260dd7b5f 100644
--- a/gcc/config/loongarch/loongarch.cc
+++ b/gcc/config/loongarch/loongarch.cc
@@ -4459,42 +4459,13 @@ loongarch_split_move_p (rtx dest, rtx src)
 void
 loongarch_split_move (rtx dest, rtx src)
 {
-  rtx low_dest;
-
   gcc_checking_assert (loongarch_split_move_p (dest, src));
   if (LSX_SUPPORTED_MODE_P (GET_MODE (dest)))
     loongarch_split_128bit_move (dest, src);
   else if (LASX_SUPPORTED_MODE_P (GET_MODE (dest)))
     loongarch_split_256bit_move (dest, src);
-  else if (FP_REG_RTX_P (dest) || FP_REG_RTX_P (src))
-    {
-      if (!TARGET_64BIT && GET_MODE (dest) == DImode)
-	emit_insn (gen_move_doubleword_fprdi (dest, src));
-      else if (!TARGET_64BIT && GET_MODE (dest) == DFmode)
-	emit_insn (gen_move_doubleword_fprdf (dest, src));
-      else if (TARGET_64BIT && GET_MODE (dest) == TFmode)
-	emit_insn (gen_move_doubleword_fprtf (dest, src));
-      else
-	gcc_unreachable ();
-    }
   else
-    {
-      /* The operation can be split into two normal moves.  Decide in
-	 which order to do them.  */
-      low_dest = loongarch_subword (dest, false);
-      if (REG_P (low_dest) && reg_overlap_mentioned_p (low_dest, src))
-	{
-	  loongarch_emit_move (loongarch_subword (dest, true),
-			       loongarch_subword (src, true));
-	  loongarch_emit_move (low_dest, loongarch_subword (src, false));
-	}
-      else
-	{
-	  loongarch_emit_move (low_dest, loongarch_subword (src, false));
-	  loongarch_emit_move (loongarch_subword (dest, true),
-			       loongarch_subword (src, true));
-	}
-    }
+    gcc_unreachable ();
 }
 
 /* Check if adding an integer constant value for a specific mode can be
@@ -6743,20 +6714,18 @@ loongarch_hard_regno_mode_ok_uncached (unsigned int regno, machine_mode mode)
   size = GET_MODE_SIZE (mode);
   mclass = GET_MODE_CLASS (mode);
 
-  if (GP_REG_P (regno) && !LSX_SUPPORTED_MODE_P (mode)
+  if (GP_REG_P (regno)
+      && !LSX_SUPPORTED_MODE_P (mode)
       && !LASX_SUPPORTED_MODE_P (mode))
     return ((regno - GP_REG_FIRST) & 1) == 0 || size <= UNITS_PER_WORD;
 
-  /* For LSX, allow TImode and 128-bit vector modes in all FPR.  */
-  if (FP_REG_P (regno) && LSX_SUPPORTED_MODE_P (mode))
-    return true;
-
-  /* FIXED ME: For LASX, allow TImode and 256-bit vector modes in all FPR.  */
-  if (FP_REG_P (regno) && LASX_SUPPORTED_MODE_P (mode))
-    return true;
-
   if (FP_REG_P (regno))
     {
+      /* Allow 128-bit or 256-bit vector modes in all FPR.  */
+      if (LSX_SUPPORTED_MODE_P (mode)
+	  || LASX_SUPPORTED_MODE_P (mode))
+	return true;
+
       if (mclass == MODE_FLOAT
 	  || mclass == MODE_COMPLEX_FLOAT
 	  || mclass == MODE_VECTOR_FLOAT)
diff --git a/gcc/config/loongarch/loongarch.md b/gcc/config/loongarch/loongarch.md
index 55a759850..16f9f37c8 100644
--- a/gcc/config/loongarch/loongarch.md
+++ b/gcc/config/loongarch/loongarch.md
@@ -400,9 +400,6 @@
 ;; 64-bit modes for which we provide move patterns.
 (define_mode_iterator MOVE64 [DI DF])
 
-;; 128-bit modes for which we provide move patterns on 64-bit targets.
-(define_mode_iterator MOVE128 [TI TF])
-
 ;; Iterator for sub-32-bit integer modes.
 (define_mode_iterator SHORT [QI HI])
 
@@ -421,12 +418,6 @@
 (define_mode_iterator ANYFI [(SI "TARGET_HARD_FLOAT")
 			     (DI "TARGET_DOUBLE_FLOAT")])
 
-;; A mode for which moves involving FPRs may need to be split.
-(define_mode_iterator SPLITF
-  [(DF "!TARGET_64BIT && TARGET_DOUBLE_FLOAT")
-   (DI "!TARGET_64BIT && TARGET_DOUBLE_FLOAT")
-   (TF "TARGET_64BIT && TARGET_DOUBLE_FLOAT")])
-
 ;; A mode for anything with 32 bits or more, and able to be loaded with
 ;; the same addressing mode as ld.w.
 (define_mode_iterator LD_AT_LEAST_32_BIT [GPR ANYF])
@@ -2421,41 +2412,6 @@
   [(set_attr "move_type" "move,load,store")
    (set_attr "mode" "DF")])
 
-;; Emit a doubleword move in which exactly one of the operands is
-;; a floating-point register.  We can't just emit two normal moves
-;; because of the constraints imposed by the FPU register model;
-;; see loongarch_can_change_mode_class for details.  Instead, we keep
-;; the FPR whole and use special patterns to refer to each word of
-;; the other operand.
-
-(define_expand "move_doubleword_fpr<mode>"
-  [(set (match_operand:SPLITF 0)
-	(match_operand:SPLITF 1))]
-  ""
-{
-  if (FP_REG_RTX_P (operands[0]))
-    {
-      rtx low = loongarch_subword (operands[1], 0);
-      rtx high = loongarch_subword (operands[1], 1);
-      emit_insn (gen_load_low<mode> (operands[0], low));
-      if (!TARGET_64BIT)
-       emit_insn (gen_movgr2frh<mode> (operands[0], high, operands[0]));
-      else
-       emit_insn (gen_load_high<mode> (operands[0], high, operands[0]));
-    }
-  else
-    {
-      rtx low = loongarch_subword (operands[0], 0);
-      rtx high = loongarch_subword (operands[0], 1);
-      emit_insn (gen_store_word<mode> (low, operands[1], const0_rtx));
-      if (!TARGET_64BIT)
-       emit_insn (gen_movfrh2gr<mode> (high, operands[1]));
-      else
-       emit_insn (gen_store_word<mode> (high, operands[1], const1_rtx));
-    }
-  DONE;
-})
-
 ;; Clear one FCC register
 
 (define_expand "movfcc"
@@ -2742,49 +2698,6 @@
   [(set_attr "type" "fcvt")
    (set_attr "mode" "<ANYF:MODE>")])
 
-;; Load the low word of operand 0 with operand 1.
-(define_insn "load_low<mode>"
-  [(set (match_operand:SPLITF 0 "register_operand" "=f,f")
-	(unspec:SPLITF [(match_operand:<HALFMODE> 1 "general_operand" "rJ,m")]
-		       UNSPEC_LOAD_LOW))]
-  "TARGET_HARD_FLOAT"
-{
-  operands[0] = loongarch_subword (operands[0], 0);
-  return loongarch_output_move (operands[0], operands[1]);
-}
-  [(set_attr "move_type" "mgtf,fpload")
-   (set_attr "mode" "<HALFMODE>")])
-
-;; Load the high word of operand 0 from operand 1, preserving the value
-;; in the low word.
-(define_insn "load_high<mode>"
-  [(set (match_operand:SPLITF 0 "register_operand" "=f,f")
-	(unspec:SPLITF [(match_operand:<HALFMODE> 1 "general_operand" "rJ,m")
-			(match_operand:SPLITF 2 "register_operand" "0,0")]
-		       UNSPEC_LOAD_HIGH))]
-  "TARGET_HARD_FLOAT"
-{
-  operands[0] = loongarch_subword (operands[0], 1);
-  return loongarch_output_move (operands[0], operands[1]);
-}
-  [(set_attr "move_type" "mgtf,fpload")
-   (set_attr "mode" "<HALFMODE>")])
-
-;; Store one word of operand 1 in operand 0.  Operand 2 is 1 to store the
-;; high word and 0 to store the low word.
-(define_insn "store_word<mode>"
-  [(set (match_operand:<HALFMODE> 0 "nonimmediate_operand" "=r,m")
-	(unspec:<HALFMODE> [(match_operand:SPLITF 1 "register_operand" "f,f")
-			    (match_operand 2 "const_int_operand")]
-			   UNSPEC_STORE_WORD))]
-  "TARGET_HARD_FLOAT"
-{
-  operands[1] = loongarch_subword (operands[1], INTVAL (operands[2]));
-  return loongarch_output_move (operands[0], operands[1]);
-}
-  [(set_attr "move_type" "mftg,fpstore")
-   (set_attr "mode" "<HALFMODE>")])
-
 ;; Thread-Local Storage
 
 (define_insn "@got_load_tls_desc<mode>"
@@ -2876,28 +2789,6 @@
 	(const_int 4)
 	(const_int 2)))])
 
-;; Move operand 1 to the high word of operand 0 using movgr2frh.w, preserving the
-;; value in the low word.
-(define_insn "movgr2frh<mode>"
-  [(set (match_operand:SPLITF 0 "register_operand" "=f")
-	(unspec:SPLITF [(match_operand:<HALFMODE> 1 "reg_or_0_operand" "rJ")
-			(match_operand:SPLITF 2 "register_operand" "0")]
-			UNSPEC_MOVGR2FRH))]
-  "TARGET_DOUBLE_FLOAT"
-  "movgr2frh.w\t%z1,%0"
-  [(set_attr "move_type" "mgtf")
-   (set_attr "mode" "<HALFMODE>")])
-
-;; Move high word of operand 1 to operand 0 using movfrh2gr.s.
-(define_insn "movfrh2gr<mode>"
-  [(set (match_operand:<HALFMODE> 0 "register_operand" "=r")
-	(unspec:<HALFMODE> [(match_operand:SPLITF 1 "register_operand" "f")]
-			    UNSPEC_MOVFRH2GR))]
-  "TARGET_DOUBLE_FLOAT"
-  "movfrh2gr.s\t%0,%1"
-  [(set_attr "move_type" "mftg")
-   (set_attr "mode" "<HALFMODE>")])
-
 
 ;; Expand in-line code to clear the instruction cache between operand[0] and
 ;; operand[1].
-- 
2.43.0

