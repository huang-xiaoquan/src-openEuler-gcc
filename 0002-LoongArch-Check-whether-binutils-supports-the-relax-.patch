From 13c33536900709bf1f33171d5ae2b2af97789601 Mon Sep 17 00:00:00 2001
From: Lulu Cheng <chenglulu@loongson.cn>
Date: Fri, 15 Sep 2023 10:22:49 +0800
Subject: [PATCH 002/188] LoongArch: Check whether binutils supports the relax
 function. If supported, explicit relocs are turned off by default.

gcc/ChangeLog:

	* config.in: Regenerate.
	* config/loongarch/genopts/loongarch.opt.in: Add compilation option
	mrelax. And set the initial value of explicit-relocs according to the
	detection status.
	* config/loongarch/gnu-user.h: When compiling with -mno-relax, pass the
	--no-relax option to the linker.
	* config/loongarch/loongarch-driver.h (ASM_SPEC): When compiling with
	-mno-relax, pass the -mno-relax option to the assembler.
	* config/loongarch/loongarch-opts.h (HAVE_AS_MRELAX_OPTION): Define macro.
	* config/loongarch/loongarch.opt: Regenerate.
	* configure: Regenerate.
	* configure.ac: Add detection of support for binutils relax function.
---
 gcc/config.in                                 |  6 ++++
 gcc/config/loongarch/genopts/loongarch.opt.in |  7 ++++-
 gcc/config/loongarch/gnu-user.h               |  3 +-
 gcc/config/loongarch/loongarch-driver.h       |  2 +-
 gcc/config/loongarch/loongarch-opts.h         |  4 +++
 gcc/config/loongarch/loongarch.opt            |  7 ++++-
 gcc/configure                                 | 31 +++++++++++++++++++
 gcc/configure.ac                              |  4 +++
 8 files changed, 60 insertions(+), 4 deletions(-)

diff --git a/gcc/config.in b/gcc/config.in
index 0dff36199..0c55e67e7 100644
--- a/gcc/config.in
+++ b/gcc/config.in
@@ -637,6 +637,12 @@
 #endif
 
 
+/* Define if your assembler supports -mrelax option. */
+#ifndef USED_FOR_TARGET
+#undef HAVE_AS_MRELAX_OPTION
+#endif
+
+
 /* Define if your assembler supports .mspabi_attribute. */
 #ifndef USED_FOR_TARGET
 #undef HAVE_AS_MSPABI_ATTRIBUTE
diff --git a/gcc/config/loongarch/genopts/loongarch.opt.in b/gcc/config/loongarch/genopts/loongarch.opt.in
index 2ef1b1e3b..f18733c24 100644
--- a/gcc/config/loongarch/genopts/loongarch.opt.in
+++ b/gcc/config/loongarch/genopts/loongarch.opt.in
@@ -181,7 +181,7 @@ Target Joined RejectNegative UInteger Var(loongarch_max_inline_memcpy_size) Init
 -mmax-inline-memcpy-size=SIZE	Set the max size of memcpy to inline, default is 1024.
 
 mexplicit-relocs
-Target Var(TARGET_EXPLICIT_RELOCS) Init(HAVE_AS_EXPLICIT_RELOCS)
+Target Var(TARGET_EXPLICIT_RELOCS) Init(HAVE_AS_EXPLICIT_RELOCS & !HAVE_AS_MRELAX_OPTION)
 Use %reloc() assembly operators.
 
 ; The code model option names for -mcmodel.
@@ -214,3 +214,8 @@ Specify the code model.
 mdirect-extern-access
 Target Var(TARGET_DIRECT_EXTERN_ACCESS) Init(0)
 Avoid using the GOT to access external symbols.
+
+mrelax
+Target Var(loongarch_mrelax) Init(HAVE_AS_MRELAX_OPTION)
+Take advantage of linker relaxations to reduce the number of instructions
+required to materialize symbol addresses.
diff --git a/gcc/config/loongarch/gnu-user.h b/gcc/config/loongarch/gnu-user.h
index 44e4f2575..60ef75601 100644
--- a/gcc/config/loongarch/gnu-user.h
+++ b/gcc/config/loongarch/gnu-user.h
@@ -48,7 +48,8 @@ along with GCC; see the file COPYING3.  If not see
   "%{!shared: %{static} " \
   "%{!static: %{!static-pie: %{rdynamic:-export-dynamic} " \
   "-dynamic-linker " GNU_USER_DYNAMIC_LINKER "}} " \
-  "%{static-pie: -static -pie --no-dynamic-linker -z text}}"
+  "%{static-pie: -static -pie --no-dynamic-linker -z text}}" \
+  "%{mno-relax: --no-relax}"
 
 
 /* Similar to standard Linux, but adding -ffast-math support.  */
diff --git a/gcc/config/loongarch/loongarch-driver.h b/gcc/config/loongarch/loongarch-driver.h
index e7d083677..59fa3263d 100644
--- a/gcc/config/loongarch/loongarch-driver.h
+++ b/gcc/config/loongarch/loongarch-driver.h
@@ -53,7 +53,7 @@ along with GCC; see the file COPYING3.  If not see
 
 #undef ASM_SPEC
 #define ASM_SPEC \
-  "%{mabi=*} %(subtarget_asm_spec)"
+  "%{mabi=*} %{mno-relax} %(subtarget_asm_spec)"
 
 
 extern const char*
diff --git a/gcc/config/loongarch/loongarch-opts.h b/gcc/config/loongarch/loongarch-opts.h
index 624e246bb..f2b59abe6 100644
--- a/gcc/config/loongarch/loongarch-opts.h
+++ b/gcc/config/loongarch/loongarch-opts.h
@@ -99,4 +99,8 @@ loongarch_update_gcc_opt_status (struct loongarch_target *target,
 #define HAVE_AS_EXPLICIT_RELOCS 0
 #endif
 
+#ifndef HAVE_AS_MRELAX_OPTION
+#define HAVE_AS_MRELAX_OPTION 0
+#endif
+
 #endif /* LOONGARCH_OPTS_H */
diff --git a/gcc/config/loongarch/loongarch.opt b/gcc/config/loongarch/loongarch.opt
index f2d21c9f3..78f2baf3a 100644
--- a/gcc/config/loongarch/loongarch.opt
+++ b/gcc/config/loongarch/loongarch.opt
@@ -188,7 +188,7 @@ Target Joined RejectNegative UInteger Var(loongarch_max_inline_memcpy_size) Init
 -mmax-inline-memcpy-size=SIZE	Set the max size of memcpy to inline, default is 1024.
 
 mexplicit-relocs
-Target Var(TARGET_EXPLICIT_RELOCS) Init(HAVE_AS_EXPLICIT_RELOCS)
+Target Var(TARGET_EXPLICIT_RELOCS) Init(HAVE_AS_EXPLICIT_RELOCS & !HAVE_AS_MRELAX_OPTION)
 Use %reloc() assembly operators.
 
 ; The code model option names for -mcmodel.
@@ -221,3 +221,8 @@ Specify the code model.
 mdirect-extern-access
 Target Var(TARGET_DIRECT_EXTERN_ACCESS) Init(0)
 Avoid using the GOT to access external symbols.
+
+mrelax
+Target Var(loongarch_mrelax) Init(HAVE_AS_MRELAX_OPTION)
+Take advantage of linker relaxations to reduce the number of instructions
+required to materialize symbol addresses.
diff --git a/gcc/configure b/gcc/configure
index 2a5d3aaf3..8ae8a924a 100755
--- a/gcc/configure
+++ b/gcc/configure
@@ -28830,6 +28830,37 @@ if test $gcc_cv_as_loongarch_eh_frame_pcrel_encoding_support = yes; then
 
 $as_echo "#define HAVE_AS_EH_FRAME_PCREL_ENCODING_SUPPORT 1" >>confdefs.h
 
+fi
+
+    { $as_echo "$as_me:${as_lineno-$LINENO}: checking assembler for -mrelax option" >&5
+$as_echo_n "checking assembler for -mrelax option... " >&6; }
+if ${gcc_cv_as_loongarch_relax+:} false; then :
+  $as_echo_n "(cached) " >&6
+else
+  gcc_cv_as_loongarch_relax=no
+  if test x$gcc_cv_as != x; then
+    $as_echo '.text' > conftest.s
+    if { ac_try='$gcc_cv_as $gcc_cv_as_flags -mrelax -o conftest.o conftest.s >&5'
+  { { eval echo "\"\$as_me\":${as_lineno-$LINENO}: \"$ac_try\""; } >&5
+  (eval $ac_try) 2>&5
+  ac_status=$?
+  $as_echo "$as_me:${as_lineno-$LINENO}: \$? = $ac_status" >&5
+  test $ac_status = 0; }; }
+    then
+	gcc_cv_as_loongarch_relax=yes
+    else
+      echo "configure: failed program was" >&5
+      cat conftest.s >&5
+    fi
+    rm -f conftest.o conftest.s
+  fi
+fi
+{ $as_echo "$as_me:${as_lineno-$LINENO}: result: $gcc_cv_as_loongarch_relax" >&5
+$as_echo "$gcc_cv_as_loongarch_relax" >&6; }
+if test $gcc_cv_as_loongarch_relax = yes; then
+
+$as_echo "#define HAVE_AS_MRELAX_OPTION 1" >>confdefs.h
+
 fi
 
     ;;
diff --git a/gcc/configure.ac b/gcc/configure.ac
index ba2bf1ffc..f7161e66e 100644
--- a/gcc/configure.ac
+++ b/gcc/configure.ac
@@ -5322,6 +5322,10 @@ x:
        .cfi_endproc],,
       [AC_DEFINE(HAVE_AS_EH_FRAME_PCREL_ENCODING_SUPPORT, 1,
 	  [Define if your assembler supports eh_frame pcrel encoding.])])
+    gcc_GAS_CHECK_FEATURE([-mrelax option], gcc_cv_as_loongarch_relax,
+      [-mrelax], [.text],,
+      [AC_DEFINE(HAVE_AS_MRELAX_OPTION, 1,
+		[Define if your assembler supports -mrelax option.])])
     ;;
     s390*-*-*)
     gcc_GAS_CHECK_FEATURE([.gnu_attribute support],
-- 
2.43.0

