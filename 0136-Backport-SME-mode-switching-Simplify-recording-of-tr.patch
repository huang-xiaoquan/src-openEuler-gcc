From ac51d446ee605e942b0831d3ff617980d94bf502 Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Sat, 11 Nov 2023 17:28:56 +0000
Subject: [PATCH 037/157] [Backport][SME] mode-switching: Simplify recording of
 transparency

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=335b55f4146c5ef9e3bf4bcb7e58e887c3150b02

For a given block, an entity is either transparent for
all modes or for none.  Each update to the transparency set
therefore used a loop like:

		for (i = 0; i < no_mode; i++)
		  clear_mode_bit (transp[bb->index], j, i);

This patch instead starts out with a bit-per-block bitmap
and updates the main bitmap at the end.

This isn't much of a simplification on its own.  The main
purpose is to simplify later patches.

gcc/
	* mode-switching.cc (optimize_mode_switching): Initially
	compute transparency in a bit-per-block bitmap.
---
 gcc/mode-switching.cc | 19 +++++++++++--------
 1 file changed, 11 insertions(+), 8 deletions(-)

diff --git a/gcc/mode-switching.cc b/gcc/mode-switching.cc
index 584cd4f67..4d2b9e284 100644
--- a/gcc/mode-switching.cc
+++ b/gcc/mode-switching.cc
@@ -555,6 +555,8 @@ optimize_mode_switching (void)
   bitmap_vector_clear (antic, last_basic_block_for_fn (cfun));
   bitmap_vector_clear (comp, last_basic_block_for_fn (cfun));
 
+  auto_sbitmap transp_all (last_basic_block_for_fn (cfun));
+
   for (j = n_entities - 1; j >= 0; j--)
     {
       int e = entity_map[j];
@@ -562,6 +564,8 @@ optimize_mode_switching (void)
       struct bb_info *info = bb_info[j];
       rtx_insn *insn;
 
+      bitmap_ones (transp_all);
+
       /* Determine what the first use (if any) need for a mode of entity E is.
 	 This will be the mode that is anticipatable for this block.
 	 Also compute the initial transparency settings.  */
@@ -594,8 +598,7 @@ optimize_mode_switching (void)
 		  ins_pos = NEXT_INSN (ins_pos);
 		ptr = new_seginfo (no_mode, no_mode, ins_pos, live_now);
 		add_seginfo (&tail_ptr, ptr);
-		for (i = 0; i < no_mode; i++)
-		  clear_mode_bit (transp[bb->index], j, i);
+		bitmap_clear_bit (transp_all, bb->index);
 	      }
 	  }
 
@@ -610,8 +613,7 @@ optimize_mode_switching (void)
 		    {
 		      ptr = new_seginfo (last_mode, mode, insn, live_now);
 		      add_seginfo (&tail_ptr, ptr);
-		      for (i = 0; i < no_mode; i++)
-			clear_mode_bit (transp[bb->index], j, i);
+		      bitmap_clear_bit (transp_all, bb->index);
 		      any_set_required = true;
 		      last_mode = mode;
 		    }
@@ -642,8 +644,7 @@ optimize_mode_switching (void)
 	      ptr = new_seginfo (last_mode, no_mode, BB_END (bb), live_now);
 	      add_seginfo (&tail_ptr, ptr);
 	      if (last_mode != no_mode)
-		for (i = 0; i < no_mode; i++)
-		  clear_mode_bit (transp[bb->index], j, i);
+		bitmap_clear_bit (transp_all, bb->index);
 	    }
 	}
       if (targetm.mode_switching.entry && targetm.mode_switching.exit)
@@ -666,8 +667,7 @@ optimize_mode_switching (void)
 		 an extra check in make_preds_opaque.  We also
 		 need this to avoid confusing pre_edge_lcm when
 		 antic is cleared but transp and comp are set.  */
-	      for (i = 0; i < no_mode; i++)
-		clear_mode_bit (transp[bb->index], j, i);
+	      bitmap_clear_bit (transp_all, bb->index);
 
 	      /* Insert a fake computing definition of MODE into entry
 		 blocks which compute no mode. This represents the mode on
@@ -687,6 +687,9 @@ optimize_mode_switching (void)
 
 	  FOR_EACH_BB_FN (bb, cfun)
 	    {
+	      if (!bitmap_bit_p (transp_all, bb->index))
+		clear_mode_bit (transp[bb->index], j, m);
+
 	      if (info[bb->index].seginfo->mode == m)
 		set_mode_bit (antic[bb->index], j, m);
 
-- 
2.33.0

