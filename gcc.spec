%global gcc_version 12.3.1
%global gcc_major 12
# Note, gcc_release must be integer, if you want to add suffixes to
# %%{release}, append them after %%{gcc_release} on Release: line.
%global gcc_release 75

%global _unpackaged_files_terminate_build 0
%global _performance_build 1
# Hardening slows the compiler way too much.
%undefine _hardened_build
%undefine _auto_set_build_flags
%undefine _annotated_build

%global multilib_64_archs sparc64 ppc64 ppc64p7

%global build_ada 0
%global build_objc 1
%global build_go 0
%global build_d 0

%ifarch %{ix86} x86_64 ia64 ppc64le aarch64
%global build_libquadmath 1
%else
%global build_libquadmath 0
%endif
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le ppc64p7 s390 s390x %{arm} aarch64 loongarch64 riscv64 sw_64
%global build_libasan 1
%else
%global build_libasan 0
%endif
%ifarch x86_64 ppc64 ppc64le aarch64 s390x loongarch64 sw_64
%global build_libtsan 1
%else
%global build_libtsan 0
%endif
%ifarch x86_64 ppc64 ppc64le aarch64 s390x loongarch64 sw_64
%global build_liblsan 1
%else
%global build_liblsan 0
%endif
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le ppc64p7 s390 s390x %{arm} aarch64 loongarch64 riscv64 sw_64
%global build_libubsan 1
%else
%global build_libubsan 0
%endif
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le ppc64p7 s390 s390x %{arm} aarch64 %{mips} riscv64 loongarch64 sw_64
%global build_libatomic 1
%else
%global build_libatomic 0
%endif
%ifarch %{ix86} x86_64 %{arm} alpha ppc ppc64 ppc64le ppc64p7 s390 s390x aarch64 loongarch64 sw_64
%global build_libitm 1
%else
%global build_libitm 0
%endif
%global build_libstdcxx_docs 0
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le ppc64p7 s390 s390x %{arm} aarch64 %{mips} loongarch64
%global attr_ifunc 1
%else
%global attr_ifunc 0
%endif
%ifarch sparc64
%global multilib_32_arch sparcv9
%endif
%ifarch ppc64 ppc64p7
%global multilib_32_arch ppc
%endif
%ifarch x86_64
%global multilib_32_arch i686
%endif
%ifarch riscv64
%global _smp_mflags -j8
%endif

%global isl_enable 0
%global check_enable 0

Summary: Various compilers (C, C++, Objective-C, ...)
Name: gcc
Version: %{gcc_version}
Release: %{gcc_release}
# libgcc, libgfortran, libgomp, libstdc++ and crtstuff have
# GCC Runtime Exception.
License: GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD
URL: https://gcc.gnu.org
Source0: https://ftp.gnu.org/gnu/gcc/gcc-12.3.0/gcc-12.3.0.tar.xz

BuildRequires: binutils >= 2.31
BuildRequires: glibc-headers
BuildRequires: libtool, zlib-devel, texinfo, gettext, flex, bison, sharutils
BuildRequires: gmp-devel >= 4.1.2, mpfr-devel >= 3.1.0, libmpc-devel >= 0.8.1
%if %{isl_enable}
BuildRequires: isl >= 0.15.0, isl-devel >= 0.15.0
%endif
BuildRequires: gcc, gcc-c++, make, chrpath
%if %{build_go}
BuildRequires: hostname, procps
%endif
BuildRequires: gdb
BuildRequires: glibc-devel >= 2.17
%ifarch %{multilib_64_archs} sparcv9 ppc
# Ensure glibc{,-devel} is installed for both multilib arches
BuildRequires: /lib/libc.so.6 /usr/lib/libc.so /lib64/libc.so.6 /usr/lib64/libc.so
%endif
%if %{build_ada}
# Ada requires Ada to build
BuildRequires: gcc-gnat >= 3.1, libgnat >= 3.1
%endif
%if %{build_d}
# D requires D to build
BuildRequires: gcc-gdc >= 11.0.0, libgphobos-static >= 11.0.0
%endif
%ifarch ia64
BuildRequires: libunwind >= 0.98
%endif
%if %{build_libstdcxx_docs}
BuildRequires: doxygen >= 1.7.1
BuildRequires: graphviz, dblatex, texlive-collection-latex, docbook5-style-xsl
%endif
Requires: cpp = %{version}-%{release}
Requires: binutils >= 2.31
Conflicts: gdb < 5.1-2
Requires: glibc-devel >= 2.17
Requires: libgcc >= %{version}-%{release}
Requires: libgomp = %{version}-%{release}
# lto-wrapper invokes make
Requires: make
%if !%{build_ada}
Obsoletes: gcc-gnat < %{version}-%{release}
%endif
Obsoletes: gcc-java < %{version}-%{release}
AutoReq: true
Provides: bundled(libiberty)
Provides: bundled(libbacktrace)
Provides: bundled(libffi)
Provides: gcc(major) = %{gcc_major}

Patch1: 0001-Version-Set-version-to-12.3.1.patch
Patch2: 0002-RISCV-Backport-inline-subword-atomic-patches.patch
Patch3: 0003-CONFIG-Regenerate-configure-file.patch
Patch4: 0004-libquadmath-Enable-libquadmath-on-kunpeng.patch
Patch6: 0006-MULL64-1-3-Add-A-B-op-CST-B-match-and-simplify-optim.patch
Patch7: 0007-MULL64-2-3-Fold-series-of-instructions-into-mul.patch
Patch8: 0008-MULL64-3-3-Fold-series-of-instructions-into-umulh.patch
Patch9: 0009-MULL64-Disable-mull64-transformation-by-default.patch
Patch10: 0010-Version-Clear-DATESTAMP_s.patch
Patch11: 0011-Add-attribute-hot-judgement-for-INLINE_HINT_known_ho.patch
Patch12: 0012-Enable-small-loop-unrolling-for-O2.patch
Patch13: 0013-i386-Only-enable-small-loop-unrolling-in-backend-PR-.patch
Patch14: 0014-Array-widen-compare-Add-a-new-optimization-for-array.patch
Patch15: 0015-Backport-Structure-reorganization-optimization.patch
Patch16: 0016-CompleteStructRelayout-Complete-Structure-Relayout.patch
Patch17: 0017-StructReorg-Some-bugfix-for-structure-reorganization.patch
Patch18: 0018-ccmp-Add-another-optimization-opportunity-for-ccmp-i.patch
Patch19: 0019-fp-model-Enable-fp-model-on-kunpeng.patch
Patch20: 0020-simdmath-Enable-simdmath-on-kunpeng.patch
Patch21: 0021-StructReorderFields-Structure-reorder-fields.patch
Patch22: 0022-DFE-Add-Dead-Field-Elimination-in-Struct-Reorg.patch
Patch23: 0023-PGO-kernel-Add-fkernel-pgo-option-to-support-PGO-ker.patch
Patch24: 0024-Struct-Reorg-Refactoring-and-merge-reorder-fields-in.patch
Patch25: 0025-AArch64-Rewrite-the-tsv110-option.patch
Patch26: 0026-GOMP-Enabling-moutline-atomics-improves-libgomp-perf.patch
Patch27: 0027-LoopElim-Redundant-loop-elimination-optimization.patch
Patch28: 0028-Array-widen-compare-Fix-the-return-value-match-after.patch
Patch29: 0029-Struct-Reorg-Add-Safe-Structure-Pointer-Compression.patch
Patch30: 0030-Struct-Reorg-Add-unsafe-structure-pointer-compressio.patch
Patch31: 0031-AutoBOLT-Support-saving-feedback-count-info-to-ELF-s.patch
Patch32: 0032-AutoBOLT-Add-bolt-linker-plugin-2-3.patch
Patch33: 0033-AutoBOLT-Enable-BOLT-linker-plugin-on-aarch64-3-3.patch
Patch34: 0034-Autofdo-Enable-discrimibator-and-MCF-algorithm-on-Au.patch
Patch35: 0035-Add-insn-defs-and-correct-costs-for-cmlt-generation.patch
Patch36: 0036-rtl-ifcvt-introduce-rtl-ifcvt-enchancements.patch           
Patch37: 0037-Perform-early-if-conversion-of-simple-arithmetic.patch      
Patch38: 0038-Add-option-to-allow-matching-uaddsub-overflow-for-wi.patch  
Patch39: 0039-Match-double-sized-mul-pattern.patch                        
Patch40: 0040-Port-icp-patch-to-GCC-12.patch                              
Patch41: 0041-Port-fixes-in-icp-to-GCC-12.patch
Patch42: 0042-Add-split-complex-instructions-pass.patch                   
Patch43: 0043-Extending-and-refactoring-of-pass_split_complex_inst.patch
Patch44: 0044-Port-maxmin-patch-to-GCC-12.patch
Patch45: 0045-Port-moving-minmask-pattern-to-gimple-to-GCC-12.patch
Patch46: 0046-Add-new-pattern-to-pass-the-maxmin-tests.patch
Patch47: 0047-AES-Implement-AES-pattern-matching.patch
Patch48: 0048-crypto-accel-add-optimization-level-requirement-to-t.patch
Patch49: 0049-Add-more-flexible-check-for-pointer-aliasing-during-.patch
Patch50: 0050-Port-IPA-prefetch-to-GCC-12.patch
Patch51: 0051-Port-fixes-for-IPA-prefetch-to-GCC-12.patch
Patch52: 0052-Fix-fails-in-IPA-prefetch-src-openEuler-gcc-I96ID7.patch
Patch53: 0053-struct-reorg-Add-Semi-Relayout.patch
Patch54: 0054-Struct-Reorg-Bugfix-for-structure-pointer-compressio.patch
Patch55: 0055-Struct-Reorg-Port-bugfixes-to-GCC-12.3.1.patch
Patch56: 0056-Fix-bug-that-verifying-gimple-failed-when-reorg-leve.patch
Patch57: 0057-AutoFdo-Fix-memory-leaks-in-autofdo.patch
Patch86: 0086-Modfify-cost-calculation-for-dealing-with-equivalenc.patch
Patch87: 0087-Add-cost-calculation-for-reg-equivalence-invariants.patch
Patch88: 0088-BUGFIX-Fix-the-configure-file-of-BOLT.patch
Patch89: 0089-StructReorderFields-Fix-gimple-call-not-rewritten.patch
Patch90: 0090-double-sized-mul-testsuite-Add-march-armv8.2-a-for-d.patch
Patch91: 0091-IPA-Bugfix-Fix-fails-in-IPA-prefetch-src-openEuler-g.patch
Patch92: 0092-AES-Bugfix-Change-set_of-to-reg_set_p-and-add-check-.patch
Patch93: 0093-fix-bugs-within-pointer-compression-and-DFE.patch
Patch94: 0094-BUGFIX-AutoBOLT-function-miss-bind-type.patch
Patch95: 0095-STABS-remove-gstabs-and-gxcoff-functionality.patch
Patch96: 0096-Bugfix-Autofdo-use-PMU-sampling-set-num-eauals-den.patch
Patch97: 0097-Improve-non-loop-disambiguation.patch
Patch98: 0098-CHREC-multiplication-and-undefined-overflow.patch
Patch99: 0099-Enable-Transposed-SLP.patch
Patch100: 0100-Add-hip09-machine-discribtion.patch
Patch101: 0101-Add-hip11-CPU-pipeline-scheduling.patch
Patch102: 0102-Add-Crc32-Optimization-in-Gzip-For-crc32-algorithm-i.patch
Patch103: 0103-SME-Remove-hip09-and-hip11-in-aarch64-cores.def-to-b.patch
Patch104: 0104-Backport-SME-AArch64-Cleanup-CPU-option-processing-c.patch
Patch105: 0105-Backport-SME-AArch64-Cleanup-option-processing-code.patch
Patch106: 0106-Backport-SME-aarch64-Add-march-support-for-Armv9.1-A.patch
Patch107: 0107-Backport-SME-Revert-aarch64-Define-__ARM_FEATURE_RCP.patch
Patch108: 0108-Backport-SME-Revert-Ampere-1-and-Ampere-1A-core-defi.patch
Patch109: 0109-Backport-SME-aarch64-Rename-AARCH64_ISA-architecture.patch
Patch110: 0110-Backport-SME-aarch64-Rename-AARCH64_FL-architecture-.patch
Patch111: 0111-Backport-SME-aarch64-Rename-AARCH64_FL_FOR_ARCH-macr.patch
Patch112: 0112-Backport-SME-aarch64-Add-V-to-aarch64-arches.def-nam.patch
Patch113: 0113-Backport-SME-aarch64-Small-config.gcc-cleanups.patch
Patch114: 0114-Backport-SME-aarch64-Avoid-redundancy-in-aarch64-cor.patch
Patch115: 0115-Backport-SME-aarch64-Remove-AARCH64_FL_RCPC8_4-PR107.patch
Patch116: 0116-Backport-SME-aarch64-Fix-transitive-closure-of-featu.patch
Patch117: 0117-Backport-SME-aarch64-Reorder-an-entry-in-aarch64-opt.patch
Patch118: 0118-Backport-SME-aarch64-Simplify-feature-definitions.patch
Patch119: 0119-Backport-SME-aarch64-Simplify-generation-of-.arch-st.patch
Patch120: 0120-Backport-SME-aarch64-Avoid-std-string-in-static-data.patch
Patch121: 0121-Backport-SME-aarch64-Tweak-constness-of-option-relat.patch
Patch122: 0122-Backport-SME-aarch64-Make-more-use-of-aarch64_featur.patch
Patch123: 0123-Backport-SME-aarch64-Tweak-contents-of-flags_on-off-.patch
Patch124: 0124-Backport-SME-aarch64-Tweak-handling-of-mgeneral-regs.patch
Patch125: 0125-Backport-SME-aarch64-Remove-redundant-TARGET_-checks.patch
Patch126: 0126-Backport-SME-aarch64-Define-__ARM_FEATURE_RCPC.patch
Patch127: 0127-Backport-SME-Add-Ampere-1-and-Ampere-1A-core-definit.patch
Patch128: 0128-Backport-SME-aarch64-Fix-nosimd-handling-of-FPR-move.patch
Patch129: 0129-Backport-SME-aarch64-Commonise-some-folding-code.patch
Patch130: 0130-Backport-SME-aarch64-Add-a-Z-operand-modifier-for-SV.patch
Patch131: 0131-Backport-SME-mode-switching-Remove-unused-bbnum-fiel.patch
Patch132: 0132-Backport-SME-mode-switching-Tweak-the-macro-hook-doc.patch
Patch133: 0133-Backport-SME-mode-switching-Add-note-problem.patch
Patch134: 0134-Backport-SME-mode-switching-Avoid-quadractic-list-op.patch
Patch135: 0135-Backport-SME-mode-switching-Fix-the-mode-passed-to-t.patch
Patch136: 0136-Backport-SME-mode-switching-Simplify-recording-of-tr.patch
Patch137: 0137-Backport-SME-mode-switching-Tweak-entry-exit-handlin.patch
Patch138: 0138-Backport-SME-mode-switching-Allow-targets-to-set-the.patch
Patch139: 0139-Backport-SME-mode-switching-Pass-set-of-live-registe.patch
Patch140: 0140-Backport-SME-mode-switching-Pass-the-set-of-live-reg.patch
Patch141: 0141-Backport-SME-mode-switching-Use-1-based-edge-aux-fie.patch
Patch142: 0142-Backport-SME-mode-switching-Add-a-target-configurabl.patch
Patch143: 0143-Backport-SME-mode-switching-Add-a-backprop-hook.patch
Patch144: 0144-Backport-SME-aarch64-Add-a-result_mode-helper-functi.patch
Patch145: 0145-Backport-SME-rtl-Try-to-remove-EH-edges-after-pro-ep.patch
Patch146: 0146-Backport-SME-Fix-PR-middle-end-107705-ICE-after-recl.patch
Patch147: 0147-Backport-SME-function-Change-return-type-of-predicat.patch
Patch148: 0148-Backport-SME-Allow-prologues-and-epilogues-to-be-ins.patch
Patch149: 0149-Backport-SME-Add-a-target-hook-for-sibcall-epilogues.patch
Patch150: 0150-Backport-SME-Add-a-new-target-hook-TARGET_START_CALL.patch
Patch151: 0151-Backport-SME-Allow-targets-to-add-USEs-to-asms.patch
Patch152: 0152-Backport-SME-New-compact-syntax-for-insn-and-insn_sp.patch
Patch153: 0153-Backport-SME-recog-Improve-parser-for-pattern-new-co.patch
Patch154: 0154-Backport-SME-recog-Support-space-in-cons.patch
Patch155: 0155-Backport-SME-aarch64-Generalise-require_immediate_la.patch
Patch156: 0156-Backport-SME-aarch64-Add-backend-support-for-DFP.patch
Patch157: 0157-Backport-SME-aarch64-Vector-move-fixes-for-nosimd.patch
Patch158: 0158-Backport-SME-aarch64-Simplify-output-template-emissi.patch
Patch159: 0159-Backport-SME-Improve-immediate-expansion-PR106583.patch
Patch160: 0160-Backport-SME-AArch64-Cleanup-move-immediate-code.patch
Patch161: 0161-Backport-SME-AArch64-convert-some-patterns-to-compac.patch
Patch162: 0162-Backport-SME-aarch64-Use-SVE-s-RDVL-instruction.patch
Patch163: 0163-Backport-SME-aarch64-Make-AARCH64_FL_SVE-requirement.patch
Patch164: 0164-Backport-SME-aarch64-Add-group-suffixes-to-SVE-intri.patch
Patch165: 0165-Backport-SME-aarch64-Add-sve_type-to-SVE-builtins-co.patch
Patch166: 0166-Backport-SME-aarch64-Generalise-some-SVE-ACLE-error-.patch
Patch167: 0167-Backport-SME-aarch64-Replace-vague-previous-argument.patch
Patch168: 0168-Backport-SME-aarch64-Make-more-use-of-sve_type-in-AC.patch
Patch169: 0169-Backport-SME-aarch64-Tweak-error-message-for-tuple-v.patch
Patch170: 0170-Backport-SME-aarch64-Add-tuple-forms-of-svreinterpre.patch
Patch171: 0171-Backport-SME-attribs-Use-existing-traits-for-excl_ha.patch
Patch172: 0172-Backport-SME-Allow-target-attributes-in-non-gnu-name.patch
Patch173: 0173-Backport-SME-aarch64-Fix-plugin-header-install.patch
Patch174: 0174-Backport-SME-aarch64-Add-arm_streaming-_compatible-a.patch
Patch175: 0175-Backport-SME-aarch64-Add-sme.patch
Patch176: 0176-Backport-SME-aarch64-Add-r-m-and-m-r-alternatives-to.patch
Patch177: 0177-Backport-SME-AArch64-Rewrite-simd-move-immediate-pat.patch
Patch178: 0178-Backport-SME-AArch64-remove-test-comment-from-mov-mo.patch
Patch179: 0179-Backport-SME-aarch64-Distinguish-streaming-compatibl.patch
Patch180: 0180-Backport-SME-aarch64-Mark-relevant-SVE-instructions-.patch
Patch181: 0181-Backport-SME-AArch64-Support-new-tbranch-optab.patch
Patch182: 0182-Backport-SME-aarch64-Use-local-frame-vars-in-shrink-.patch
Patch183: 0183-Backport-SME-aarch64-Avoid-a-use-of-callee_offset.patch
Patch184: 0184-Backport-SME-aarch64-Explicitly-handle-frames-with-n.patch
Patch185: 0185-Backport-SME-aarch64-Add-bytes_below_saved_regs-to-f.patch
Patch186: 0186-Backport-SME-aarch64-Add-bytes_below_hard_fp-to-fram.patch
Patch187: 0187-Backport-SME-aarch64-Robustify-stack-tie-handling.patch
Patch188: 0188-Backport-SME-aarch64-Tweak-aarch64_save-restore_call.patch
Patch189: 0189-Backport-SME-aarch64-Only-calculate-chain_offset-if-.patch
Patch190: 0190-Backport-SME-aarch64-Rename-locals_offset-to-bytes_a.patch
Patch191: 0191-Backport-SME-aarch64-Rename-hard_fp_offset-to-bytes_.patch
Patch192: 0192-Backport-SME-aarch64-Tweak-frame_size-comment.patch
Patch193: 0193-Backport-SME-aarch64-Measure-reg_offset-from-the-bot.patch
Patch194: 0194-Backport-SME-aarch64-Simplify-top-of-frame-allocatio.patch
Patch195: 0195-Backport-SME-aarch64-Minor-initial-adjustment-tweak.patch
Patch196: 0196-Backport-SME-aarch64-Tweak-stack-clash-boundary-cond.patch
Patch197: 0197-Backport-SME-aarch64-Put-LR-save-probe-in-first-16-b.patch
Patch198: 0198-Backport-SME-aarch64-Simplify-probe-of-final-frame-a.patch
Patch199: 0199-Backport-SME-aarch64-Explicitly-record-probe-registe.patch
Patch200: 0200-Backport-SME-aarch64-Remove-below_hard_fp_saved_regs.patch
Patch201: 0201-Backport-SME-aarch64-Make-stack-smash-canary-protect.patch
Patch202: 0202-Backport-SME-Handle-epilogues-that-contain-jumps.patch
Patch203: 0203-Backport-SME-aarch64-Use-vecs-to-store-register-save.patch
Patch204: 0204-Backport-SME-aarch64-Put-LR-save-slot-first-in-more-.patch
Patch205: 0205-Backport-SME-aarch64-Switch-PSTATE.SM-around-calls.patch
Patch206: 0206-Backport-SME-aarch64-Add-support-for-SME-ZA-attribut.patch
Patch207: 0207-Backport-SME-aarch64-Add-a-register-class-for-w12-w1.patch
Patch208: 0208-Backport-SME-aarch64-Add-a-VNx1TI-mode.patch
Patch209: 0209-Backport-SME-aarch64-Generalise-unspec_based_functio.patch
Patch210: 0210-Backport-SME-aarch64-Generalise-_m-rules-for-SVE-int.patch
Patch211: 0211-Backport-SME-aarch64-Add-support-for-arm_sme.h.patch
Patch212: 0212-Backport-SME-aarch64-Add-support-for-__arm_locally_s.patch
Patch213: 0213-Backport-SME-aarch64-Handle-PSTATE.SM-across-abnorma.patch
Patch214: 0214-Backport-SME-aarch64-Enforce-inlining-restrictions-f.patch
Patch215: 0215-Backport-SME-aarch64-Update-sibcall-handling-for-SME.patch
Patch216: 0216-Backport-SME-libgcc-aarch64-Configure-check-for-.var.patch
Patch217: 0217-Backport-SME-libgcc-aarch64-Configure-check-for-__ge.patch
Patch218: 0218-Backport-SME-libgcc-aarch64-Add-SME-runtime-support.patch
Patch219: 0219-Backport-SME-libgcc-aarch64-Add-SME-unwinder-support.patch
Patch220: 0220-Backport-SME-libgcc-Fix-config.in.patch
Patch221: 0221-Backport-SME-aarch64-Add-funwind-tables-to-some-test.patch
Patch222: 0222-Backport-SME-aarch64-Skip-some-SME-register-save-tes.patch
Patch223: 0223-Backport-SME-Add-OPTIONS_H_EXTRA-to-GTFILES.patch
Patch224: 0224-Backport-SME-aarch64-Add-V1DI-mode.patch
Patch225: 0225-Backport-SME-Allow-md-iterators-to-include-other-ite.patch
Patch226: 0226-Backport-SME-riscv-Add-support-for-strlen-inline-exp.patch
Patch227: 0227-Backport-SME-attribs-Add-overloads-with-namespace-na.patch
Patch228: 0228-Backport-SME-vec-Add-array_slice-constructors-from-n.patch
Patch229: 0229-Backport-SME-A-couple-of-va_gc_atomic-tweaks.patch
Patch230: 0230-Backport-SME-middle-end-Fix-issue-of-poly_uint16-1-1.patch
Patch231: 0231-SME-Add-missing-header-file-in-aarch64.cc.patch
Patch232: 0232-Backport-SME-c-Add-support-for-__extension__.patch
Patch233: 0233-Backport-SME-lra-Updates-of-biggest-mode-for-hard-re.patch
Patch234: 0234-Backport-SME-c-Support-C2x-empty-initializer-braces.patch
Patch235: 0235-Backport-SME-aarch64-Update-sizeless-tests-for-recen.patch
Patch236: 0236-Backport-SME-attribs-Namespace-aware-lookup_attribut.patch
Patch237: 0237-Backport-SME-c-family-ICE-with-gnu-nocf_check-PR1069.patch
Patch238: 0238-Backport-SME-AArch64-Fix-assert-in-aarch64_move_imm-.patch
Patch239: 0239-Backport-SME-testsuite-Only-run-fcf-protection-test-.patch
Patch240: 0240-Backport-SME-Fix-PRs-106764-106765-and-107307-all-IC.patch
Patch241: 0241-Backport-SME-aarch64-Remove-expected-error-for-compo.patch
Patch242: 0242-Backport-SME-aarch64-Remove-redundant-builtins-code.patch
Patch243: 0243-Backport-SME-AArch64-Fix-Armv9-a-warnings-that-get-e.patch
Patch244: 0244-Backport-SME-Canonicalize-X-Y-as-X-Y-in-match.pd-whe.patch
Patch245: 0245-Backport-SME-middle-end-Add-new-tbranch-optab-to-add.patch
Patch246: 0246-Backport-SME-explow-Allow-dynamic-allocations-after-.patch
Patch247: 0247-Backport-SME-PR105169-Fix-references-to-discarded-se.patch
Patch248: 0248-Backport-SME-RISC-V-autovec-Verify-that-GET_MODE_NUN.patch
Patch249: 0249-Backport-SME-Add-operator-to-gimple_stmt_iterator-an.patch
Patch250: 0250-Backport-SME-tree-optimization-110221-SLP-and-loop-m.patch
Patch251: 0251-SME-Adapt-some-testsuites.patch
Patch252: 0252-SME-Fix-error-by-backported-patches-and-IPA-prefetch.patch
Patch253: 0253-aarch64-Fix-return-register-handling-in-untyped_call.patch
Patch254: 0254-aarch64-Fix-loose-ldpstp-check.patch
Patch255: 0255-x86-Add-a-new-option-mdaz-ftz-to-enable-FTZ-and-DAZ-.patch
Patch256: 0256-Explicitly-view_convert_expr-mask-to-signed-type-whe.patch
Patch257: 0257-Make-option-mvzeroupper-independent-of-optimization-.patch
Patch258: 0258-i386-Sync-tune_string-with-arch_string-for-target-at.patch
Patch259: 0259-Refine-maskloadmn-pattern-with-UNSPEC_MASKLOAD.patch
Patch260: 0260-Refine-maskstore-patterns-with-UNSPEC_MASKMOV.patch
Patch261: 0261-x86-Update-model-values-for-Alderlake-and-Rocketlake.patch
Patch262: 0262-Workaround-possible-CPUID-bug-in-Sandy-Bridge.patch
Patch263: 0263-Software-mitigation-Disable-gather-generation-in-vec.patch
Patch264: 0264-Support-m-no-gather-m-no-scatter-to-enable-disable-v.patch
Patch265: 0265-Remove-constraint-modifier-for-fcmaddcph-fmaddcph-fc.patch
Patch266: 0266-Disparage-slightly-for-the-alternative-which-move-DF.patch
Patch267: 0267-Fix-wrong-code-due-to-vec_merge-pcmp-to-blendvb-spli.patch
Patch268: 0268-Don-t-assume-it-s-AVX_U128_CLEAN-after-call_insn-who.patch
Patch269: 0269-Disable-FMADD-in-chains-for-Zen4-and-generic.patch
Patch270: 0270-Initial-Raptorlake-Support.patch
Patch271: 0271-Initial-Meteorlake-Support.patch
Patch272: 0272-Support-Intel-AMX-FP16-ISA.patch
Patch273: 0273-Support-Intel-prefetchit0-t1.patch
Patch274: 0274-Initial-Granite-Rapids-Support.patch
Patch275: 0275-Support-Intel-AMX-COMPLEX.patch
Patch276: 0276-i386-Add-AMX-COMPLEX-to-Granite-Rapids.patch
Patch277: 0277-Initial-Granite-Rapids-D-Support.patch
Patch278: 0278-Correct-Granite-Rapids-D-documentation.patch
Patch279: 0279-i386-Remove-Meteorlake-s-family_model.patch
Patch280: 0280-x86-Update-model-values-for-Alderlake-Rocketlake-and.patch
Patch281: 0281-x86-Update-model-values-for-Raptorlake.patch
Patch282: 0282-Fix-target_clone-arch-graniterapids-d.patch
Patch283: 0283-i386-Change-prefetchi-output-template.patch
Patch284: 0284-i386-Add-non-optimize-prefetchi-intrins.patch
Patch285: 0285-SME-Recover-hip09-and-hip11-in-aarch64-cores.def.patch
Patch286: 0286-Try-to-use-AI-model-to-guide-optimization.patch
Patch287: 0287-Add-dynamic-memory-access-checks.patch
Patch288: 0288-Enable-macro-use-commandline.patch
Patch289: 0289-tree-ssa-loop-crc.cc-TARGET_CRC32-may-be-not-defined.patch
Patch290: 0290-Add-ipa-prefetch-test-for-gcc-s-case.patch
Patch291: 0291-Fix-settings-for-wide-operations-tests.patch
Patch292: 0292-Fix-errors-in-ipa-prefetch-IAORPF-and-IAOSJ0.patch
Patch293: 0293-Fix-error-with-stmts-insertion-in-ipa-prefetch-for-I.patch
Patch294: 0294-Fix-errors-in-ipa-prefetch-IAO50J-and-IAO5H7.patch
Patch295: 0295-Fix-error-with-grouped_load-merge-in-slp-transpose-v.patch
Patch296: 0296-Fix-error-in-slp-transpose-vectorize-for-IAQFM3.patch
Patch297: 0297-Fix-grouped-load-merging-error-in-SLP-transpose-vectorization.patch
Patch298: 0298-Mark-prefetch-builtin-as-willreturn.patch
Patch299: 0299-Backport-Disallow-pointer-operands-for-and-partly-PR.patch
Patch300: 0300-Remove-erroneous-pattern-from-gimple-ifcvt.patch
Patch301: 0301-Add-required-check-for-iteration-through-uses.patch
Patch302: 0302-Added-param-for-optimization-for-merging-bb-s-with-c.patch
Patch303: 0303-Add-generation-of-stream-in-functions-for-pre-versio.patch
Patch304: 0304-Add-multi-version-lto-symbol-parse-cross-lto-units-i.patch
Patch305: 0305-Backport-varasm-Handle-private-COMDAT-function-symbo.patch
Patch306: 0306-RISC-V-Install-libstdc-libcc1-etc-to-lib64-instead-o.patch
Patch307: 0307-Set-fallback-value-for-print-multi-os-directory.patch
Patch308: 0308-Fix-enum-INPUT-MIDDLE-FINAL-aes_stage.patch
Patch309: 0309-CSPGO-Add-context-sensitive-PGO.patch
Patch310: 0310-CFGO-Add-cfgo-pgo-optimization.patch
Patch311: 0311-PATCH-Add-if-split-optimization-pass.patch
Patch312: 0312-Add-late-slp-vectorization-pass-with-additional-chec.patch
Patch313: 0313-Add-tracer-transformation-for-static-probabilities.patch
Patch314: 0314-bugfix-Modify-the-hip09-tune-flags.patch
Patch315: 0315-Bugfix-Add-no-var-recored-check-for-ssa_name-in-stru.patch
Patch316: 0316-Use-ai-ability-to-guide-optimization.patch
Patch317: 0317-Bugfix-set-default-value-when-tune_native-is-NULL.patch
Patch318: 0318-add-flag-flto-try.patch 
Patch319: 0319-CSPGO-fix-bugs-when-using-cspgo.patch
Patch320: 0320-if-split-fix-bugs.patch
Patch321: 0321-Struct-reorg-Avoid-doing-struct-split-and-reorder_fi.patch
Patch322: 0322-Bugfix-Create-POINTER_PLUS_EXPR-for-REFERENCE_TYPE.patch
Patch323: 0323-Bugfix-replace-tmp-pattern-split.patch
Patch324: 0324-bugfix-fix-vector-costs-for-hip09.patch
Patch325: 0325-gcc-opts-common.cc-Fix-build-with-clang.patch
Patch326: 0326-BUGFIX-Fix-build-error-on-risv_64.patch
Patch327: 0327-Bugfix-Adjust-the-same-gate-to-use-struct-option.patch
Patch328: 0328-Bugfix-if-split-Added-checking-for-ssa_name.patch
Patch329: 0329-Fixed-work-with-loops-in-process_complex_cond.patch
Patch330: 0330-bugfix-fix-typo-error.patch
Patch331: 0331-fix-function-missing-return-value.patch
Patch332: 0332-Bugfix-Can-not-find-fdata-file.patch
Patch333: 0333-CSPGO-Update-the-gate-of-cspgo.patch
Patch334: 0334-Dont-use-local_detect_cpu-when-cross-build.patch
Patch335: 0335-fix-costs-for-hip09.patch
Patch336: 0336-sfc-Add-struct-static-field-compression-optimization.patch
Patch337: 0337-Reduce-ipa-inline-warning-output.patch
Patch338: 0338-i386-Fix-AVX512-intrin-macro-typo.patch
Patch339: 0339-i386-Use-_mm_setzero_ps-d-instead-of-_mm_avx512_setz.patch
Patch340: 0340-Refine-constraint-Bk-to-define_special_memory_constr.patch
Patch341: 0341-Align-ix86_-move_max-store_max-with-vectorizer.patch
Patch342: 0342-Fix-testcase-failure.patch
Patch343: 0343-Check-avx-upper-register-for-parallel.patch
Patch344: 0344-i386-Fix-vfpclassph-non-optimizied-intrin.patch
Patch345: 0345-doc-Add-more-alias-option-and-reorder-Intel-CPU-marc.patch
Patch346: 0346-Refine-splitters-related-to-combine-vpcmpuw-zero_ext.patch
Patch347: 0347-Fix-ICE-due-to-isa-mismatch-for-the-builtins.patch
Patch348: 0348-Fix-ICE-due-to-subreg-us_truncate.patch
Patch349: 0349-i386-Zero-extend-32-bit-address-to-64-bit-with-optio.patch
Patch350: 0350-Fix-uninitialized-operands-2-in-vec_unpacks_hi_v4sf.patch
Patch351: 0351-GCC13-GCC12-Fix-testcase.patch
Patch352: 0352-Add-hip10c-machine-discription.patch
Patch353: 0353-Add-hip10a-machine-discription.patch
Patch354: 0354-Fix-for-hip11-and-hip10c-addrcost_table.patch
Patch355: 0355-Fix-errors-in-ipa-struct-sfc-IBMY84-IBN2JO-IBN42Q.patch
Patch356: 0356-Avoid-doing-sfc-with-struct_split-and-compressing-de.patch
Patch357: 0357-struct-reorg-disable-malloc-support-when-struct_layo.patch
Patch358: 0358-struct-reorg-fix-residual-ssa_name-issue.patch

# Part 1001-1999
%ifarch sw_64
Patch1001: 0001-Sw64-Port-add-configure-support-for-sw64.patch
Patch1002: 0002-Sw64-Port-add-gcc-compiler.patch
Patch1003: 0003-Sw64-Port-add-multi-prefetch-support-for-sw64.patch
Patch1004: 0004-Sw64-Port-update-gcc-testsuite-for-sw64.patch
Patch1005: 0005-Sw64-Port-libatomic.patch
Patch1006: 0006-Sw64-Port-libgcc.patch
Patch1007: 0007-Sw64-Port-libffi.patch
Patch1008: 0008-Sw64-Port-libgfortran.patch
Patch1009: 0009-Sw64-Port-libgo.patch
Patch1010: 0010-Sw64-Port-libgomp.patch
Patch1011: 0011-Sw64-Port-libitm.patch
Patch1012: 0012-Sw64-Port-libstdc.patch
Patch1013: 0013-Sw64-Port-set-raise-FPE-when-DivbyZero-on-Sw_64.patch
Patch1014: 0014-Sw64-Port-add-lex-builtin-support-in-libcpp.patch
Patch1015: 0015-Sw64-Port-libsanitizer.patch
Patch1016: 0016-libsanitizer-fix-isoc23-function-interception.patch
Patch1017: 0017-Sw64-Port-Fix-target-explicit_mask.patch
%endif

# Part 3000 ~ 4999
%ifarch loongarch64
Patch3001: loongarch-add-alternatives-for-idiv-insns-to-improve.patch
Patch3002: loongarch-avoid-unnecessary-sign-extend-after-32-bit.patch
Patch3003: LoongArch-Subdivision-symbol-type-add-SYMBOL_PCREL-s.patch
Patch3004: LoongArch-Support-split-symbol.patch
Patch3005: LoongArch-Modify-the-output-message-string-of-the-wa.patch
Patch3006: LoongArch-adjust-the-default-of-mexplicit-relocs-by-.patch
Patch3007: LoongArch-document-m-no-explicit-relocs.patch
Patch3008: LoongArch-Define-the-macro-ASM_PREFERRED_EH_DATA_FOR.patch
Patch3009: LoongArch-Provide-fmin-fmax-RTL-pattern.patch
Patch3010: LoongArch-Get-__tls_get_addr-address-through-got-tab.patch
Patch3011: LoongArch-Add-support-code-model-extreme.patch
Patch3012: LoongArch-Add-new-code-model-medium.patch
Patch3013: LoongArch-Avoid-RTL-flag-check-failure-in-loongarch_.patch
Patch3014: LoongArch-add-model-attribute.patch
Patch3015: LoongArch-testsuite-refine-__tls_get_addr-tests-with.patch
Patch3016: LoongArch-add-mdirect-extern-access-option.patch
Patch3017: LoongArch-Fix-pr106828-by-define-hook-TARGET_ASAN_SH.patch
Patch3018: LoongArch-Prepare-static-PIE-support.patch
Patch3019: LoongArch-Libitm-add-LoongArch-support.patch
Patch3020: LoongArch-Fixed-a-typo-in-the-comment-information-of.patch
Patch3021: LoongArch-Use-UNSPEC-for-fmin-fmax-RTL-pattern-PR105.patch
Patch3022: LoongArch-Fixed-a-bug-in-the-loongarch-architecture-.patch
Patch3023: LoongArch-implement-count_-leading-trailing-_zeros.patch
Patch3024: Libvtv-Add-loongarch-support.patch
Patch3025: LoongArch-Add-fcopysign-instructions.patch
Patch3026: LoongArch-fix-signed-overflow-in-loongarch_emit_int_.patch
Patch3027: LoongArch-Rename-frint_-fmt-to-rint-mode-2.patch
Patch3028: LoongArch-Add-ftint-rm-rp-.-w-l-.-s-d-instructions.patch
Patch3029: LoongArch-Add-fscaleb.-s-d-instructions-as-ldexp-sf-.patch
Patch3030: LoongArch-Add-flogb.-s-d-instructions-and-expand-log.patch
Patch3031: LoongArch-Add-prefetch-instructions.patch
Patch3032: LoongArch-Optimize-immediate-load.patch
Patch3033: LoongArch-Optimize-the-implementation-of-stack-check.patch
Patch3034: LoongArch-Fixed-a-compilation-failure-with-c-in-inli.patch
Patch3035: LoongArch-Don-t-add-crtfastmath.o-for-shared.patch
Patch3036: LoongArch-Generate-bytepick.-wd-for-suitable-bit-ope.patch
Patch3037: LoongArch-Change-the-value-of-macro-TRY_EMPTY_VM_SPA.patch
Patch3038: LoongArch-testsuite-Disable-stack-protector-for-some.patch
Patch3039: LoongArch-Add-built-in-functions-description-of-Loon.patch
Patch3040: LoongArch-Remove-the-definition-of-the-macro-LOGICAL.patch
Patch3041: LoongArch-Optimize-additions-with-immediates.patch
Patch3042: LoongArch-Improve-GAR-store-for-va_list.patch
Patch3043: LoongArch-Improve-cpymemsi-expansion-PR109465.patch
Patch3044: LoongArch-Fix-MUSL_DYNAMIC_LINKER.patch
Patch3045: LoongArch-Enable-shrink-wrapping.patch
Patch3046: LoongArch-Change-the-default-value-of-LARCH_CALL_RAT.patch
Patch3047: LoongArch-Set-default-alignment-for-functions-and-la.patch
Patch3048: LoongArch-Avoid-non-returning-indirect-jumps-through.patch
Patch3049: Loongarch-Fix-plugin-header-missing-install.patch
Patch3050: libffi-Backport-of-LoongArch-support-for-libffi.patch
Patch3051: LoongArch-Remove-redundant-sign-extension-instructio.patch
Patch3052: LoongArch-Enable-free-starting-at-O2.patch
Patch3053: LoongArch-Fix-bug-in-loongarch_emit_stack_tie-PR1104.patch
Patch3054: LoongArch-Implement-128-bit-floating-point-functions.patch
Patch3056: LoongArch-Optimize-switch-with-sign-extended-index.patch
Patch3057: LoongArch-Support-storing-floating-point-zero-into-M.patch
Patch3058: LoongArch-improved-target-configuration-interface.patch
Patch3059: LoongArch-define-preprocessing-macros-__loongarch_-a.patch
Patch3060: LoongArch-add-new-configure-option-with-strict-align.patch
Patch3061: LoongArch-support-loongarch-elf-target.patch
Patch3062: LoongArch-initial-ada-support-on-linux.patch
Patch3063: LoongArch-Add-Loongson-SX-base-instruction-support.patch
Patch3064: LoongArch-Add-Loongson-SX-directive-builtin-function.patch
Patch3065: LoongArch-Add-Loongson-ASX-base-instruction-support.patch
Patch3066: LoongArch-Add-Loongson-ASX-directive-builtin-functio.patch
Patch3067: LoongArch-Fix-unintentionally-breakage-in-r14-3665.patch
Patch3068: LoongArch-Use-bstrins-instruction-for-a-mask-and-a-m.patch
Patch3069: LoongArch-Adjust-C-multilib-header-layout.patch
Patch3070: LoongArch-Fix-unintentional-bash-ism-in-r14-3665.patch
Patch3071: LoongArch-Enable-fsched-pressure-by-default-at-O1-an.patch
Patch3072: LoongArch-Use-LSX-and-LASX-for-block-move.patch
Patch3073: LoongArch-Slightly-simplify-loongarch_block_move_str.patch
Patch3074: LoongArch-Optimized-multiply-instruction-generation.patch
Patch3075: LoongArch-Fix-up-memcpy-vec-3.c-test-case.patch
Patch3076: LoongArch-Add-tests-of-mstrict-align-option.patch
Patch3077: LoongArch-Add-testsuite-framework-for-Loongson-SX-AS.patch
Patch3078: LoongArch-Add-tests-for-Loongson-SX-builtin-function.patch
Patch3079: LoongArch-Add-tests-for-SX-vector-floating-point-ins.patch
Patch3080: LoongArch-Add-tests-for-SX-vector-addition-instructi.patch
Patch3081: LoongArch-Add-tests-for-SX-vector-subtraction-instru.patch
Patch3082: LoongArch-Add-tests-for-SX-vector-addition-vsadd-ins.patch
Patch3083: LoongArch-Add-tests-for-the-SX-vector-multiplication.patch
Patch3084: LoongArch-Add-tests-for-SX-vector-vavg-vavgr-instruc.patch
Patch3085: LoongArch-Add-tests-for-SX-vector-vmax-vmaxi-vmin-vm.patch
Patch3086: LoongArch-Add-tests-for-SX-vector-vexth-vextl-vldi-v.patch
Patch3087: LoongArch-Add-tests-for-SX-vector-vabsd-vmskgez-vmsk.patch
Patch3088: LoongArch-Add-tests-for-SX-vector-vdiv-vmod-instruct.patch
Patch3089: LoongArch-Add-tests-for-SX-vector-vsll-vslli-vsrl-vs.patch
Patch3090: LoongArch-Add-tests-for-SX-vector-vrotr-vrotri-vsra-.patch
Patch3091: LoongArch-Add-tests-for-SX-vector-vssran-vssrani-vss.patch
Patch3092: LoongArch-Add-tests-for-SX-vector-vbitclr-vbitclri-v.patch
Patch3093: LoongArch-Add-tests-for-SX-vector-floating-point-ari.patch
Patch3094: LoongArch-Add-tests-for-SX-vector-vfrstp-vfrstpi-vse.patch
Patch3095: LoongArch-Add-tests-for-SX-vector-vfcmp-instructions.patch
Patch3096: LoongArch-Add-tests-for-SX-vector-handling-and-shuff.patch
Patch3097: LoongArch-Add-tests-for-SX-vector-vand-vandi-vandn-v.patch
Patch3098: LoongArch-Add-tests-for-SX-vector-vfmadd-vfnmadd-vld.patch
Patch3099: LoongArch-Add-tests-for-ASX-vector-xvadd-xvadda-xvad.patch
Patch3100: LoongArch-Add-tests-for-ASX-vector-xvhadd-xvhaddw-xv.patch
Patch3101: LoongArch-Add-tests-for-ASX-vector-subtraction-instr.patch
Patch3102: LoongArch-Add-tests-for-ASX-vector-xvmul-xvmod-xvdiv.patch
Patch3103: LoongArch-Add-tests-for-ASX-vector-xvmax-xvmaxi-xvmi.patch
Patch3104: LoongArch-Add-tests-for-ASX-vector-xvldi-xvmskgez-xv.patch
Patch3105: LoongArch-Add-tests-for-ASX-vector-xvand-xvandi-xvan.patch
Patch3106: LoongArch-Add-tests-for-ASX-vector-xvsll-xvsrl-instr.patch
Patch3107: LoongArch-Add-tests-for-ASX-vector-xvextl-xvsra-xvsr.patch
Patch3108: LoongArch-Add-tests-for-ASX-vector-xvbitclr-xvbitclr.patch
Patch3109: LoongArch-Add-tests-for-ASX-builtin-functions.patch
Patch3110: LoongArch-Add-tests-for-ASX-xvldrepl-xvstelm-instruc.patch
Patch3111: LoongArch-Add-tests-for-ASX-vector-floating-point-op.patch
Patch3112: LoongArch-Add-tests-for-ASX-vector-floating-point-co.patch
Patch3113: LoongArch-Add-tests-for-ASX-vector-comparison-and-se.patch
Patch3114: LoongArch-Add-tests-for-ASX-vector-xvfnmadd-xvfrstp-.patch
Patch3115: LoongArch-Add-tests-for-ASX-vector-xvabsd-xvavg-xvav.patch
Patch3116: LoongArch-Add-tests-for-ASX-vector-xvfcmp-caf-ceq-cl.patch
Patch3117: LoongArch-Add-tests-for-ASX-vector-xvfcmp-saf-seq-sl.patch
Patch3118: LoongArch-Add-tests-for-ASX-vector-xvext2xv-xvexth-x.patch
Patch3119: LoongArch-Add-tests-for-ASX-vector-xvpackev-xvpackod.patch
Patch3120: LoongArch-Add-tests-for-ASX-vector-xvssrln-xvssrlni-.patch
Patch3121: LoongArch-Add-tests-for-ASX-vector-xvssran-xvssrani-.patch
Patch3122: LoongArch-Fix-bug-of-optab-di3_fake.patch
Patch3123: LoongArch-Change-the-value-of-branch_cost-from-2-to-.patch
Patch3124: libsanitizer-add-LoongArch-support.patch
Patch3125: LoongArch-fix-error-building.patch
Patch3126: libjccjit-do-not-link-objects-contained-same-element.patch
Patch3127: LoongArch-Use-finer-grained-DBAR-hints.patch
Patch3128: LoongArch-Add-LA664-support.patch
Patch3129: LoongArch-Fix-internal-error-running-gcc-march-nativ.patch
Patch3130: LoongArch-Fix-lsx-vshuf.c-and-lasx-xvshuf_b.c-tests-.patch
# -- Update to master, lastest commit: 60e99901aef8e7efd4d60adf9f82021fcbd1101f
Patch3131: 0001-LoongArch-Reimplement-multilib-build-option-handling.patch
Patch3132: 0002-LoongArch-Check-whether-binutils-supports-the-relax-.patch
Patch3133: 0003-Modify-gas-uleb128-support-test.patch
Patch3134: 0004-LoongArch-Optimizations-of-vector-construction.patch
Patch3135: 0005-LoongArch-Replace-UNSPEC_FCOPYSIGN-with-copysign-RTL.patch
Patch3136: 0006-LoongArch-Adjust-makefile-dependency-for-loongarch-h.patch
Patch3137: 0007-LoongArch-Enable-vect.exp-for-LoongArch.-PR111424.patch
Patch3138: 0008-LoongArch-Delete-macro-definition-ASM_OUTPUT_ALIGN_W.patch
Patch3139: 0009-LoongArch-Fix-vec_initv32qiv16qi-template-to-avoid-I.patch
Patch3140: 0010-LoongArch-Use-fcmp.caf.s-instead-of-movgr2cf-for-zer.patch
Patch3141: 0011-LoongArch-Implement-avg-and-sad-standard-names.patch
Patch3142: 0012-LoongArch-Implement-vec_widen-standard-names.patch
Patch3143: 0013-LoongArch-Implement-the-new-vector-cost-model-framew.patch
Patch3144: 0014-LoongArch-Define-macro-CLEAR_INSN_CACHE.patch
Patch3145: 0015-LoongArch-Add-enum-style-mexplicit-relocs-option.patch
Patch3146: 0016-LoongArch-Use-explicit-relocs-for-GOT-access-when-me.patch
Patch3147: 0017-LoongArch-Use-explicit-relocs-for-TLS-access-with-me.patch
Patch3148: 0018-LoongArch-Use-explicit-relocs-for-addresses-only-use.patch
Patch3149: 0019-LoongArch-Implement-__builtin_thread_pointer-for-TLS.patch
Patch3150: 0020-LoongArch-Fix-vfrint-releated-comments-in-lsxintrin..patch
Patch3151: 0021-LoongArch-Enable-vcond_mask_mn-expanders-for-SF-DF-m.patch
Patch3152: 0022-LoongArch-Define-HAVE_AS_TLS-to-0-if-it-s-undefined-.patch
Patch3153: 0023-LoongArch-Fix-instruction-name-typo-in-lsx_vreplgr2v.patch
Patch3154: 0024-LoongArch-Use-simplify_gen_subreg-instead-of-gen_rtx.patch
Patch3155: 0025-LoongArch-Optimize-single-used-address-with-mexplici.patch
Patch3156: 0026-LoongArch-Disable-relaxation-if-the-assembler-don-t-.patch
Patch3157: 0027-LoongArch-Remove-redundant-barrier-instructions-befo.patch
Patch3158: 0028-LoongArch-Fix-scan-assembler-times-of-lasx-lsx-test-.patch
Patch3159: 0029-LoongArch-Increase-cost-of-vector-aligned-store-load.patch
Patch3160: 0030-LoongArch-Implement-C-LT-Z_DEFINED_VALUE_AT_ZERO.patch
Patch3161: 0031-LoongArch-Handle-vectorized-copysign-x-1-expansion-e.patch
Patch3162: 0032-LoongArch-Add-code-generation-support-for-call36-fun.patch
Patch3163: 0033-LoongArch-Implement-atomic-operations-using-LoongArc.patch
Patch3164: 0034-LoongArch-atomic_load-and-atomic_store-are-implement.patch
Patch3165: 0035-LoongArch-genopts-Add-infrastructure-to-generate-cod.patch
Patch3166: 0036-LoongArch-Add-evolution-features-of-base-ISA-revisio.patch
Patch3167: 0037-LoongArch-Take-the-advantage-of-mdiv32-if-it-s-enabl.patch
Patch3168: 0038-LoongArch-Don-t-emit-dbar-0x700-if-mld-seq-sa.patch
Patch3169: 0039-LoongArch-Add-fine-grained-control-for-LAM_BH-and-LA.patch
Patch3170: 0040-LoongArch-Fix-mexplict-relocs-none-mcmodel-medium-pr.patch
Patch3171: 0041-LoongArch-Modify-MUSL_DYNAMIC_LINKER.patch
Patch3172: 0042-LoongArch-Fix-libgcc-build-failure-when-libc-is-not-.patch
Patch3173: 0043-LoongArch-Optimize-LSX-vector-shuffle-on-floating-po.patch
Patch3174: 0044-LoongArch-Optimize-the-loading-of-immediate-numbers-.patch
Patch3175: 0045-LoongArch-Fix-runtime-error-in-a-gcc-build-with-with.patch
Patch3176: 0046-LoongArch-Fix-usage-of-LSX-and-LASX-frint-ftint-inst.patch
Patch3177: 0047-LoongArch-Use-standard-pattern-name-and-RTX-code-for.patch
Patch3178: 0048-LoongArch-Use-standard-pattern-name-and-RTX-code-for.patch
Patch3179: 0049-LoongArch-Remove-lrint_allow_inexact.patch
Patch3180: 0050-LoongArch-Use-LSX-for-scalar-FP-rounding-with-explic.patch
Patch3181: 0051-LoongArch-Remove-duplicate-definition-of-CLZ_DEFINED.patch
Patch3182: 0052-LoongArch-Added-vectorized-hardware-inspection-for-t.patch
Patch3183: 0053-LoongArch-Accelerate-optimization-of-scalar-signed-u.patch
Patch3184: 0054-LoongArch-Optimize-vector-constant-extract-even-odd-.patch
Patch3185: 0055-LoongArch-Add-intrinsic-function-descriptions-for-LS.patch
Patch3186: 0056-LoongArch-Switch-loongarch-def-from-C-to-C-to-make-i.patch
Patch3187: 0057-LoongArch-Remove-the-definition-of-ISA_BASE_LA64V110.patch
Patch3188: 0058-LoongArch-Add-support-for-xorsign.patch
Patch3189: 0059-LoongArch-Add-support-for-LoongArch-V1.1-approximate.patch
Patch3190: 0060-LoongArch-Use-standard-pattern-name-for-xvfrsqrt-vfr.patch
Patch3191: 0061-LoongArch-Redefine-pattern-for-xvfrecip-vfrecip-inst.patch
Patch3192: 0062-LoongArch-New-options-mrecip-and-mrecip-with-ffast-m.patch
Patch3193: 0063-LoongArch-Vectorized-loop-unrolling-is-disable-for-d.patch
Patch3194: 0064-LoongArch-Fix-lsx-vshuf.c-and-lasx-xvshuf_b.c-tests-.patch
Patch3195: 0065-LoongArch-Fix-ICE-and-use-simplify_gen_subreg-instea.patch
Patch3196: 0066-LoongArch-Fix-eh_return-epilogue-for-normal-returns.patch
Patch3197: 0067-LoongArch-Allow-mcmodel-extreme-and-model-attribute-.patch
Patch3198: 0068-LoongArch-Fix-warnings-building-libgcc.patch
Patch3199: 0069-LoongArch-testsuite-Remove-XFAIL-in-vect-ftint-no-in.patch
Patch3200: 0070-LoongArch-Include-rtl.h-for-COSTS_N_INSNS-instead-of.patch
Patch3201: 0071-LoongArch-Fix-instruction-costs-PR112936.patch
Patch3202: 0072-LoongArch-Add-alslsi3_extend.patch
Patch3203: 0073-LoongArch-Add-support-for-D-frontend.patch
Patch3204: 0074-libruntime-Add-fiber-context-switch-code-for-LoongAr.patch
Patch3205: 0075-LoongArch-Fix-FP-vector-comparsons-PR113034.patch
Patch3206: 0076-LoongArch-Use-force_reg-instead-of-gen_reg_rtx-emit_.patch
Patch3207: 0077-LoongArch-Clean-up-vec_init-expander.patch
Patch3208: 0078-LoongArch-Fix-incorrect-code-generation-for-sad-patt.patch
Patch3209: 0079-LoongArch-Modify-the-check-type-of-the-vector-builti.patch
Patch3210: 0080-LoongArch-extend.texi-Fix-typos-in-LSX-intrinsics.patch
Patch3211: 0081-LoongArch-Fix-builtin-function-prototypes-for-LASX-i.patch
Patch3212: 0082-LoongArch-Add-asm-modifiers-to-the-LSX-and-LASX-dire.patch
Patch3213: 0083-LoongArch-Implement-FCCmode-reload-and-cstore-ANYF-m.patch
Patch3214: 0084-LoongArch-Add-sign_extend-pattern-for-32-bit-rotate-.patch
Patch3215: 0085-LoongArch-Fixed-bug-in-bstrins_-mode-_for_ior_mask-t.patch
Patch3216: 0086-LoongArch-Fix-insn-output-of-vec_concat-templates-fo.patch
Patch3217: 0087-LoongArch-Fix-ICE-when-passing-two-same-vector-argum.patch
Patch3218: 0088-LoongArch-Expand-left-rotate-to-right-rotate-with-ne.patch
Patch3219: 0089-LoongArch-Fix-infinite-secondary-reloading-of-FCCmod.patch
Patch3220: 0090-LoongArch-Replace-mexplicit-relocs-auto-simple-used-.patch
Patch3221: 0091-LoongArch-Fix-the-format-of-bstrins_-mode-_for_ior_m.patch
Patch3222: 0092-LoongArch-Added-TLS-Le-Relax-support.patch
Patch3223: 0093-LoongArch-Provide-fmin-fmax-RTL-pattern-for-vectors.patch
Patch3224: 0094-LoongArch-Merge-constant-vector-permuatation-impleme.patch
Patch3225: 0095-LoongArch-testsuite-Fix-FAIL-in-lasx-xvstelm.c-file.patch
Patch3226: 0096-LoongArch-testsuite-Modify-the-test-behavior-of-the-.patch
Patch3227: 0097-LoongArch-testsuite-Delete-the-default-run-behavior-.patch
Patch3228: 0098-LoongArch-testsuite-Added-additional-vectorization-m.patch
Patch3229: 0099-LoongArch-testsuite-Give-up-the-detection-of-the-gcc.patch
Patch3230: 0100-LoongArch-Fixed-the-problem-of-incorrect-judgment-of.patch
Patch3231: 0101-LoongArch-Improve-lasx_xvpermi_q_-LASX-mode-insn-pat.patch
Patch3232: 0102-LoongArch-Implement-vec_init-M-N-where-N-is-a-LSX-ve.patch
Patch3233: 0103-LoongArch-Handle-ISA-evolution-switches-along-with-o.patch
Patch3234: 0104-LoongArch-Rename-ISA_BASE_LA64V100-to-ISA_BASE_LA64.patch
Patch3235: 0105-LoongArch-Use-enums-for-constants.patch
Patch3236: 0106-LoongArch-Simplify-mexplicit-reloc-definitions.patch
Patch3237: 0107-LoongArch-testsuite-Add-loongarch-support-to-slp-21..patch
Patch3238: 0108-LoongArch-Optimized-some-of-the-symbolic-expansion-i.patch
Patch3239: 0109-LoongArch-Implement-option-save-restore.patch
Patch3240: 0110-LoongArch-Redundant-sign-extension-elimination-optim.patch
Patch3241: 0111-LoongArch-Redundant-sign-extension-elimination-optim.patch
Patch3242: 0112-LoongArch-Assign-the-u-attribute-to-the-mem-to-which.patch
Patch3243: 0113-LoongArch-testsuite-Fix-fail-in-gen-vect-2-25-.c-fil.patch
Patch3244: 0114-LoongArch-Remove-constraint-z-from-movsi_internal.patch
Patch3245: 0115-LoongArch-doc-Add-attribute-descriptions-defined-in-.patch
Patch3246: 0116-LoongArch-Disable-explicit-reloc-for-TLS-LD-GD-with-.patch
Patch3247: 0117-LoongArch-testsuite-Disable-stack-protector-for-got-.patch
Patch3248: 0118-LoongArch-Disable-TLS-type-symbols-from-generating-n.patch
Patch3249: 0119-LoongArch-Remove-vec_concatz-mode-pattern.patch
Patch3250: 0120-LoongArch-Optimize-implementation-of-single-precisio.patch
Patch3251: 0121-LoongArch-Define-LOGICAL_OP_NON_SHORT_CIRCUIT.patch
Patch3252: 0122-LoongArch-Split-vec_selects-of-bottom-elements-into-.patch
Patch3253: 0123-LoongArch-Modify-the-address-calculation-logic-for-o.patch
Patch3254: 0124-LoongArch-Merge-template-got_load_tls_-ld-gd-le-ie.patch
Patch3255: 0125-LoongArch-Add-the-macro-implementation-of-mcmodel-ex.patch
Patch3256: 0126-LoongArch-Enable-explicit-reloc-for-extreme-TLS-GD-L.patch
Patch3257: 0127-LoongArch-Added-support-for-loading-__get_tls_addr-s.patch
Patch3258: 0128-LoongArch-Don-t-split-the-instructions-containing-re.patch
Patch3259: 0129-LoongArch-Adjust-cost-of-vector_stmt-that-match-mult.patch
Patch3260: 0130-LoongArch-Fix-incorrect-return-type-for-frecipe-frsq.patch
Patch3261: 0131-LoongArch-Fix-an-ODR-violation.patch
Patch3262: 0132-LoongArch-testsuite-Fix-gcc.dg-vect-vect-reduc-mul_-.patch
Patch3263: 0133-LoongArch-Avoid-out-of-bounds-access-in-loongarch_sy.patch
Patch3264: 0134-LoongArch-Fix-wrong-LSX-FP-vector-negation.patch
Patch3265: 0135-LoongArch-Fix-wrong-return-value-type-of-__iocsrrd_h.patch
Patch3266: 0136-LoongArch-Remove-redundant-symbol-type-conversions-i.patch
Patch3267: 0137-LoongArch-When-checking-whether-the-assembler-suppor.patch
Patch3268: 0138-LoongArch-Don-t-falsely-claim-gold-supported-in-topl.patch
Patch3269: 0139-LoongArch-NFC-Deduplicate-crc-instruction-defines.patch
Patch3270: 0140-LoongArch-Remove-unneeded-sign-extension-after-crc-c.patch
Patch3271: 0141-LoongArch-Allow-s9-as-a-register-alias.patch
Patch3272: 0142-LoongArch-testsuite-Rewrite-x-vfcmp-d-f-.c-to-avoid-.patch
Patch3273: 0143-LoongArch-Use-lib-instead-of-lib64-as-the-library-se.patch
Patch3274: 0144-LoongArch-testsuite-Fix-problems-with-incorrect-resu.patch
Patch3275: 0145-LoongArch-Fixed-an-issue-with-the-implementation-of-.patch
Patch3276: 0146-LoongArch-testsuite-Add-compilation-options-to-the-r.patch
Patch3277: 0147-LoongArch-Emit-R_LARCH_RELAX-for-TLS-IE-with-non-ext.patch
Patch3278: 0148-LoongArch-Remove-unused-and-incorrect-sge-u-_-X-mode.patch
Patch3279: 0149-LoongArch-Remove-masking-process-for-operand-3-of-xv.patch
Patch3280: 0150-LoongArch-Fix-C23-.-functions-returning-large-aggreg.patch
Patch3281: 0151-LoongArch-Remove-unused-useless-definitions.patch
Patch3282: 0152-LoongArch-Change-loongarch_expand_vec_cmp-s-return-t.patch
Patch3283: 0153-LoongArch-Combine-UNITS_PER_FP_REG-and-UNITS_PER_FPR.patch
Patch3284: 0154-LoongArch-Fix-a-typo-PR-114407.patch
Patch3285: 0155-testsuite-Add-a-test-case-for-negating-FP-vectors-co.patch
Patch3286: 0156-LoongArch-Add-descriptions-of-the-compilation-option.patch
Patch3287: 0157-LoongArch-Split-loongarch_option_override_internal-i.patch
Patch3288: 0158-LoongArch-Regenerate-loongarch.opt.urls.patch
Patch3289: 0159-LoongArch-Add-support-for-TLS-descriptors.patch
Patch3290: 0160-LoongArch-Fix-missing-plugin-header.patch
Patch3291: 0161-LoongArch-Remove-unused-code.patch
Patch3292: 0162-LoongArch-Set-default-alignment-for-functions-jumps-.patch
Patch3293: 0163-LoongArch-Enable-switchable-target.patch
Patch3294: 0164-LoongArch-Define-ISA-versions.patch
Patch3295: 0165-LoongArch-Define-builtin-macros-for-ISA-evolutions.patch
Patch3296: 0166-LoongArch-Add-constraints-for-bit-string-operation-d.patch
Patch3297: 0167-LoongArch-Guard-REGNO-with-REG_P-in-loongarch_expand.patch
Patch3298: 0168-LoongArch-Fix-mode-size-comparision-in-loongarch_exp.patch
Patch3299: 0169-LoongArch-Use-bstrins-for-value-1u-const.patch
Patch3300: 0170-LoongArch-Tweak-IOR-rtx_cost-for-bstrins.patch
Patch3301: 0171-LoongArch-NFC-Dedup-and-sort-the-comment-in-loongarc.patch
Patch3302: 0172-LoongArch-Fix-explicit-relocs-extreme-tls-desc.c-tes.patch
Patch3303: 0173-LoongArch-Define-loongarch_insn_cost-and-set-the-cos.patch
Patch3304: 0174-LoongArch-TFmode-is-not-allowed-to-be-stored-in-the-.patch
Patch3305: 0175-LoongArch-Remove-unreachable-codes.patch
Patch3306: 0176-LoongArch-Organize-the-code-related-to-split-move-an.patch
Patch3307: 0177-LoongArch-Expand-some-SImode-operations-through-si3_.patch
Patch3308: 0178-LoongArch-Relax-ins_zero_bitmask_operand-and-remove-.patch
Patch3309: 0179-LoongArch-Rework-bswap-hi-si-di-2-definition.patch
Patch3310: 0180-testsuite-fix-dg-do-preprocess-typo.patch
Patch3311: 0181-LoongArch-Remove-gawk-extension-from-a-generator-scr.patch
Patch3312: 0182-LoongArch-Use-iorn-and-andn-standard-pattern-names.patch
Patch3313: 0183-LoongArch-Drop-vcond-u-expanders.patch
Patch3314: 0184-LoongArch-Provide-ashr-lshr-and-ashl-RTL-pattern-for.patch
Patch3315: 0185-LoongArch-Implement-scalar-isinf-isnormal-and-isfini.patch
Patch3316: 0186-LoongArch-Add-support-to-annotate-tablejump.patch
Patch3317: 0187-LoongArch-Fix-up-r15-4130.patch
Patch3318: 0188-libphobos-Update-build-scripts-for-LoongArch64.patch
Patch3319: 0189-LoongArch-fix-building-errors.patch
Patch3320: 0190-tree-optimization-110702-avoid-zero-based-memory-ref.patch
Patch3321: 0191-LoongArch-Change-OSDIR-for-distribution.patch
Patch3322: Fix-indentation-and-numbering-errors.diff
Patch3323: LoongArch-Allow-attributes-in-non-gnu-namespaces.diff
%endif

# On ARM EABI systems, we do want -gnueabi to be part of the
# target triple.
%ifnarch %{arm}
%global _gnu %{nil}
%else
%global _gnu -gnueabi
%endif
%ifarch sparcv9
%global gcc_target_platform sparc64-%{_vendor}-%{_target_os}
%endif
%ifarch ppc ppc64p7
%global gcc_target_platform ppc64-%{_vendor}-%{_target_os}
%endif
%ifnarch sparcv9 ppc ppc64p7
%global gcc_target_platform %{_target_platform}
%endif

%if %{build_go}
# Avoid stripping these libraries and binaries.
%global __os_install_post \
chmod 644 %{buildroot}%{_prefix}/%{_lib}/libgo.so.21.* \
chmod 644 %{buildroot}%{_prefix}/bin/go.gcc \
chmod 644 %{buildroot}%{_prefix}/bin/gofmt.gcc \
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cgo \
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/buildid \
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/test2json \
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/vet \
%__os_install_post \
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgo.so.21.* \
chmod 755 %{buildroot}%{_prefix}/bin/go.gcc \
chmod 755 %{buildroot}%{_prefix}/bin/gofmt.gcc \
chmod 755 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cgo \
chmod 755 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/buildid \
chmod 755 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/test2json \
chmod 755 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/vet \
%{nil}
%endif

%description
The gcc package contains the GNU Compiler Collection version 12.
You'll need this package in order to compile C code.

%package -n libgcc
Summary: GCC version 12 shared support library
Autoreq: false
%if !%{build_ada}
Obsoletes: libgnat < %{version}-%{release}
%endif
Obsoletes: libgcj < %{version}-%{release}
Obsoletes: libgcj-devel < %{version}-%{release}
Obsoletes: libgcj-src < %{version}-%{release}
%ifarch %{ix86} x86_64
Obsoletes: libcilkrts
Obsoletes: libcilkrts-static
Obsoletes: libmpx
Obsoletes: libmpx-static
%endif

%description -n libgcc
This package contains GCC shared support library which is needed
e.g. for exception handling support.

%package c++
Summary: C++ support for GCC
Requires: gcc = %{version}-%{release}
Requires: libstdc++ = %{version}-%{release}
Requires: libstdc++-devel = %{version}-%{release}
Provides: gcc-g++ = %{version}-%{release}
Provides: g++ = %{version}-%{release}
Autoreq: true

%description c++
This package adds C++ support to the GNU Compiler Collection.
It includes support for most of the current C++ specification,
including templates and exception handling.

%package -n libstdc++
Summary: GNU Standard C++ Library
Autoreq: true
Requires: glibc >= 2.10.90-7

%description -n libstdc++
The libstdc++ package contains a rewritten standard compliant GCC Standard
C++ Library.

%package -n libstdc++-devel
Summary: Header files and libraries for C++ development
Requires: libstdc++%{?_isa} = %{version}-%{release}
Autoreq: true

%description -n libstdc++-devel
This is the GNU implementation of the standard C++ libraries.  This
package includes the header files and libraries needed for C++
development. This includes rewritten implementation of STL.

%package -n libstdc++-static
Summary: Static libraries for the GNU standard C++ library
Requires: libstdc++-devel = %{version}-%{release}
Autoreq: true

%description -n libstdc++-static
Static libraries for the GNU standard C++ library.

%package -n libstdc++-docs
Summary: Documentation for the GNU standard C++ library
Autoreq: true

%description -n libstdc++-docs
Manual, doxygen generated API information and Frequently Asked Questions
for the GNU standard C++ library.

%package objc
Summary: Objective-C support for GCC
Requires: gcc = %{version}-%{release}
Requires: libobjc = %{version}-%{release}
Autoreq: true

%description objc
gcc-objc provides Objective-C support for the GCC.
Mainly used on systems running NeXTSTEP, Objective-C is an
object-oriented derivative of the C language.

%package objc++
Summary: Objective-C++ support for GCC
Requires: gcc-c++ = %{version}-%{release}, gcc-objc = %{version}-%{release}
Autoreq: true

%description objc++
gcc-objc++ package provides Objective-C++ support for the GCC.

%package -n libobjc
Summary: Objective-C runtime
Autoreq: true

%description -n libobjc
This package contains Objective-C shared library which is needed to run
Objective-C dynamically linked programs.

%package gfortran
Summary: Fortran support
Requires: gcc = %{version}-%{release}
Requires: libgfortran = %{version}-%{release}
%if %{build_libquadmath}
Requires: libquadmath = %{version}-%{release}
Requires: libquadmath-devel = %{version}-%{release}
%endif
Provides: gcc-fortran = %{version}-%{release}
Provides: gfortran = %{version}-%{release}
Autoreq: true

%description gfortran
The gcc-gfortran package provides support for compiling Fortran
programs with the GNU Compiler Collection.

%package -n libgfortran
Summary: Fortran runtime
Autoreq: true
%if %{build_libquadmath}
Requires: libquadmath = %{version}-%{release}
%endif

%description -n libgfortran
This package contains Fortran shared library which is needed to run
Fortran dynamically linked programs.

%package -n libgfortran-static
Summary: Static Fortran libraries
Requires: libgfortran = %{version}-%{release}
Requires: gcc = %{version}-%{release}
%if %{build_libquadmath}
Requires: libquadmath-static = %{version}-%{release}
%endif

%description -n libgfortran-static
This package contains static Fortran libraries.

%package gdc
Summary: D support
Requires: gcc = %{version}-%{release}
Requires: libgphobos = %{version}-%{release}
Provides: gcc-d = %{version}-%{release}
Provides: gdc = %{version}-%{release}
Autoreq: true

%description gdc
The gcc-gdc package provides support for compiling D
programs with the GNU Compiler Collection.

%package -n libgphobos
Summary: D runtime
Autoreq: true

%description -n libgphobos
This package contains D shared library which is needed to run
D dynamically linked programs.

%package -n libgphobos-static
Summary: Static D libraries
Requires: libgphobos = %{version}-%{release}
Requires: gcc-gdc = %{version}-%{release}

%description -n libgphobos-static
This package contains static D libraries.

%package -n libgomp
Summary: GCC OpenMP v4.5 shared support library

%description -n libgomp
This package contains GCC shared support library which is needed
for OpenMP v4.5 support.

%package gdb-plugin
Summary: GCC plugin for GDB
Requires: gcc = %{version}-%{release}

%description gdb-plugin
This package contains GCC plugin for GDB C expression evaluation.

%package -n libgccjit
Summary: Library for embedding GCC inside programs and libraries
Requires: gcc = %{version}-%{release}

%description -n libgccjit
This package contains shared library with GCC JIT front-end.

%package -n libgccjit-devel
Summary: Support for embedding GCC inside programs and libraries
BuildRequires: python3-sphinx
Requires: libgccjit = %{version}-%{release}

%description -n libgccjit-devel
This package contains header files and documentation for GCC JIT front-end.

%package -n libquadmath
Summary: GCC __float128 shared support library

%description -n libquadmath
This package contains GCC shared support library which is needed
for __float128 math support and for Fortran REAL*16 support.

%package -n libquadmath-devel
Summary: GCC __float128 support
Requires: libquadmath = %{version}-%{release}
Requires: gcc = %{version}-%{release}

%description -n libquadmath-devel
This package contains headers for building Fortran programs using
REAL*16 and programs using __float128 math.

%package -n libquadmath-static
Summary: Static libraries for __float128 support
Requires: libquadmath-devel = %{version}-%{release}

%description -n libquadmath-static
This package contains static libraries for building Fortran programs
using REAL*16 and programs using __float128 math.

%package -n libitm
Summary: The GNU Transactional Memory library

%description -n libitm
This package contains the GNU Transactional Memory library
which is a GCC transactional memory support runtime library.

%package -n libitm-devel
Summary: The GNU Transactional Memory support
Requires: libitm = %{version}-%{release}
Requires: gcc = %{version}-%{release}

%description -n libitm-devel
This package contains headers and support files for the
GNU Transactional Memory library.

%package -n libitm-static
Summary: The GNU Transactional Memory static library
Requires: libitm-devel = %{version}-%{release}

%description -n libitm-static
This package contains GNU Transactional Memory static libraries.

%package -n libatomic
Summary: The GNU Atomic library

%description -n libatomic
This package contains the GNU Atomic library
which is a GCC support runtime library for atomic operations not supported
by hardware.

%package -n libatomic-static
Summary: The GNU Atomic static library
Requires: libatomic = %{version}-%{release}

%description -n libatomic-static
This package contains GNU Atomic static libraries.

%package -n libasan
Summary: The Address Sanitizer runtime library

%description -n libasan
This package contains the Address Sanitizer library
which is used for -fsanitize=address instrumented programs.

%package -n libasan-static
Summary: The Address Sanitizer static library
Requires: libasan = %{version}-%{release}

%description -n libasan-static
This package contains Address Sanitizer static runtime library.

%package -n libtsan
Summary: The Thread Sanitizer runtime library

%description -n libtsan
This package contains the Thread Sanitizer library
which is used for -fsanitize=thread instrumented programs.

%package -n libtsan-static
Summary: The Thread Sanitizer static library
Requires: libtsan = %{version}-%{release}

%description -n libtsan-static
This package contains Thread Sanitizer static runtime library.

%package -n libubsan
Summary: The Undefined Behavior Sanitizer runtime library

%description -n libubsan
This package contains the Undefined Behavior Sanitizer library
which is used for -fsanitize=undefined instrumented programs.

%package -n libubsan-static
Summary: The Undefined Behavior Sanitizer static library
Requires: libubsan = %{version}-%{release}

%description -n libubsan-static
This package contains Undefined Behavior Sanitizer static runtime library.

%package -n liblsan
Summary: The Leak Sanitizer runtime library

%description -n liblsan
This package contains the Leak Sanitizer library
which is used for -fsanitize=leak instrumented programs.

%package -n liblsan-static
Summary: The Leak Sanitizer static library
Requires: liblsan = %{version}-%{release}

%description -n liblsan-static
This package contains Leak Sanitizer static runtime library.

%package -n cpp
Summary: The C Preprocessor
Requires: filesystem >= 3
Provides: /lib/cpp
Autoreq: true

%description -n cpp
Cpp is the GNU C-Compatible Compiler Preprocessor.
Cpp is a macro processor which is used automatically
by the C compiler to transform your program before actual
compilation. It is called a macro processor because it allows
you to define macros, abbreviations for longer
constructs.

The C preprocessor provides four separate functionalities: the
inclusion of header files (files of declarations that can be
substituted into your program); macro expansion (you can define macros,
and the C preprocessor will replace the macros with their definitions
throughout the program); conditional compilation (using special
preprocessing directives, you can include or exclude parts of the
program according to various conditions); and line control (if you use
a program to combine or rearrange source files into an intermediate
file which is then compiled, you can use line control to inform the
compiler about where each source line originated).

You should install this package if you are a C programmer and you use
macros.

%package gnat
Summary: Ada 83, 95, 2005 and 2012 support for GCC
Requires: gcc = %{version}-%{release}
Requires: libgnat = %{version}-%{release}, libgnat-devel = %{version}-%{release}
Autoreq: true

%description gnat
GNAT is a GNU Ada 83, 95, 2005 and 2012 front-end to GCC. This package includes
development tools, the documents and Ada compiler.

%package -n libgnat
Summary: GNU Ada 83, 95, 2005 and 2012 runtime shared libraries
Autoreq: true

%description -n libgnat
GNAT is a GNU Ada 83, 95, 2005 and 2012 front-end to GCC. This package includes
shared libraries, which are required to run programs compiled with the GNAT.

%package -n libgnat-devel
Summary: GNU Ada 83, 95, 2005 and 2012 libraries
Autoreq: true

%description -n libgnat-devel
GNAT is a GNU Ada 83, 95, 2005 and 2012 front-end to GCC. This package includes
libraries, which are required to compile with the GNAT.

%package -n libgnat-static
Summary: GNU Ada 83, 95, 2005 and 2012 static libraries
Requires: libgnat-devel = %{version}-%{release}
Autoreq: true

%description -n libgnat-static
GNAT is a GNU Ada 83, 95, 2005 and 2012 front-end to GCC. This package includes
static libraries.

%package go
Summary: Go support
Requires: gcc = %{version}-%{release}
Requires: libgo = %{version}-%{release}
Requires: libgo-devel = %{version}-%{release}
Requires(post): %{_sbindir}/update-alternatives
Requires(postun): %{_sbindir}/update-alternatives
Provides: gccgo = %{version}-%{release}
Autoreq: true

%description go
The gcc-go package provides support for compiling Go programs
with the GNU Compiler Collection.

%package -n libgo
Summary: Go runtime
Autoreq: true

%description -n libgo
This package contains Go shared library which is needed to run
Go dynamically linked programs.

%package -n libgo-devel
Summary: Go development libraries
Requires: libgo = %{version}-%{release}
Autoreq: true

%description -n libgo-devel
This package includes libraries and support files for compiling
Go programs.

%package -n libgo-static
Summary: Static Go libraries
Requires: libgo = %{version}-%{release}
Requires: gcc = %{version}-%{release}

%description -n libgo-static
This package contains static Go libraries.

%package plugin-devel
Summary: Support for compiling GCC plugins
Requires: gcc = %{version}-%{release}
Requires: gmp-devel >= 4.1.2-8, mpfr-devel >= 3.1.0, libmpc-devel >= 0.8.1

%description plugin-devel
This package contains header files and other support files
for compiling GCC plugins.  The GCC plugin ABI is currently
not stable, so plugins must be rebuilt any time GCC is updated.

%prep
%setup -q -n gcc-12.3.0

%patch -P1 -p1
%patch -P2 -p1
%patch -P3 -p1
%patch -P4 -p1
%patch -P6 -p1
%patch -P7 -p1
%patch -P8 -p1
%patch -P9 -p1
%patch -P10 -p1
%patch -P11 -p1
%patch -P12 -p1
%patch -P13 -p1
%patch -P14 -p1
%patch -P15 -p1
%patch -P16 -p1
%patch -P17 -p1
%patch -P18 -p1
%patch -P19 -p1
%patch -P20 -p1
%patch -P21 -p1
%patch -P22 -p1
%patch -P23 -p1
%patch -P24 -p1
%patch -P25 -p1
%patch -P26 -p1
%patch -P27 -p1
%patch -P28 -p1
%patch -P29 -p1
%patch -P30 -p1
%patch -P31 -p1
%patch -P32 -p1
%patch -P33 -p1
%patch -P34 -p1
%patch -P35 -p1
%patch -P36 -p1
%patch -P37 -p1
%patch -P38 -p1
%patch -P39 -p1
%patch -P40 -p1
%patch -P41 -p1
%patch -P42 -p1
%patch -P43 -p1
%patch -P44 -p1
%patch -P45 -p1
%patch -P46 -p1
%patch -P47 -p1
%patch -P48 -p1
%patch -P49 -p1
%patch -P50 -p1
%patch -P51 -p1
%patch -P52 -p1
%patch -P53 -p1
%patch -P54 -p1
%patch -P55 -p1
%patch -P56 -p1
%patch -P57 -p1
%patch -P86 -p1
%patch -P87 -p1
%patch -P88 -p1
%patch -P89 -p1
%patch -P90 -p1
%patch -P91 -p1
%patch -P92 -p1
%patch -P93 -p1
%patch -P94 -p1
%patch -P95 -p1
%patch -P96 -p1
%patch -P97 -p1
%patch -P98 -p1
%patch -P99 -p1
%patch -P100 -p1
%patch -P101 -p1
%patch -P102 -p1
%patch -P103 -p1
%patch -P104 -p1
%patch -P105 -p1
%patch -P106 -p1
%patch -P107 -p1
%patch -P108 -p1
%patch -P109 -p1
%patch -P110 -p1
%patch -P111 -p1
%patch -P112 -p1
%patch -P113 -p1
%patch -P114 -p1
%patch -P115 -p1
%patch -P116 -p1
%patch -P117 -p1
%patch -P118 -p1
%patch -P119 -p1
%patch -P120 -p1
%patch -P121 -p1
%patch -P122 -p1
%patch -P123 -p1
%patch -P124 -p1
%patch -P125 -p1
%patch -P126 -p1
%patch -P127 -p1
%patch -P128 -p1
%patch -P129 -p1
%patch -P130 -p1
%patch -P131 -p1
%patch -P132 -p1
%patch -P133 -p1
%patch -P134 -p1
%patch -P135 -p1
%patch -P136 -p1
%patch -P137 -p1
%patch -P138 -p1
%patch -P139 -p1
%patch -P140 -p1
%patch -P141 -p1
%patch -P142 -p1
%patch -P143 -p1
%patch -P144 -p1
%patch -P145 -p1
%patch -P146 -p1
%patch -P147 -p1
%patch -P148 -p1
%patch -P149 -p1
%patch -P150 -p1
%patch -P151 -p1
%patch -P152 -p1
%patch -P153 -p1
%patch -P154 -p1
%patch -P155 -p1
%patch -P156 -p1
%patch -P157 -p1
%patch -P158 -p1
%patch -P159 -p1
%patch -P160 -p1
%patch -P161 -p1
%patch -P162 -p1
%patch -P163 -p1
%patch -P164 -p1
%patch -P165 -p1
%patch -P166 -p1
%patch -P167 -p1
%patch -P168 -p1
%patch -P169 -p1
%patch -P170 -p1
%patch -P171 -p1
%patch -P172 -p1
%patch -P173 -p1
%patch -P174 -p1
%patch -P175 -p1
%patch -P176 -p1
%patch -P177 -p1
%patch -P178 -p1
%patch -P179 -p1
%patch -P180 -p1
%patch -P181 -p1
%patch -P182 -p1
%patch -P183 -p1
%patch -P184 -p1
%patch -P185 -p1
%patch -P186 -p1
%patch -P187 -p1
%patch -P188 -p1
%patch -P189 -p1
%patch -P190 -p1
%patch -P191 -p1
%patch -P192 -p1
%patch -P193 -p1
%patch -P194 -p1
%patch -P195 -p1
%patch -P196 -p1
%patch -P197 -p1
%patch -P198 -p1
%patch -P199 -p1
%patch -P200 -p1
%patch -P201 -p1
%patch -P202 -p1
%patch -P203 -p1
%patch -P204 -p1
%patch -P205 -p1
%patch -P206 -p1
%patch -P207 -p1
%patch -P208 -p1
%patch -P209 -p1
%patch -P210 -p1
%patch -P211 -p1
%patch -P212 -p1
%patch -P213 -p1
%patch -P214 -p1
%patch -P215 -p1
%patch -P216 -p1
%patch -P217 -p1
%patch -P218 -p1
%patch -P219 -p1
%patch -P220 -p1
%patch -P221 -p1
%patch -P222 -p1
%patch -P223 -p1
%patch -P224 -p1
%patch -P225 -p1
%patch -P226 -p1
%patch -P227 -p1
%patch -P228 -p1
%patch -P229 -p1
%patch -P230 -p1
%patch -P231 -p1
%patch -P232 -p1
%patch -P233 -p1
%patch -P234 -p1
%patch -P235 -p1
%patch -P236 -p1
%patch -P237 -p1
%patch -P238 -p1
%patch -P239 -p1
%patch -P240 -p1
%patch -P241 -p1
%patch -P242 -p1
%patch -P243 -p1
%patch -P244 -p1
%patch -P245 -p1
%patch -P246 -p1
%patch -P247 -p1
%patch -P248 -p1
%patch -P249 -p1
%patch -P250 -p1
%patch -P251 -p1
%patch -P252 -p1
%patch -P253 -p1
%patch -P254 -p1
%patch -P255 -p1
%patch -P256 -p1
%patch -P257 -p1
%patch -P258 -p1
%patch -P259 -p1
%patch -P260 -p1
%patch -P261 -p1
%patch -P262 -p1
%patch -P263 -p1
%patch -P264 -p1
%patch -P265 -p1
%patch -P266 -p1
%patch -P267 -p1
%patch -P268 -p1
%patch -P269 -p1
%patch -P270 -p1
%patch -P271 -p1
%patch -P272 -p1
%patch -P273 -p1
%patch -P274 -p1
%patch -P275 -p1
%patch -P276 -p1
%patch -P277 -p1
%patch -P278 -p1
%patch -P279 -p1
%patch -P280 -p1
%patch -P281 -p1
%patch -P282 -p1
%patch -P283 -p1
%patch -P284 -p1
%patch -P285 -p1
%patch -P286 -p1
%patch -P287 -p1
%patch -P288 -p1
%patch -P289 -p1
%patch -P290 -p1
%patch -P291 -p1
%patch -P292 -p1
%patch -P293 -p1
%patch -P294 -p1
%patch -P295 -p1
%patch -P296 -p1
%patch -P297 -p1
%patch -P298 -p1
%patch -P299 -p1
%patch -P300 -p1
%patch -P301 -p1
%patch -P302 -p1
%patch -P303 -p1
%patch -P304 -p1
%patch -P305 -p1
%patch -P306 -p1
%patch -P307 -p1
%patch -P308 -p1
%patch -P309 -p1
%patch -P310 -p1
%patch -P311 -p1
%patch -P312 -p1
%patch -P313 -p1
%patch -P314 -p1
%patch -P315 -p1
%patch -P316 -p1
%patch -P317 -p1
%patch -P318 -p1
%patch -P319 -p1
%patch -P320 -p1
%patch -P321 -p1
%patch -P322 -p1
%patch -P323 -p1
%patch -P324 -p1
%patch -P325 -p1
%patch -P326 -p1
%patch -P327 -p1
%patch -P328 -p1
%patch -P329 -p1
%patch -P330 -p1
%patch -P331 -p1
%patch -P332 -p1
%patch -P333 -p1
%patch -P334 -p1
%patch -P335 -p1
%patch -P336 -p1
%patch -P337 -p1
%patch -P338 -p1
%patch -P339 -p1
%patch -P340 -p1
%patch -P341 -p1
%patch -P342 -p1
%patch -P343 -p1
%patch -P344 -p1
%patch -P345 -p1
%patch -P346 -p1
%patch -P347 -p1
%patch -P348 -p1
%patch -P349 -p1
%patch -P350 -p1
%patch -P351 -p1
%patch -P352 -p1
%patch -P353 -p1
%patch -P354 -p1
%patch -P355 -p1
%patch -P356 -p1
%patch -P357 -p1
%patch -P358 -p1

%ifarch sw_64
%patch -P1001 -p1
%patch -P1002 -p1
%patch -P1003 -p1
%patch -P1004 -p1
%patch -P1005 -p1
%patch -P1006 -p1
%patch -P1007 -p1
%patch -P1008 -p1
%patch -P1009 -p1
%patch -P1010 -p1
%patch -P1011 -p1
%patch -P1012 -p1
%patch -P1013 -p1
%patch -P1014 -p1
%patch -P1015 -p1
%patch -P1016 -p1
%patch -P1017 -p1
%endif

%ifarch loongarch64
%patch -P3001 -p1
%patch -P3002 -p1
%patch -P3003 -p1
%patch -P3004 -p1
%patch -P3005 -p1
%patch -P3006 -p1
%patch -P3007 -p1
%patch -P3008 -p1
%patch -P3009 -p1
%patch -P3010 -p1
%patch -P3011 -p1
%patch -P3012 -p1
%patch -P3013 -p1
%patch -P3014 -p1
%patch -P3015 -p1
%patch -P3016 -p1
%patch -P3017 -p1
%patch -P3018 -p1
%patch -P3019 -p1
%patch -P3020 -p1
%patch -P3021 -p1
%patch -P3022 -p1
%patch -P3023 -p1
%patch -P3024 -p1
%patch -P3025 -p1
%patch -P3026 -p1
%patch -P3027 -p1
%patch -P3028 -p1
%patch -P3029 -p1
%patch -P3030 -p1
%patch -P3031 -p1
%patch -P3032 -p1
%patch -P3033 -p1
%patch -P3034 -p1
%patch -P3035 -p1
%patch -P3036 -p1
%patch -P3037 -p1
%patch -P3038 -p1
%patch -P3039 -p1
%patch -P3040 -p1
%patch -P3041 -p1
%patch -P3042 -p1
%patch -P3043 -p1
%patch -P3044 -p1
%patch -P3045 -p1
%patch -P3046 -p1
%patch -P3047 -p1
%patch -P3048 -p1
%patch -P3049 -p1
%patch -P3050 -p1
%patch -P3051 -p1
%patch -P3052 -p1
%patch -P3053 -p1
%patch -P3054 -p1
%patch -P3056 -p1
%patch -P3057 -p1
%patch -P3058 -p1
%patch -P3059 -p1
%patch -P3060 -p1
%patch -P3061 -p1
%patch -P3062 -p1
%patch -P3063 -p1
%patch -P3064 -p1
%patch -P3065 -p1
%patch -P3066 -p1
%patch -P3067 -p1
%patch -P3068 -p1
%patch -P3069 -p1
%patch -P3070 -p1
%patch -P3071 -p1
%patch -P3072 -p1
%patch -P3073 -p1
%patch -P3074 -p1
%patch -P3075 -p1
%patch -P3076 -p1
%patch -P3077 -p1
%patch -P3078 -p1
%patch -P3079 -p1
%patch -P3080 -p1
%patch -P3081 -p1
%patch -P3082 -p1
%patch -P3083 -p1
%patch -P3084 -p1
%patch -P3085 -p1
%patch -P3086 -p1
%patch -P3087 -p1
%patch -P3088 -p1
%patch -P3089 -p1
%patch -P3090 -p1
%patch -P3091 -p1
%patch -P3092 -p1
%patch -P3093 -p1
%patch -P3094 -p1
%patch -P3095 -p1
%patch -P3096 -p1
%patch -P3097 -p1
%patch -P3098 -p1
%patch -P3099 -p1
%patch -P3100 -p1
%patch -P3101 -p1
%patch -P3102 -p1
%patch -P3103 -p1
%patch -P3104 -p1
%patch -P3105 -p1
%patch -P3106 -p1
%patch -P3107 -p1
%patch -P3108 -p1
%patch -P3109 -p1
%patch -P3110 -p1
%patch -P3111 -p1
%patch -P3112 -p1
%patch -P3113 -p1
%patch -P3114 -p1
%patch -P3115 -p1
%patch -P3116 -p1
%patch -P3117 -p1
%patch -P3118 -p1
%patch -P3119 -p1
%patch -P3120 -p1
%patch -P3121 -p1
%patch -P3122 -p1
%patch -P3123 -p1
%patch -P3124 -p1
%patch -P3125 -p1
%patch -P3126 -p1
%patch -P3127 -p1
%patch -P3128 -p1
%patch -P3129 -p1
%patch -P3130 -p1
#--
%patch -P3131 -p1
%patch -P3132 -p1
%patch -P3133 -p1
%patch -P3134 -p1
%patch -P3135 -p1
%patch -P3136 -p1
%patch -P3137 -p1
%patch -P3138 -p1
%patch -P3139 -p1
%patch -P3140 -p1
%patch -P3141 -p1
%patch -P3142 -p1
%patch -P3143 -p1
%patch -P3144 -p1
%patch -P3145 -p1
%patch -P3146 -p1
%patch -P3147 -p1
%patch -P3148 -p1
%patch -P3149 -p1
%patch -P3150 -p1
%patch -P3151 -p1
%patch -P3152 -p1
%patch -P3153 -p1
%patch -P3154 -p1
%patch -P3155 -p1
%patch -P3156 -p1
%patch -P3157 -p1
%patch -P3158 -p1
%patch -P3159 -p1
%patch -P3160 -p1
%patch -P3161 -p1
%patch -P3162 -p1
%patch -P3163 -p1
%patch -P3164 -p1
%patch -P3165 -p1
%patch -P3166 -p1
%patch -P3167 -p1
%patch -P3168 -p1
%patch -P3169 -p1
%patch -P3170 -p1
%patch -P3171 -p1
%patch -P3172 -p1
%patch -P3173 -p1
%patch -P3174 -p1
%patch -P3175 -p1
%patch -P3176 -p1
%patch -P3177 -p1
%patch -P3178 -p1
%patch -P3179 -p1
%patch -P3180 -p1
%patch -P3181 -p1
%patch -P3182 -p1
%patch -P3183 -p1
%patch -P3184 -p1
%patch -P3185 -p1
%patch -P3186 -p1
%patch -P3187 -p1
%patch -P3188 -p1
%patch -P3189 -p1
%patch -P3190 -p1
%patch -P3191 -p1
%patch -P3192 -p1
%patch -P3193 -p1
%patch -P3194 -p1
%patch -P3195 -p1
%patch -P3196 -p1
%patch -P3197 -p1
%patch -P3198 -p1
%patch -P3199 -p1
%patch -P3200 -p1
%patch -P3201 -p1
%patch -P3202 -p1
%patch -P3203 -p1
%patch -P3204 -p1
%patch -P3205 -p1
%patch -P3206 -p1
%patch -P3207 -p1
%patch -P3208 -p1
%patch -P3209 -p1
%patch -P3210 -p1
%patch -P3211 -p1
%patch -P3212 -p1
%patch -P3213 -p1
%patch -P3214 -p1
%patch -P3215 -p1
%patch -P3216 -p1
%patch -P3217 -p1
%patch -P3218 -p1
%patch -P3219 -p1
%patch -P3220 -p1
%patch -P3221 -p1
%patch -P3222 -p1
%patch -P3223 -p1
%patch -P3224 -p1
%patch -P3225 -p1
%patch -P3226 -p1
%patch -P3227 -p1
%patch -P3228 -p1
%patch -P3229 -p1
%patch -P3230 -p1
%patch -P3231 -p1
%patch -P3232 -p1
%patch -P3233 -p1
%patch -P3234 -p1
%patch -P3235 -p1
%patch -P3236 -p1
%patch -P3237 -p1
%patch -P3238 -p1
%patch -P3239 -p1
%patch -P3240 -p1
%patch -P3241 -p1
%patch -P3242 -p1
%patch -P3243 -p1
%patch -P3244 -p1
%patch -P3245 -p1
%patch -P3246 -p1
%patch -P3247 -p1
%patch -P3248 -p1
%patch -P3249 -p1
%patch -P3250 -p1
%patch -P3251 -p1
%patch -P3252 -p1
%patch -P3253 -p1
%patch -P3254 -p1
%patch -P3255 -p1
%patch -P3256 -p1
%patch -P3257 -p1
%patch -P3258 -p1
%patch -P3259 -p1
%patch -P3260 -p1
%patch -P3261 -p1
%patch -P3262 -p1
%patch -P3263 -p1
%patch -P3264 -p1
%patch -P3265 -p1
%patch -P3266 -p1
%patch -P3267 -p1
%patch -P3268 -p1
%patch -P3269 -p1
%patch -P3270 -p1
%patch -P3271 -p1
%patch -P3272 -p1
%patch -P3273 -p1
%patch -P3274 -p1
%patch -P3275 -p1
%patch -P3276 -p1
%patch -P3277 -p1
%patch -P3278 -p1
%patch -P3279 -p1
%patch -P3280 -p1
%patch -P3281 -p1
%patch -P3282 -p1
%patch -P3283 -p1
%patch -P3284 -p1
%patch -P3285 -p1
%patch -P3286 -p1
%patch -P3287 -p1
%patch -P3288 -p1
%patch -P3289 -p1
%patch -P3290 -p1
%patch -P3291 -p1
%patch -P3292 -p1
%patch -P3293 -p1
%patch -P3294 -p1
%patch -P3295 -p1
%patch -P3296 -p1
%patch -P3297 -p1
%patch -P3298 -p1
%patch -P3299 -p1
%patch -P3300 -p1
%patch -P3301 -p1
%patch -P3302 -p1
%patch -P3303 -p1
%patch -P3304 -p1
%patch -P3305 -p1
%patch -P3306 -p1
%patch -P3307 -p1
%patch -P3308 -p1
%patch -P3309 -p1
%patch -P3310 -p1
%patch -P3311 -p1
%patch -P3312 -p1
%patch -P3313 -p1
%patch -P3314 -p1
%patch -P3315 -p1
%patch -P3316 -p1
%patch -P3317 -p1
%patch -P3318 -p1
%patch -P3319 -p1
%patch -P3320 -p1
%patch -P3321 -p1
%patch -P3322 -p1
%patch -P3323 -p1
%endif

echo '%{_vendor} %{version}-%{release}' > gcc/DEV-PHASE

cp -a libstdc++-v3/config/cpu/i{4,3}86/atomicity.h

./contrib/gcc_update --touch

LC_ALL=C sed -i -e 's/\xa0/ /' gcc/doc/options.texi

sed -i -e 's/Common Driver Var(flag_report_bug)/& Init(1)/' gcc/common.opt
sed -i -e 's/context->report_bug = false;/context->report_bug = true;/' gcc/diagnostic.cc

%build

export CONFIG_SITE=NONE

CC=gcc
CXX=g++
OPT_FLAGS=`echo %{optflags}|sed -e 's/-flto=auto//g;s/-flto//g;s/-ffat-lto-objects//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-m64//g;s/-m32//g;s/-m31//g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mfpmath=sse/-mfpmath=sse -msse2/g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/ -pipe / /g'`
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-Werror=format-security/ /g'`
%ifarch sparc
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-mcpu=ultrasparc/-mtune=ultrasparc/g;s/-mcpu=v[78]//g'`
%endif
%ifarch %{ix86}
OPT_FLAGS=`echo $OPT_FLAGS|sed -e 's/-march=i.86//g'`
%endif
OPT_FLAGS=`echo "$OPT_FLAGS" | sed -e 's/[[:blank:]]\+/ /g'`
case "$OPT_FLAGS" in
  *-fasynchronous-unwind-tables*)
    sed -i -e 's/-fno-exceptions /-fno-exceptions -fno-asynchronous-unwind-tables /' \
      libgcc/Makefile.in
    ;;
esac

rm -rf obj-%{gcc_target_platform}
mkdir obj-%{gcc_target_platform}
cd obj-%{gcc_target_platform}

enablelgo=
enablelada=
enablelobjc=
enableld=
%if %{build_objc}
enablelobjc=,objc,obj-c++
%endif
%if %{build_ada}
enablelada=,ada
%endif
%if %{build_go}
enablelgo=,go
%endif
%if %{build_d}
enableld=,d
%endif

CONFIGURE_OPTS="\
	--prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
	--with-bugurl=https://gitee.com/src-openeuler/gcc/issues \
	--enable-shared --enable-threads=posix --enable-checking=release\
%ifarch aarch64
	--enable-multilib \
%else
	--disable-multilib \
%endif
	--with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions \
	--enable-gnu-unique-object --enable-linker-build-id --with-gcc-major-version-only \
	--enable-libstdcxx-backtrace \
%ifnarch %{mips}
	--with-linker-hash-style=gnu \
%endif
	--enable-plugin --enable-initfini-array \
%if %{isl_enable}
	--with-isl \
%else
	--without-isl \
%endif
%if %{attr_ifunc}
	--enable-gnu-indirect-function \
%endif
%ifarch %{arm}
	--disable-sjlj-exceptions \
%endif
%ifarch aarch64
	--with-multilib-list=lp64 \
        --enable-bolt
%endif
%ifarch %{ix86} x86_64
	--enable-cet \
	--with-tune=generic \
%endif
%ifarch %{ix86}
	--with-arch=x86-64 \
%endif
%ifarch x86_64
	--with-arch_64=x86-64-v2 \
%endif
%ifarch riscv64
	--with-arch=rv64gc --with-abi=lp64d --disable-libquadmath \
%endif
%ifnarch sparc sparcv9 ppc
	--build=%{gcc_target_platform} \
%endif
%ifarch loongarch64
	   --with-arch=loongarch64 --with-abi=lp64d \
	   --disable-libquadmath --disable-multilib --enable-tls \
	   --enable-default-pie
%endif
%ifarch sw_64
	--with-cpu=sw6b --disable-libquadmath --disable-multilib \
	--enable-tls
%endif
%ifarch ppc64le
        --enable-targets=powerpcle-linux \
        --with-cpu-32=power8 --with-tune-32=power8 --with-cpu-64=power8 --with-tune-64=power8 \
%endif
	"

# Add Secure Compilation Options
OPT_FLAGS="$OPT_FLAGS -O2 -Wp,-D_FORTIFY_SOURCE=2 -fstack-protector-strong -fPIE -Wl,-z,relro,-z,now"
OPT_LDFLAGS="$OPT_LDFLAGS -Wl,-z,relro,-z,now"

# Disable bootstrap for saving compilation time
export extra_ldflags_libobjc="-Wl,-z,relro,-z,now"
export FCFLAGS="$OPT_FLAGS"
CC="$CC" CXX="$CXX" \
	CFLAGS="$OPT_FLAGS" \
	CXXFLAGS="`echo " $OPT_FLAGS " | sed 's/ -Wall / /g;s/ -fexceptions / /g' \
		  | sed 's/ -Wformat-security / -Wformat -Wformat-security /'`" \
	LDFLAGS="$OPT_LDFLAGS" \
	CFLAGS_FOR_TARGET="$OPT_FLAGS" \
	CXXFLAGS_FOR_TARGET="$OPT_FLAGS" \
	XCFLAGS="$OPT_FLAGS" TCFLAGS="$OPT_FLAGS" GCJFLAGS="$OPT_FLAGS" \
	../configure --disable-bootstrap --disable-libgcj --without-cloog\
	--enable-languages=c,c++,fortran${enablelobjc}${enablelada}${enablelgo}${enableld},lto \
	$CONFIGURE_OPTS

%ifarch sparc sparcv9 sparc64
make %{?_smp_mflags} BOOT_CFLAGS="$OPT_FLAGS" LDFLAGS_FOR_TARGET=-Wl,-z,relro,-z,now
%else
make %{?_smp_mflags} BOOT_CFLAGS="$OPT_FLAGS" LDFLAGS_FOR_TARGET=-Wl,-z,relro,-z,now
%endif

CC="`%{gcc_target_platform}/libstdc++-v3/scripts/testsuite_flags --build-cc`"
CXX="`%{gcc_target_platform}/libstdc++-v3/scripts/testsuite_flags --build-cxx` `%{gcc_target_platform}/libstdc++-v3/scripts/testsuite_flags --build-includes`"

# Build libgccjit separately, so that normal compiler binaries aren't -fpic
# unnecessarily.
mkdir objlibgccjit
cd objlibgccjit
CC="$CC" CXX="$CXX" \
	CFLAGS="$OPT_FLAGS" \
	CXXFLAGS="`echo " $OPT_FLAGS " | sed 's/ -Wall / /g;s/ -fexceptions / /g' \
		  | sed 's/ -Wformat-security / -Wformat -Wformat-security /'`" \
	LDFLAGS="$OPT_LDFLAGS" \
	CFLAGS_FOR_TARGET="$OPT_FLAGS" \
	CXXFLAGS_FOR_TARGET="$OPT_FLAGS" \
	XCFLAGS="$OPT_FLAGS" TCFLAGS="$OPT_FLAGS" GCJFLAGS="$OPT_FLAGS" \
	../../configure --disable-bootstrap --enable-host-shared \
	--enable-languages=jit $CONFIGURE_OPTS
make %{?_smp_mflags} BOOT_CFLAGS="$OPT_FLAGS" all-gcc
cp -a gcc/libgccjit.so* ../gcc/
cd ../gcc/
ln -sf xgcc %{gcc_target_platform}-gcc-%{gcc_major}
cp -a Makefile{,.orig}
sed -i -e '/^CHECK_TARGETS/s/$/ check-jit/' Makefile
touch -r Makefile.orig Makefile
rm Makefile.orig
make jit.sphinx.html
make jit.sphinx.install-html jit_htmldir=`pwd`/../../rpm.doc/libgccjit-devel/html
cd ..

# Make generated man pages even if Pod::Man is not new enough
perl -pi -e 's/head3/head2/' ../contrib/texi2pod.pl
for i in ../gcc/doc/*.texi; do
  cp -a $i $i.orig; sed 's/ftable/table/' $i.orig > $i
done
make -C gcc generated-manpages
for i in ../gcc/doc/*.texi; do mv -f $i.orig $i; done

# Make generated doxygen pages.
%if %{build_libstdcxx_docs}
cd %{gcc_target_platform}/libstdc++-v3
make doc-html-doxygen
make doc-man-doxygen
cd ../..
%endif

# Copy various doc files here and there
cd ..
mkdir -p rpm.doc/gfortran rpm.doc/objc rpm.doc/gdc rpm.doc/libphobos
mkdir -p rpm.doc/go rpm.doc/libgo rpm.doc/libquadmath rpm.doc/libitm
mkdir -p rpm.doc/changelogs/{gcc/cp,gcc/ada,gcc/jit,libstdc++-v3,libobjc,libgomp,libcc1,libatomic,libsanitizer}

for i in {gcc,gcc/cp,gcc/ada,gcc/jit,libstdc++-v3,libobjc,libgomp,libcc1,libatomic,libsanitizer}/ChangeLog*; do
	cp -p $i rpm.doc/changelogs/$i
done

(cd gcc/fortran; for i in ChangeLog*; do
	cp -p $i ../../rpm.doc/gfortran/$i
done)
(cd libgfortran; for i in ChangeLog*; do
	cp -p $i ../rpm.doc/gfortran/$i.libgfortran
done)
%if %{build_objc}
(cd libobjc; for i in README*; do
	cp -p $i ../rpm.doc/objc/$i.libobjc
done)
%endif
%if %{build_d}
(cd gcc/d; for i in ChangeLog*; do
	cp -p $i ../../rpm.doc/gdc/$i.gdc
done)
(cd libphobos; for i in ChangeLog*; do
	cp -p $i ../rpm.doc/libphobos/$i.libphobos
done
cp -a src/LICENSE*.txt libdruntime/LICENSE.txt ../rpm.doc/libphobos/)
%endif
%if %{build_libquadmath}
(cd libquadmath; for i in ChangeLog* COPYING.LIB; do
	cp -p $i ../rpm.doc/libquadmath/$i.libquadmath
done)
%endif
%if %{build_libitm}
(cd libitm; for i in ChangeLog*; do
	cp -p $i ../rpm.doc/libitm/$i.libitm
done)
%endif
%if %{build_go}
(cd gcc/go; for i in README* ChangeLog*; do
	cp -p $i ../../rpm.doc/go/$i
done)
(cd libgo; for i in LICENSE* PATENTS* README; do
	cp -p $i ../rpm.doc/libgo/$i.libgo
done)
%endif

rm -f rpm.doc/changelogs/gcc/ChangeLog.[1-9]
find rpm.doc -name \*ChangeLog\* | xargs bzip2 -9

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

# RISC-V ABI wants to install everything in /lib64/lp64d or /usr/lib64/lp64d.
# Make these be symlinks to /lib64 or /usr/lib64 respectively. See:
# https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/DRHT5YTPK4WWVGL3GIN5BF2IKX2ODHZ3/
%ifarch riscv64
for d in %{buildroot}%{_libdir} %{buildroot}/%{_lib} \
	  %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib} \
	  %{buildroot}%{_prefix}/include/c++/%{gcc_major}/%{gcc_target_platform}/%{_lib}; do
  mkdir -p $d
  (cd $d && ln -sf . lp64d)
done
%endif

cd obj-%{gcc_target_platform}

TARGET_PLATFORM=%{gcc_target_platform}

# There are some MP bugs in libstdc++ Makefiles
make -C %{gcc_target_platform}/libstdc++-v3

make prefix=%{buildroot}%{_prefix} mandir=%{buildroot}%{_mandir} \
  infodir=%{buildroot}%{_infodir} install
%if %{build_ada}
chmod 644 %{buildroot}%{_infodir}/gnat*
%endif

FULLPATH=%{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
FULLEPATH=%{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}

# fix some things
ln -sf gcc %{buildroot}%{_prefix}/bin/cc
rm -f %{buildroot}%{_prefix}/lib/cpp
ln -sf ../bin/cpp %{buildroot}/%{_prefix}/lib/cpp
ln -sf gfortran %{buildroot}%{_prefix}/bin/f95
rm -f %{buildroot}%{_infodir}/dir
gzip -9 %{buildroot}%{_infodir}/*.info*
ln -sf gcc %{buildroot}%{_prefix}/bin/gnatgcc
mkdir -p %{buildroot}%{_fmoddir}

%if %{build_go}
mv %{buildroot}%{_prefix}/bin/go{,.gcc}
mv %{buildroot}%{_prefix}/bin/gofmt{,.gcc}
ln -sf /etc/alternatives/go %{buildroot}%{_prefix}/bin/go
ln -sf /etc/alternatives/gofmt %{buildroot}%{_prefix}/bin/gofmt
%endif

cxxconfig="`find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h`"
for i in `find %{gcc_target_platform}/[36]*/libstdc++-v3/include -name c++config.h 2>/dev/null`; do
  if ! diff -up $cxxconfig $i; then
    cat > %{buildroot}%{_prefix}/include/c++/%{gcc_major}/%{gcc_target_platform}/bits/c++config.h <<EOF
#ifndef _CPP_CPPCONFIG_WRAPPER
#define _CPP_CPPCONFIG_WRAPPER 1
#include <bits/wordsize.h>
#if __WORDSIZE == 32
%ifarch %{multilib_64_archs}
`cat $(find %{gcc_target_platform}/32/libstdc++-v3/include -name c++config.h)`
%else
`cat $(find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h)`
%endif
#else
%ifarch %{multilib_64_archs}
`cat $(find %{gcc_target_platform}/libstdc++-v3/include -name c++config.h)`
%else
`cat $(find %{gcc_target_platform}/64/libstdc++-v3/include -name c++config.h)`
%endif
#endif
#endif
EOF
    break
  fi
done

for f in `find %{buildroot}%{_prefix}/include/c++/%{gcc_major}/%{gcc_target_platform}/ -name c++config.h`; do
  for i in 1 2 4 8; do
    sed -i -e 's/#define _GLIBCXX_ATOMIC_BUILTINS_'$i' 1/#ifdef __GCC_HAVE_SYNC_COMPARE_AND_SWAP_'$i'\
&\
#endif/' $f
  done
done

# Nuke bits/*.h.gch dirs
# 1) there is no bits/*.h header installed, so when gch file can't be
#    used, compilation fails
# 2) sometimes it is hard to match the exact options used for building
#    libstdc++-v3 or they aren't desirable
# 3) there are multilib issues, conflicts etc. with this
# 4) it is huge
# People can always precompile on their own whatever they want, but
# shipping this for everybody is unnecessary.
rm -rf %{buildroot}%{_prefix}/include/c++/%{gcc_major}/%{gcc_target_platform}/bits/*.h.gch

%if %{build_libstdcxx_docs}
libstdcxx_doc_builddir=%{gcc_target_platform}/libstdc++-v3/doc/doxygen
mkdir -p ../rpm.doc/libstdc++-v3
cp -r -p ../libstdc++-v3/doc/html ../rpm.doc/libstdc++-v3/html
cp -r -p $libstdcxx_doc_builddir/html ../rpm.doc/libstdc++-v3/html/api
mkdir -p %{buildroot}%{_mandir}/man3
cp -r -p $libstdcxx_doc_builddir/man/man3/* %{buildroot}%{_mandir}/man3/
find ../rpm.doc/libstdc++-v3 -name \*~ | xargs rm
%endif

%ifarch sparcv9 sparc64
ln -f %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-gcc \
  %{buildroot}%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc
%endif
%ifarch ppc ppc64 ppc64p7
ln -f %{buildroot}%{_prefix}/bin/%{gcc_target_platform}-gcc \
  %{buildroot}%{_prefix}/bin/ppc-%{_vendor}-%{_target_os}-gcc
%endif

FULLLSUBDIR=
%ifarch sparcv9 ppc
FULLLSUBDIR=lib32
%endif
%ifarch sparc64 ppc64 ppc64p7
FULLLSUBDIR=lib64
%endif
if [ -n "$FULLLSUBDIR" ]; then
  FULLLPATH=$FULLPATH/$FULLLSUBDIR
  mkdir -p $FULLLPATH
else
  FULLLPATH=$FULLPATH
fi

find %{buildroot} -name \*.la | xargs rm -f

mv %{buildroot}%{_prefix}/%{_lib}/libgfortran.spec $FULLPATH/
%if %{build_d}
mv %{buildroot}%{_prefix}/%{_lib}/libgphobos.spec $FULLPATH/
%endif
%if %{build_libitm}
mv %{buildroot}%{_prefix}/%{_lib}/libitm.spec $FULLPATH/
%endif
%if %{build_libasan}
mv %{buildroot}%{_prefix}/%{_lib}/libsanitizer.spec $FULLPATH/
%endif

mkdir -p %{buildroot}/%{_lib}
mv -f %{buildroot}%{_prefix}/%{_lib}/libgcc_s.so.1 %{buildroot}/%{_lib}/libgcc_s-%{gcc_major}.so.1
chmod 755 %{buildroot}/%{_lib}/libgcc_s-%{gcc_major}.so.1
ln -sf libgcc_s-%{gcc_major}.so.1 %{buildroot}/%{_lib}/libgcc_s.so.1
%ifarch %{ix86} x86_64 ppc ppc64 ppc64p7 ppc64le %{arm} aarch64 riscv64 loongarch64 sw_64
rm -f $FULLPATH/libgcc_s.so
echo '/* GNU ld script
   Use the shared library, but some functions are only in
   the static library, so try that secondarily.  */
OUTPUT_FORMAT('`gcc -Wl,--print-output-format -nostdlib -r -o /dev/null`')
GROUP ( /%{_lib}/libgcc_s.so.1 libgcc.a )' > $FULLPATH/libgcc_s.so
%else
ln -sf /%{_lib}/libgcc_s.so.1 $FULLPATH/libgcc_s.so
%endif
%ifarch sparcv9 ppc
%ifarch ppc
rm -f $FULLPATH/64/libgcc_s.so
echo '/* GNU ld script
   Use the shared library, but some functions are only in
   the static library, so try that secondarily.  */
OUTPUT_FORMAT('`gcc -m64 -Wl,--print-output-format -nostdlib -r -o /dev/null`')
GROUP ( /lib64/libgcc_s.so.1 libgcc.a )' > $FULLPATH/64/libgcc_s.so
%else
ln -sf /lib64/libgcc_s.so.1 $FULLPATH/64/libgcc_s.so
%endif
%endif
%ifarch %{multilib_64_archs}
%ifarch x86_64 ppc64 ppc64p7
rm -f $FULLPATH/64/libgcc_s.so
echo '/* GNU ld script
   Use the shared library, but some functions are only in
   the static library, so try that secondarily.  */
OUTPUT_FORMAT('`gcc -m32 -Wl,--print-output-format -nostdlib -r -o /dev/null`')
GROUP ( /lib/libgcc_s.so.1 libgcc.a )' > $FULLPATH/32/libgcc_s.so
%else
ln -sf /lib/libgcc_s.so.1 $FULLPATH/32/libgcc_s.so
%endif
%endif

mv -f %{buildroot}%{_prefix}/%{_lib}/libgomp.spec $FULLPATH/

%if %{build_ada}
mv -f $FULLPATH/adalib/libgnarl-*.so %{buildroot}%{_prefix}/%{_lib}/
mv -f $FULLPATH/adalib/libgnat-*.so %{buildroot}%{_prefix}/%{_lib}/
rm -f $FULLPATH/adalib/libgnarl.so* $FULLPATH/adalib/libgnat.so*
%endif

mkdir -p %{buildroot}%{_prefix}/libexec/getconf
if gcc/xgcc -B gcc/ -E -P -dD -xc /dev/null | grep '__LONG_MAX__.*\(2147483647\|0x7fffffff\($\|[LU]\)\)'; then
  ln -sf POSIX_V6_ILP32_OFF32 %{buildroot}%{_prefix}/libexec/getconf/default
else
  ln -sf POSIX_V6_LP64_OFF64 %{buildroot}%{_prefix}/libexec/getconf/default
fi

mkdir -p %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}
mv -f %{buildroot}%{_prefix}/%{_lib}/libstdc++*gdb.py* \
      %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/
pushd ../libstdc++-v3/python
for i in `find . -name \*.py`; do
  touch -r $i %{buildroot}%{_prefix}/share/gcc-%{gcc_major}/python/$i
done
touch -r hook.in %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/libstdc++*gdb.py
popd
for f in `find %{buildroot}%{_prefix}/share/gcc-%{gcc_major}/python/ \
	       %{buildroot}%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/ -name \*.py`; do
  r=${f/$RPM_BUILD_ROOT/}
  %{__python3} -c 'import py_compile; py_compile.compile("'$f'", dfile="'$r'")'
  %{__python3} -O -c 'import py_compile; py_compile.compile("'$f'", dfile="'$r'")'
done

rm -f $FULLEPATH/libgccjit.so
cp -a objlibgccjit/gcc/libgccjit.so* %{buildroot}%{_prefix}/%{_lib}/
cp -a ../gcc/jit/libgccjit*.h %{buildroot}%{_prefix}/include/
/usr/bin/install -c -m 644 objlibgccjit/gcc/doc/libgccjit.info %{buildroot}/%{_infodir}/
gzip -9 %{buildroot}/%{_infodir}/libgccjit.info

pushd $FULLPATH
if [ "%{_lib}" = "lib" ]; then
%if %{build_objc}
ln -sf ../../../libobjc.so.4 libobjc.so
%endif
ln -sf ../../../libstdc++.so.6.*[0-9] libstdc++.so
ln -sf ../../../libgfortran.so.5.* libgfortran.so
ln -sf ../../../libgomp.so.1.* libgomp.so
%if %{build_go}
ln -sf ../../../libgo.so.21.* libgo.so
%endif
%if %{build_libquadmath}
ln -sf ../../../libquadmath.so.0.* libquadmath.so
%endif
%if %{build_d}
ln -sf ../../../libgdruntime.so.3.* libgdruntime.so
ln -sf ../../../libgphobos.so.3.* libgphobos.so
%endif
%if %{build_libitm}
ln -sf ../../../libitm.so.1.* libitm.so
%endif
%if %{build_libatomic}
ln -sf ../../../libatomic.so.1.* libatomic.so
%endif
%if %{build_libasan}
ln -sf ../../../libasan.so.8.* libasan.so
mv ../../../libasan_preinit.o libasan_preinit.o
%endif
%if %{build_libubsan}
ln -sf ../../../libubsan.so.1.* libubsan.so
%endif
%ifarch loongarch64 sw_64
%if %{build_libtsan}
rm -f libtsan.so
echo 'INPUT ( %{_prefix}/%{_lib}/'`echo ../../../../%{_lib}/libtsan.so.2.* | sed 's,^.*libt,libt,'`' )' > libtsan.so
mv ../../../../%{_lib}/libtsan_preinit.o libtsan_preinit.o
%endif
%if %{build_liblsan}
rm -f liblsan.so
echo 'INPUT ( %{_prefix}/%{_lib}/'`echo ../../../../%{_lib}/liblsan.so.0.* | sed 's,^.*libl,libl,'`' )' > liblsan.so
mv ../../../../%{_lib}/liblsan_preinit.o liblsan_preinit.o
%endif
%endif
else
%if %{build_objc}
ln -sf ../../../../%{_lib}/libobjc.so.4 libobjc.so
%endif
ln -sf ../../../../%{_lib}/libstdc++.so.6.*[0-9] libstdc++.so
ln -sf ../../../../%{_lib}/libgfortran.so.5.* libgfortran.so
ln -sf ../../../../%{_lib}/libgomp.so.1.* libgomp.so
%if %{build_go}
ln -sf ../../../../%{_lib}/libgo.so.21.* libgo.so
%endif
%if %{build_libquadmath}
ln -sf ../../../../%{_lib}/libquadmath.so.0.* libquadmath.so
%endif
%if %{build_d}
ln -sf ../../../../%{_lib}/libgdruntime.so.3.* libgdruntime.so
ln -sf ../../../../%{_lib}/libgphobos.so.3.* libgphobos.so
%endif
%if %{build_libitm}
ln -sf ../../../../%{_lib}/libitm.so.1.* libitm.so
%endif
%if %{build_libatomic}
ln -sf ../../../../%{_lib}/libatomic.so.1.* libatomic.so
%endif
%if %{build_libasan}
ln -sf ../../../../%{_lib}/libasan.so.8.* libasan.so
mv ../../../../%{_lib}/libasan_preinit.o libasan_preinit.o
%endif
%if %{build_libubsan}
ln -sf ../../../../%{_lib}/libubsan.so.1.* libubsan.so
%endif
%if %{build_libtsan}
rm -f libtsan.so
echo 'INPUT ( %{_prefix}/%{_lib}/'`echo ../../../../%{_lib}/libtsan.so.2.* | sed 's,^.*libt,libt,'`' )' > libtsan.so
mv ../../../../%{_lib}/libtsan_preinit.o libtsan_preinit.o
%endif
%if %{build_liblsan}
rm -f liblsan.so
echo 'INPUT ( %{_prefix}/%{_lib}/'`echo ../../../../%{_lib}/liblsan.so.0.* | sed 's,^.*libl,libl,'`' )' > liblsan.so
mv ../../../../%{_lib}/liblsan_preinit.o liblsan_preinit.o
%endif
fi
mv -f %{buildroot}%{_prefix}/%{_lib}/libstdc++.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libstdc++fs.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libstdc++_libbacktrace.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libsupc++.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libgfortran.*a $FULLLPATH/
%if %{build_objc}
mv -f %{buildroot}%{_prefix}/%{_lib}/libobjc.*a .
%endif
mv -f %{buildroot}%{_prefix}/%{_lib}/libgomp.*a .
%if %{build_libquadmath}
mv -f %{buildroot}%{_prefix}/%{_lib}/libquadmath.*a $FULLLPATH/
%endif
%if %{build_d}
mv -f %{buildroot}%{_prefix}/%{_lib}/libgdruntime.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libgphobos.*a $FULLLPATH/
%endif
%if %{build_libitm}
mv -f %{buildroot}%{_prefix}/%{_lib}/libitm.*a $FULLLPATH/
%endif
%if %{build_libatomic}
mv -f %{buildroot}%{_prefix}/%{_lib}/libatomic.*a $FULLLPATH/
%endif
%if %{build_libasan}
mv -f %{buildroot}%{_prefix}/%{_lib}/libasan.*a $FULLLPATH/
%endif
%if %{build_libubsan}
mv -f %{buildroot}%{_prefix}/%{_lib}/libubsan.*a $FULLLPATH/
%endif
%if %{build_libtsan}
mv -f %{buildroot}%{_prefix}/%{_lib}/libtsan.*a $FULLPATH/
%endif
%if %{build_liblsan}
mv -f %{buildroot}%{_prefix}/%{_lib}/liblsan.*a $FULLPATH/
%endif
%if %{build_go}
mv -f %{buildroot}%{_prefix}/%{_lib}/libgo.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libgobegin.*a $FULLLPATH/
mv -f %{buildroot}%{_prefix}/%{_lib}/libgolibbegin.*a $FULLLPATH/
%endif

%if %{build_ada}
%ifarch sparcv9 ppc
rm -rf $FULLPATH/64/ada{include,lib}
%endif
%ifarch %{multilib_64_archs}
rm -rf $FULLPATH/32/ada{include,lib}
%endif
if [ "$FULLPATH" != "$FULLLPATH" ]; then
mv -f $FULLPATH/ada{include,lib} $FULLLPATH/
pushd $FULLLPATH/adalib
if [ "%{_lib}" = "lib" ]; then
ln -sf ../../../../../libgnarl-*.so libgnarl.so
ln -sf ../../../../../libgnarl-*.so libgnarl-12.so
ln -sf ../../../../../libgnat-*.so libgnat.so
ln -sf ../../../../../libgnat-*.so libgnat-12.so
else
ln -sf ../../../../../../%{_lib}/libgnarl-*.so libgnarl.so
ln -sf ../../../../../../%{_lib}/libgnarl-*.so libgnarl-12.so
ln -sf ../../../../../../%{_lib}/libgnat-*.so libgnat.so
ln -sf ../../../../../../%{_lib}/libgnat-*.so libgnat-12.so
fi
popd
else
pushd $FULLPATH/adalib
if [ "%{_lib}" = "lib" ]; then
ln -sf ../../../../libgnarl-*.so libgnarl.so
ln -sf ../../../../libgnarl-*.so libgnarl-12.so
ln -sf ../../../../libgnat-*.so libgnat.so
ln -sf ../../../../libgnat-*.so libgnat-12.so
else
ln -sf ../../../../../%{_lib}/libgnarl-*.so libgnarl.so
ln -sf ../../../../../%{_lib}/libgnarl-*.so libgnarl-12.so
ln -sf ../../../../../%{_lib}/libgnat-*.so libgnat.so
ln -sf ../../../../../%{_lib}/libgnat-*.so libgnat-12.so
fi
popd
fi
%endif

%ifarch sparcv9 ppc
%if %{build_objc}
ln -sf ../../../../../lib64/libobjc.so.4 64/libobjc.so
%endif
ln -sf ../`echo ../../../../lib/libstdc++.so.6.*[0-9] | sed s~/lib/~/lib64/~` 64/libstdc++.so
ln -sf ../`echo ../../../../lib/libgfortran.so.5.* | sed s~/lib/~/lib64/~` 64/libgfortran.so
ln -sf ../`echo ../../../../lib/libgomp.so.1.* | sed s~/lib/~/lib64/~` 64/libgomp.so
%if %{build_go}
rm -f libgo.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libgo.so.21.* | sed 's,^.*libg,libg,'`' )' > libgo.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libgo.so.21.* | sed 's,^.*libg,libg,'`' )' > 64/libgo.so
%endif
%if %{build_libquadmath}
rm -f libquadmath.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > libquadmath.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > 64/libquadmath.so
%endif
%if %{build_d}
rm -f libgdruntime.so libgphobos.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libgdruntime.so.3.* | sed 's,^.*libg,libg,'`' )' > libgdruntime.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libgdruntime.so.3.* | sed 's,^.*libg,libg,'`' )' > 64/libgdruntime.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libgphobos.so.3.* | sed 's,^.*libg,libg,'`' )' > libgphobos.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libgphobos.so.3.* | sed 's,^.*libg,libg,'`' )' > 64/libgphobos.so
%endif
%if %{build_libitm}
rm -f libitm.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libitm.so.1.* | sed 's,^.*libi,libi,'`' )' > libitm.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libitm.so.1.* | sed 's,^.*libi,libi,'`' )' > 64/libitm.so
%endif
%if %{build_libatomic}
rm -f libatomic.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libatomic.so.1.* | sed 's,^.*liba,liba,'`' )' > libatomic.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libatomic.so.1.* | sed 's,^.*liba,liba,'`' )' > 64/libatomic.so
%endif
%if %{build_libasan}
rm -f libasan.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libasan.so.8.* | sed 's,^.*liba,liba,'`' )' > libasan.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libasan.so.8.* | sed 's,^.*liba,liba,'`' )' > 64/libasan.so
mv ../../../../lib64/libasan_preinit.o 64/libasan_preinit.o
%endif
%if %{build_libubsan}
rm -f libubsan.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib/libubsan.so.1.* | sed 's,^.*libu,libu,'`' )' > libubsan.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib/libubsan.so.1.* | sed 's,^.*libu,libu,'`' )' > 64/libubsan.so
%endif
ln -sf lib32/libgfortran.a libgfortran.a
ln -sf ../lib64/libgfortran.a 64/libgfortran.a
%if %{build_objc}
mv -f %{buildroot}%{_prefix}/lib64/libobjc.*a 64/
%endif
mv -f %{buildroot}%{_prefix}/lib64/libgomp.*a 64/
ln -sf lib32/libstdc++.a libstdc++.a
ln -sf ../lib64/libstdc++.a 64/libstdc++.a
ln -sf lib32/libstdc++fs.a libstdc++fs.a
ln -sf ../lib64/libstdc++fs.a 64/libstdc++fs.a
ln -sf lib32/libstdc++_libbacktrace.a libstdc++_libbacktrace.a
ln -sf ../lib64/libstdc++_libbacktrace.a 64/libstdc++_libbacktrace.a
ln -sf lib32/libsupc++.a libsupc++.a
ln -sf ../lib64/libsupc++.a 64/libsupc++.a
%if %{build_libquadmath}
ln -sf lib32/libquadmath.a libquadmath.a
ln -sf ../lib64/libquadmath.a 64/libquadmath.a
%endif
%if %{build_d}
ln -sf lib32/libgdruntime.a libgdruntime.a
ln -sf ../lib64/libgdruntime.a 64/libgdruntime.a
ln -sf lib32/libgphobos.a libgphobos.a
ln -sf ../lib64/libgphobos.a 64/libgphobos.a
%endif
%if %{build_libitm}
ln -sf lib32/libitm.a libitm.a
ln -sf ../lib64/libitm.a 64/libitm.a
%endif
%if %{build_libatomic}
ln -sf lib32/libatomic.a libatomic.a
ln -sf ../lib64/libatomic.a 64/libatomic.a
%endif
%if %{build_libasan}
ln -sf lib32/libasan.a libasan.a
ln -sf ../lib64/libasan.a 64/libasan.a
%endif
%if %{build_libubsan}
ln -sf lib32/libubsan.a libubsan.a
ln -sf ../lib64/libubsan.a 64/libubsan.a
%endif
%if %{build_go}
ln -sf lib32/libgo.a libgo.a
ln -sf ../lib64/libgo.a 64/libgo.a
ln -sf lib32/libgobegin.a libgobegin.a
ln -sf ../lib64/libgobegin.a 64/libgobegin.a
ln -sf lib32/libgolibbegin.a libgolibbegin.a
ln -sf ../lib64/libgolibbegin.a 64/libgolibbegin.a
%endif
%if %{build_ada}
ln -sf lib32/adainclude adainclude
ln -sf ../lib64/adainclude 64/adainclude
ln -sf lib32/adalib adalib
ln -sf ../lib64/adalib 64/adalib
%endif
%endif
%ifarch %{multilib_64_archs}
mkdir -p 32
%if %{build_objc}
ln -sf ../../../../libobjc.so.4 32/libobjc.so
%endif
ln -sf ../`echo ../../../../lib64/libstdc++.so.6.*[0-9] | sed s~/../lib64/~/~` 32/libstdc++.so
ln -sf ../`echo ../../../../lib64/libgfortran.so.5.* | sed s~/../lib64/~/~` 32/libgfortran.so
ln -sf ../`echo ../../../../lib64/libgomp.so.1.* | sed s~/../lib64/~/~` 32/libgomp.so
%if %{build_go}
rm -f libgo.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libgo.so.21.* | sed 's,^.*libg,libg,'`' )' > libgo.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libgo.so.21.* | sed 's,^.*libg,libg,'`' )' > 32/libgo.so
%endif
%if %{build_libquadmath}
rm -f libquadmath.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > libquadmath.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libquadmath.so.0.* | sed 's,^.*libq,libq,'`' )' > 32/libquadmath.so
%endif
%if %{build_d}
rm -f libgdruntime.so libgphobos.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libgdruntime.so.3.* | sed 's,^.*libg,libg,'`' )' > libgdruntime.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libgdruntime.so.3.* | sed 's,^.*libg,libg,'`' )' > 32/libgdruntime.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libgphobos.so.3.* | sed 's,^.*libg,libg,'`' )' > libgphobos.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libgphobos.so.3.* | sed 's,^.*libg,libg,'`' )' > 32/libgphobos.so
%endif
%if %{build_libitm}
rm -f libitm.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libitm.so.1.* | sed 's,^.*libi,libi,'`' )' > libitm.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libitm.so.1.* | sed 's,^.*libi,libi,'`' )' > 32/libitm.so
%endif
%if %{build_libatomic}
rm -f libatomic.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libatomic.so.1.* | sed 's,^.*liba,liba,'`' )' > libatomic.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libatomic.so.1.* | sed 's,^.*liba,liba,'`' )' > 32/libatomic.so
%endif
%if %{build_libasan}
rm -f libasan.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libasan.so.8.* | sed 's,^.*liba,liba,'`' )' > libasan.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libasan.so.8.* | sed 's,^.*liba,liba,'`' )' > 32/libasan.so
mv ../../../../lib/libasan_preinit.o 32/libasan_preinit.o
%endif
%if %{build_libubsan}
rm -f libubsan.so
echo 'INPUT ( %{_prefix}/lib64/'`echo ../../../../lib64/libubsan.so.1.* | sed 's,^.*libu,libu,'`' )' > libubsan.so
echo 'INPUT ( %{_prefix}/lib/'`echo ../../../../lib64/libubsan.so.1.* | sed 's,^.*libu,libu,'`' )' > 32/libubsan.so
%endif
%if %{build_objc}
mv -f %{buildroot}%{_prefix}/lib/libobjc.*a 32/
%endif
mv -f %{buildroot}%{_prefix}/lib/libgomp.*a 32/
%endif
%ifarch sparc64 ppc64 ppc64p7
ln -sf ../lib32/libgfortran.a 32/libgfortran.a
ln -sf lib64/libgfortran.a libgfortran.a
ln -sf ../lib32/libstdc++.a 32/libstdc++.a
ln -sf lib64/libstdc++.a libstdc++.a
ln -sf ../lib32/libstdc++fs.a 32/libstdc++fs.a
ln -sf lib64/libstdc++fs.a libstdc++fs.a
ln -sf ../lib32/libstdc++_libbacktrace.a 32/libstdc++_libbacktrace.a
ln -sf lib64/libstdc++_libbacktrace.a libstdc++_libbacktrace.a
ln -sf ../lib32/libsupc++.a 32/libsupc++.a
ln -sf lib64/libsupc++.a libsupc++.a
%if %{build_libquadmath}
ln -sf ../lib32/libquadmath.a 32/libquadmath.a
ln -sf lib64/libquadmath.a libquadmath.a
%endif
%if %{build_d}
ln -sf ../lib32/libgdruntime.a 32/libgdruntime.a
ln -sf lib64/libgdruntime.a libgdruntime.a
ln -sf ../lib32/libgphobos.a 32/libgphobos.a
ln -sf lib64/libgphobos.a libgphobos.a
%endif
%if %{build_libitm}
ln -sf ../lib32/libitm.a 32/libitm.a
ln -sf lib64/libitm.a libitm.a
%endif
%if %{build_libatomic}
ln -sf ../lib32/libatomic.a 32/libatomic.a
ln -sf lib64/libatomic.a libatomic.a
%endif
%if %{build_libasan}
ln -sf ../lib32/libasan.a 32/libasan.a
ln -sf lib64/libasan.a libasan.a
%endif
%if %{build_libubsan}
ln -sf ../lib32/libubsan.a 32/libubsan.a
ln -sf lib64/libubsan.a libubsan.a
%endif
%if %{build_go}
ln -sf ../lib32/libgo.a 32/libgo.a
ln -sf lib64/libgo.a libgo.a
ln -sf ../lib32/libgobegin.a 32/libgobegin.a
ln -sf lib64/libgobegin.a libgobegin.a
ln -sf ../lib32/libgolibbegin.a 32/libgolibbegin.a
ln -sf lib64/libgolibbegin.a libgolibbegin.a
%endif
%if %{build_ada}
ln -sf ../lib32/adainclude 32/adainclude
ln -sf lib64/adainclude adainclude
ln -sf ../lib32/adalib 32/adalib
ln -sf lib64/adalib adalib
%endif
%else
%ifarch %{multilib_64_archs}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libgfortran.a 32/libgfortran.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libstdc++.a 32/libstdc++.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libstdc++fs.a 32/libstdc++fs.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libstdc++_libbacktrace.a 32/libstdc++_libbacktrace.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libsupc++.a 32/libsupc++.a
%if %{build_libquadmath}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libquadmath.a 32/libquadmath.a
%endif
%if %{build_d}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libgdruntime.a 32/libgdruntime.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libgphobos.a 32/libgphobos.a
%endif
%if %{build_libitm}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libitm.a 32/libitm.a
%endif
%if %{build_libatomic}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libatomic.a 32/libatomic.a
%endif
%if %{build_libasan}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libasan.a 32/libasan.a
%endif
%if %{build_libubsan}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libubsan.a 32/libubsan.a
%endif
%if %{build_go}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libgo.a 32/libgo.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libgobegin.a 32/libgobegin.a
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/libgolibbegin.a 32/libgolibbegin.a
%endif
%if %{build_ada}
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/adainclude 32/adainclude
ln -sf ../../../%{multilib_32_arch}-%{_vendor}-%{_target_os}/%{gcc_major}/adalib 32/adalib
%endif
%endif
%endif

# If we are building a debug package then copy all of the static archives
# into the debug directory to keep them as unstripped copies.
%if 0%{?_enable_debug_packages}
for d in . $FULLLSUBDIR; do
  mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/$d
  for f in `find $d -maxdepth 1 -a \
		\( -name libasan.a -o -name libatomic.a \
		-o -name libcaf_single.a \
		-o -name libgcc.a -o -name libgcc_eh.a \
		-o -name libgcov.a -o -name libgfortran.a \
		-o -name libgo.a -o -name libgobegin.a \
		-o -name libgolibbegin.a -o -name libgomp.a \
		-o -name libitm.a -o -name liblsan.a \
		-o -name libobjc.a -o -name libgdruntime.a -o -name libgphobos.a \
		-o -name libquadmath.a -o -name libstdc++.a \
		-o -name libstdc++fs.a -o -name libstdc++_libbacktrace.a -o -name libsupc++.a \
		-o -name libtsan.a -o -name libubsan.a \) -a -type f`; do
    cp -a $f $RPM_BUILD_ROOT%{_prefix}/lib/debug%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/$d/
  done
done
%endif

# Strip debug info from Fortran/ObjC/Java static libraries
strip -g `find . \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		    -o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		    -o -name libgdruntime.a -o -name libgphobos.a \
		    -o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		    -o -name libatomic.a -o -name libasan.a -o -name libtsan.a \
		    -o -name libubsan.a -o -name liblsan.a -o -name libcc1.a \) \
		 -a -type f`
popd
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgfortran.so.5.*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgomp.so.1.*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libcc1.so.0.*
%if %{build_libquadmath}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libquadmath.so.0.*
%endif
%if %{build_d}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgdruntime.so.3.*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgphobos.so.3.*
%endif
%if %{build_libitm}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libitm.so.1.*
%endif
%if %{build_libatomic}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libatomic.so.1.*
%endif
%if %{build_libasan}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libasan.so.8.*
chrpath -d %{buildroot}%{_prefix}/%{_lib}/libasan.so.8.*
%endif
%if %{build_libubsan}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libubsan.so.1.*
chrpath -d %{buildroot}%{_prefix}/%{_lib}/libubsan.so.1.*
%endif
%if %{build_libtsan}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libtsan.so.2.*
chrpath -d %{buildroot}%{_prefix}/%{_lib}/libtsan.so.2.*
%endif
%if %{build_liblsan}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/liblsan.so.0.*
chrpath -d %{buildroot}%{_prefix}/%{_lib}/liblsan.so.0.*
%endif
%ifarch aarch64
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libhwasan.so.0.*
chrpath -d %{buildroot}%{_prefix}/%{_lib}/libhwasan.so.0.*
%endif
%if %{build_go}
# Avoid stripping these libraries and binaries.
chmod 644 %{buildroot}%{_prefix}/%{_lib}/libgo.so.21.*
chmod 644 %{buildroot}%{_prefix}/bin/go.gcc
chmod 644 %{buildroot}%{_prefix}/bin/gofmt.gcc
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cgo
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/buildid
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/test2json
chmod 644 %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/vet
%endif
%if %{build_objc}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libobjc.so.4.*
%endif

%if %{build_ada}
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgnarl*so*
chmod 755 %{buildroot}%{_prefix}/%{_lib}/libgnat*so*
%endif

mv $FULLPATH/include-fixed/syslimits.h $FULLPATH/include/syslimits.h
mv $FULLPATH/include-fixed/limits.h $FULLPATH/include/limits.h
for h in `find $FULLPATH/include -name \*.h`; do
  if grep -q 'It has been auto-edited by fixincludes from' $h; then
    rh=`grep -A2 'It has been auto-edited by fixincludes from' $h | tail -1 | sed 's|^.*"\(.*\)".*$|\1|'`
    diff -up $rh $h || :
    rm -f $h
  fi
done

cat > %{buildroot}%{_prefix}/bin/c89 <<"EOF"
#!/bin/sh
fl="-std=c89"
for opt; do
  case "$opt" in
    -ansi|-std=c89|-std=iso9899:1990) fl="";;
    -std=*) echo "`basename $0` called with non ANSI/ISO C option $opt" >&2
	    exit 1;;
  esac
done
exec gcc $fl ${1+"$@"}
EOF
cat > %{buildroot}%{_prefix}/bin/c99 <<"EOF"
#!/bin/sh
fl="-std=c99"
for opt; do
  case "$opt" in
    -std=c99|-std=iso9899:1999) fl="";;
    -std=*) echo "`basename $0` called with non ISO C99 option $opt" >&2
	    exit 1;;
  esac
done
exec gcc $fl ${1+"$@"}
EOF
chmod 755 %{buildroot}%{_prefix}/bin/c?9

cd ..
%find_lang %{name}
%find_lang cpplib

# Remove binaries we will not be including, so that they don't end up in
# gcc-debuginfo
rm -f %{buildroot}%{_prefix}/%{_lib}/{libffi*,libiberty.a} || :
rm -f $FULLEPATH/install-tools/{mkheaders,fixincl}
rm -f %{buildroot}%{_prefix}/lib/{32,64}/libiberty.a
rm -f %{buildroot}%{_prefix}/%{_lib}/libssp*
rm -f %{buildroot}%{_prefix}/%{_lib}/libvtv* || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gfortran || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gccgo || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gcj || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-ar || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-nm || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-ranlib || :
rm -f %{buildroot}%{_prefix}/bin/%{_target_platform}-gdc || :

%ifarch %{multilib_64_archs}
# Remove libraries for the other arch on multilib arches
rm -f %{buildroot}%{_prefix}/lib/lib*.so*
rm -f %{buildroot}%{_prefix}/lib/lib*.a
rm -f %{buildroot}/lib/libgcc_s*.so*
%if %{build_go}
rm -rf %{buildroot}%{_prefix}/lib/go/%{gcc_major}/%{gcc_target_platform}
%ifnarch sparc64 ppc64 ppc64p7
ln -sf %{multilib_32_arch}-%{_vendor}-%{_target_os} %{buildroot}%{_prefix}/lib/go/%{gcc_major}/%{gcc_target_platform}
%endif
%endif
%else
%ifarch sparcv9 ppc
rm -f %{buildroot}%{_prefix}/lib64/lib*.so*
rm -f %{buildroot}%{_prefix}/lib64/lib*.a
rm -f %{buildroot}/lib64/libgcc_s*.so*
%if %{build_go}
rm -rf %{buildroot}%{_prefix}/lib64/go/%{gcc_major}/%{gcc_target_platform}
%endif
%endif
%endif

rm -f %{buildroot}%{mandir}/man3/ffi*

# Help plugins find out nvra.
echo gcc-%{version}-%{release}.%{_arch} > $FULLPATH/rpmver

# Add symlink to lto plugin in the binutils plugin directory.
%{__mkdir_p} %{buildroot}%{_libdir}/bfd-plugins/
ln -s ../../libexec/gcc/%{gcc_target_platform}/%{gcc_major}/liblto_plugin.so \
  %{buildroot}%{_libdir}/bfd-plugins/

%ifarch aarch64
    if [ -e %{buildroot}%{_prefix}/bin/gcc ]; then strip -s %{buildroot}%{_prefix}/bin/gcc; fi
    if [ -e %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-12 ]; then strip -s %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-12; fi
%endif
%ifarch x86_64
    if [ -e %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc ]; then strip -s %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc; fi
    if [ -e %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-12 ]; then strip -s %{buildroot}%{_prefix}/bin/%{_target_platform}-gcc-12; fi
%endif

# Remove installed but unpacked files.
# If any file is useful in the future, remove it from here.
if [ -f %{buildroot}%{_prefix}/bin/gnatgcc ]; then rm -f %{buildroot}%{_prefix}/bin/gnatgcc; fi
if [ -f %{buildroot}%{_prefix}/lib64/libasan.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libasan.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libasan.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libasan.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libatomic.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libatomic.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libgcc_s.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libgcc_s.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libgfortran.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libgfortran.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libgomp.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libgomp.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libitm.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libitm.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/liblsan.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/liblsan.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libobjc.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libobjc.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libquadmath.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libquadmath.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libstdc++.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libstdc++.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libtsan.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libtsan.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libubsan.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libubsan.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libhwasan.a ]; then  rm -f %{buildroot}%{_prefix}/lib64/libhwasan.a; fi
if [ -f %{buildroot}%{_prefix}/lib64/libhwasan.so ]; then  rm -f %{buildroot}%{_prefix}/lib64/libhwasan.so; fi
if [ -f %{buildroot}%{_prefix}/lib64/libhwasan.so.0 ]; then  rm -f %{buildroot}%{_prefix}/lib64/libhwasan.so.0; fi
if [ -f %{buildroot}%{_prefix}/lib64/libhwasan.so.0.0.0 ]; then  rm -f %{buildroot}%{_prefix}/lib64/libhwasan.so.0.0.0; fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/README ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/README;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/pthread.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/pthread.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/X11/Xw32defs.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/X11/Xw32defs.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/slang.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/slang.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/slang/slang.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include-fixed/slang/slang.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/ssp.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/ssp.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/stdio.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/stdio.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/string.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/string.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/unistd.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ssp/unistd.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/fixinc_list ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/fixinc_list;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/gsyslimits.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/gsyslimits.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/include/README ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/include/README;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/include/limits.h ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/include/limits.h;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/macro_list ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/macro_list;
fi

if [ -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/mkheaders.conf ];
then
  rm -f %{buildroot}%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/mkheaders.conf;
fi

if [ -f %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/fixinc.sh ];
then
  rm -f %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/fixinc.sh;
fi

if [ -f %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/mkinstalldirs ];
then
  rm -f %{buildroot}%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/install-tools/mkinstalldirs;
fi

if [ -f %{buildroot}%{_prefix}/share/locale/de/LC_MESSAGES/libstdc++.mo ];
then
  rm -f %{buildroot}%{_prefix}/share/locale/de/LC_MESSAGES/libstdc++.mo;
fi

if [ -f %{buildroot}%{_prefix}/share/locale/fr/LC_MESSAGES/libstdc++.mo ];
then
  rm -f %{buildroot}%{_prefix}/share/locale/fr/LC_MESSAGES/libstdc++.mo;
fi

%check
cd obj-%{gcc_target_platform}

# run the tests.
%if %{check_enable}
LC_ALL=C make %{?_smp_mflags} -k check ALT_CC_UNDER_TEST=gcc ALT_CXX_UNDER_TEST=g++ \
     RUNTESTFLAGS="--target_board=unix/'{,-fstack-protector-strong}'" || :
echo ====================TESTING=========================
( LC_ALL=C ../contrib/test_summary || : ) 2>&1 | sed -n '/^cat.*EOF/,/^EOF/{/^cat.*EOF/d;/^EOF/d;/^LAST_UPDATED:/d;p;}'
echo ====================TESTING END=====================
mkdir testlogs-%{_target_platform}-%{version}-%{release}
for i in `find . -name \*.log | grep -F testsuite/ | grep -v 'config.log\|acats.*/tests/'`; do
  ln $i testlogs-%{_target_platform}-%{version}-%{release}/ || :
done
tar cf - testlogs-%{_target_platform}-%{version}-%{release} | xz -9e \
  | uuencode testlogs-%{_target_platform}.tar.xz || :
rm -rf testlogs-%{_target_platform}-%{version}-%{release}
%endif

%post go
%{_sbindir}/update-alternatives --install \
  %{_prefix}/bin/go go %{_prefix}/bin/go.gcc 92 \
  --slave %{_prefix}/bin/gofmt gofmt %{_prefix}/bin/gofmt.gcc

%preun go
if [ $1 = 0 ]; then
  %{_sbindir}/update-alternatives --remove go %{_prefix}/bin/go.gcc
fi

# Because glibc Prereq's libgcc and /sbin/ldconfig
# comes from glibc, it might not exist yet when
# libgcc is installed
%post -n libgcc -p <lua>
if posix.access ("/sbin/ldconfig", "x") then
  local pid = posix.fork ()
  if pid == 0 then
    posix.exec ("/sbin/ldconfig")
  elseif pid ~= -1 then
    posix.wait (pid)
  end
end

%postun -n libgcc -p <lua>
if posix.access ("/sbin/ldconfig", "x") then
  local pid = posix.fork ()
  if pid == 0 then
    posix.exec ("/sbin/ldconfig")
  elseif pid ~= -1 then
    posix.wait (pid)
  end
end

%ldconfig_scriptlets -n libstdc++

%ldconfig_scriptlets -n libobjc

%ldconfig_scriptlets -n libgfortran

%ldconfig_scriptlets -n libgphobos

%ldconfig_scriptlets -n libgnat

%ldconfig_scriptlets -n libgomp

%ldconfig_scriptlets gdb-plugin

%ldconfig_scriptlets -n libgccjit

%ldconfig_scriptlets -n libquadmath

%ldconfig_scriptlets -n libitm

%ldconfig_scriptlets -n libatomic

%ldconfig_scriptlets -n libasan

%ldconfig_scriptlets -n libubsan

%ldconfig_scriptlets -n libtsan

%ldconfig_scriptlets -n liblsan

%ldconfig_scriptlets -n libgo

%files -f %{name}.lang
%{_prefix}/bin/cc
%{_prefix}/bin/c89
%{_prefix}/bin/c99
%{_prefix}/bin/gcc
%{_prefix}/bin/gcov
%{_prefix}/bin/gcov-tool
%{_prefix}/bin/gcov-dump
%{_prefix}/bin/gcc-ar
%{_prefix}/bin/gcc-nm
%{_prefix}/bin/gcc-ranlib
%{_prefix}/bin/lto-dump
%ifarch ppc
%{_prefix}/bin/%{_target_platform}-gcc
%endif
%ifarch sparc64 sparcv9
%{_prefix}/bin/sparc-%{_vendor}-%{_target_os}-gcc
%endif
%ifarch ppc64 ppc64p7
%{_prefix}/bin/ppc-%{_vendor}-%{_target_os}-gcc
%endif
%{_prefix}/bin/%{gcc_target_platform}-gcc*
%{_mandir}/man1/gcc.1*
%{_mandir}/man1/gcov.1*
%{_mandir}/man1/gcov-tool.1*
%{_mandir}/man1/gcov-dump.1*
%{_mandir}/man1/lto-dump.1*
%{_mandir}/man7/*
%{_infodir}/gcc*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/lto1
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/lto-wrapper
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/liblto_plugin.so*
%{_libdir}/bfd-plugins/liblto_plugin.so
%{_prefix}/libexec/gcc/onnx.fdata
%{_prefix}/libexec/gcc/optimizer.fdata
%ifarch aarch64
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/libbolt_plugin.so*
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/rpmver
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stddef.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdarg.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdfix.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/varargs.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/float.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/limits.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdbool.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/iso646.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/syslimits.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/unwind.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/omp.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/openacc.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/acc_prof.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdint.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdint-gcc.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdalign.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdnoreturn.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/stdatomic.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/gcov.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/simdmath.h
%ifarch %{ix86} x86_64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/emmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/pmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/tmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ammintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/smmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/nmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/bmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/wmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/immintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avxintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/x86intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/fma4intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xopintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/lwpintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/popcntintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/bmiintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/tbmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ia32intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx2intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/bmi2intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/f16cintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/fmaintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/lzcntintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/rtmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xtestintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/adxintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/prfchwintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/rdseedintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/fxsrintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xsaveintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xsaveoptintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512cdintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512erintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512fintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512pfintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/shaintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mm_malloc.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mm3dnow.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/cpuid.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/cross-stdarg.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512bwintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512dqintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512ifmaintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512ifmavlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vbmiintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vbmivlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vlbwintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vldqintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/clflushoptintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/clwbintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mwaitxintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xsavecintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xsavesintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/clzerointrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/pkuintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx5124fmapsintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx5124vnniwintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vpopcntdqintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/sgxintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/gfniintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/cetintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/cet.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vbmi2intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vbmi2vlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vnniintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vnnivlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/vaesintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/vpclmulqdqintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vpopcntdqvlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512bitalgintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/pconfigintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/wbnoinvdintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/movdirintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/waitpkgintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/cldemoteintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512bf16vlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512bf16intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/enqcmdintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vp2intersectintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512vp2intersectvlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/serializeintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/tsxldtrkintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/amxtileintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/amxint8intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/amxbf16intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/amxfp16intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/amxcomplexintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/x86gprintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/uintrintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/hresetintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/keylockerintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/prfchiintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avxvnniintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mwaitintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512fp16intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/avx512fp16vlintrin.h
%endif
%ifarch ia64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ia64intrin.h
%endif
%ifarch ppc ppc64 ppc64le ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ppc-asm.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/altivec.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ppu_intrinsics.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/si2vmx.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/spu2vmx.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/vec_types.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/htmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/htmxlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/bmi2intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/bmiintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/xmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mm_malloc.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/emmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/x86intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/pmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/tmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/smmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/amo.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/nmmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/immintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/x86gprintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/rs6000-vecdefines.h
%endif
%ifarch %{arm}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/unwind-arm-common.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/mmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_neon.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_acle.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_cmse.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_fp16.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_bf16.h
%endif
%ifarch aarch64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_neon.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_acle.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_fp16.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_bf16.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_sve.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/arm_sme.h
%endif
%ifarch loongarch64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/larchintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/loongarch/loongarch-protos.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/loongarch/loongarch-opts.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/loongarch/loongarch-str.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/loongarch/loongarch-def.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/loongarch/loongarch-tune.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/loongarch/loongarch-driver.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/lsxintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/lasxintrin.h
%endif
%ifarch sparc sparcv9 sparc64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/visintrin.h
%endif
%ifarch s390 s390x
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/s390intrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/htmintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/htmxlintrin.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/vecintrin.h
%endif
%ifarch sw_64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/sw_64/sw_64-protos.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include/config/sw_64/sw_64.h
%endif
%if %{build_libasan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/sanitizer
%endif
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/collect2
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgcc_s.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgomp.spec
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgomp.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgomp.so
%if %{build_libitm}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libitm.spec
%endif
%if %{build_libasan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libsanitizer.spec
%endif
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgcc_s.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgomp.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgomp.so
%if %{build_libquadmath}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libquadmath.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libquadmath.so
%endif
%if %{build_libitm}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libitm.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libitm.so
%endif
%if %{build_libatomic}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libatomic.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libatomic.so
%endif
%if %{build_libasan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libasan.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libasan.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libasan_preinit.o
%endif
%if %{build_libubsan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libubsan.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libubsan.so
%endif
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/crt*.o
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgcc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgcov.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgcc_eh.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgcc_s.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgomp.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgomp.so
%if %{build_libquadmath}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libquadmath.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libquadmath.so
%endif
%if %{build_libitm}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libitm.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libitm.so
%endif
%if %{build_libatomic}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libatomic.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libatomic.so
%endif
%if %{build_libasan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libasan.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libasan.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libasan_preinit.o
%endif
%if %{build_libubsan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libubsan.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libubsan.so
%endif
%endif
%ifarch sparcv9 sparc64 ppc ppc64 ppc64p7
%if %{build_libquadmath}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libquadmath.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libquadmath.so
%endif
%if %{build_libitm}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libitm.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libitm.so
%endif
%if %{build_libatomic}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libatomic.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libatomic.so
%endif
%if %{build_libasan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libasan.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libasan.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libasan_preinit.o
%endif
%if %{build_libubsan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libubsan.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libubsan.so
%endif
%else
%if %{build_libatomic}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libatomic.so
%endif
%if %{build_libasan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libasan.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libasan_preinit.o
%endif
%if %{build_libubsan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libubsan.so
%endif
%endif
%if %{build_libtsan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libtsan.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libtsan_preinit.o
%endif
%if %{build_liblsan}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/liblsan.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/liblsan_preinit.o
%endif
%{_prefix}/libexec/getconf/default
%doc gcc/README* rpm.doc/changelogs/gcc/ChangeLog* 
%{!?_licensedir:%global license %%doc}
%license gcc/COPYING* COPYING.RUNTIME

%files -n cpp -f cpplib.lang
%{_prefix}/lib/cpp
%{_prefix}/bin/cpp
%{_mandir}/man1/cpp.1*
%{_infodir}/cpp*
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cc1

%files -n libgcc
/%{_lib}/libgcc_s-%{gcc_major}.so.1
/%{_lib}/libgcc_s.so.1
%{!?_licensedir:%global license %%doc}
%license gcc/COPYING* COPYING.RUNTIME

%files c++
%{_prefix}/bin/%{gcc_target_platform}-*++
%{_prefix}/bin/g++
%{_prefix}/bin/c++
%{_mandir}/man1/g++.1*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cc1plus
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/g++-mapper-server
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libstdc++.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libstdc++fs.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libstdc++_libbacktrace.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libsupc++.a
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libstdc++.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libstdc++fs.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libstdc++_libbacktrace.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libsupc++.a
%endif
%ifarch sparcv9 ppc %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++.so
%endif
%ifarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++fs.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++_libbacktrace.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libsupc++.a
%endif
%doc rpm.doc/changelogs/gcc/cp/ChangeLog*

%files -n libstdc++
%{_prefix}/%{_lib}/libstdc++.so.6*
%dir %{_datadir}/gdb
%dir %{_datadir}/gdb/auto-load
%dir %{_datadir}/gdb/auto-load/%{_prefix}
%dir %{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/
# Package symlink to keep compatibility
%ifarch riscv64
%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/lp64d
%endif
%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/libstdc*gdb.py*
%{_datadir}/gdb/auto-load/%{_prefix}/%{_lib}/__pycache__
%dir %{_prefix}/share/gcc-%{gcc_major}
%dir %{_prefix}/share/gcc-%{gcc_major}/python
%{_prefix}/share/gcc-%{gcc_major}/python/libstdcxx

%files -n libstdc++-devel
%dir %{_prefix}/include/c++
%{_prefix}/include/c++/%{gcc_major}
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifnarch sparcv9 ppc %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++.so
%endif
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libstdc++fs.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libstdc++_libbacktrace.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libstdc++fs.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libstdc++_libbacktrace.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++fs.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++_libbacktrace.a
%endif
%doc rpm.doc/changelogs/libstdc++-v3/ChangeLog* libstdc++-v3/README*

%files -n libstdc++-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libsupc++.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libsupc++.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libstdc++.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libsupc++.a
%endif

%if %{build_libstdcxx_docs}
%files -n libstdc++-docs
%{_mandir}/man3/*
%doc rpm.doc/libstdc++-v3/html
%endif

%if %{build_objc}
%files objc
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/objc
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cc1obj
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libobjc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libobjc.so
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libobjc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libobjc.so
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libobjc.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libobjc.so
%endif
%doc rpm.doc/objc/*
%doc libobjc/THREADS* rpm.doc/changelogs/libobjc/ChangeLog*

%files objc++
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cc1objplus

%files -n libobjc
%{_prefix}/%{_lib}/libobjc.so.4*
%endif

%files gfortran
%{_prefix}/bin/gfortran
%{_prefix}/bin/f95
%{_mandir}/man1/gfortran.1*
%{_infodir}/gfortran*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/ISO_Fortran_binding.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/omp_lib.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/omp_lib.f90
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/omp_lib.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/omp_lib_kinds.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/openacc.f90
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/openacc.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/openacc_kinds.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/openacc_lib.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/ieee_arithmetic.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/ieee_exceptions.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/ieee_features.mod
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/finclude/simdmath_f.h
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/f951
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgfortran.spec
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libcaf_single.a
%ifarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgfortran.a
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgfortran.so
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libcaf_single.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgfortran.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgfortran.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/finclude
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libcaf_single.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgfortran.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgfortran.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/finclude
%endif
%dir %{_fmoddir}
%doc rpm.doc/gfortran/*

%files -n libgfortran
%{_prefix}/%{_lib}/libgfortran.so.5*

%files -n libgfortran-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libgfortran.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libgfortran.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgfortran.a
%endif

%if %{build_d}
%files gdc
%{_prefix}/bin/gdc
%{_mandir}/man1/gdc.1*
%{_infodir}/gdc*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/d
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/d21
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgphobos.spec
%ifarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgdruntime.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgphobos.a
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgdruntime.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgphobos.so
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgdruntime.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgphobos.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgdruntime.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgphobos.so
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgdruntime.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgphobos.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgdruntime.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgphobos.so
%endif
%doc rpm.doc/gdc/*

%files -n libgphobos
%{_prefix}/%{_lib}/libgdruntime.so.3*
%{_prefix}/%{_lib}/libgphobos.so.3*
%doc rpm.doc/libphobos/*

%files -n libgphobos-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libgdruntime.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libgphobos.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libgdruntime.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libgphobos.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgdruntime.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgphobos.a
%endif
%endif

%if %{build_ada}
%files gnat
%{_prefix}/bin/gnat
%{_prefix}/bin/gnat[^i]*
%{_infodir}/gnat*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/ada_target_properties
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/ada_target_properties
%endif
%ifarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib
%endif
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/ada_target_properties
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/gnat1
%doc rpm.doc/changelogs/gcc/ada/ChangeLog*

%files -n libgnat
%{_prefix}/%{_lib}/libgnat-*.so
%{_prefix}/%{_lib}/libgnarl-*.so

%files -n libgnat-devel
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adalib
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adalib/libgnat.a
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adalib/libgnarl.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adalib
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adalib/libgnat.a
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adalib/libgnarl.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adainclude
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib/libgnat.a
%exclude %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib/libgnarl.a
%endif

%files -n libgnat-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adalib/libgnat.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/adalib/libgnarl.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adalib/libgnat.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/adalib/libgnarl.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib/libgnat.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/adalib/libgnarl.a
%endif
%endif

%files -n libgomp
%{_prefix}/%{_lib}/libgomp.so.1*
%{_infodir}/libgomp.info*
%doc rpm.doc/changelogs/libgomp/ChangeLog*

%if %{build_libquadmath}
%files -n libquadmath
%{_prefix}/%{_lib}/libquadmath.so.0*
%{_infodir}/libquadmath.info*
%{!?_licensedir:%global license %%doc}
%license rpm.doc/libquadmath/COPYING*

%files -n libquadmath-devel
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/quadmath.h
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include/quadmath_weak.h
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libquadmath.so
%endif
%doc rpm.doc/libquadmath/ChangeLog*

%files -n libquadmath-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libquadmath.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libquadmath.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libquadmath.a
%endif
%endif

%if %{build_libitm}
%files -n libitm
%{_prefix}/%{_lib}/libitm.so.1*
%{_infodir}/libitm.info*

%files -n libitm-devel
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/include
#%%{_prefix}/lib/gcc/%%{gcc_target_platform}/%%{gcc_major}/include/itm.h
#%%{_prefix}/lib/gcc/%%{gcc_target_platform}/%%{gcc_major}/include/itm_weak.h
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libitm.so
%endif
%doc rpm.doc/libitm/ChangeLog*

%files -n libitm-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libitm.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libitm.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libitm.a
%endif
%endif

%if %{build_libatomic}
%files -n libatomic
%{_prefix}/%{_lib}/libatomic.so.1*

%files -n libatomic-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libatomic.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libatomic.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libatomic.a
%endif
%doc rpm.doc/changelogs/libatomic/ChangeLog*
%endif

%if %{build_libasan}
%files -n libasan
%{_prefix}/%{_lib}/libasan.so.8*

%files -n libasan-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libasan.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libasan.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libasan.a
%endif
%doc rpm.doc/changelogs/libsanitizer/ChangeLog*
%{!?_licensedir:%global license %%doc}
%license libsanitizer/LICENSE.TXT
%endif

%if %{build_libubsan}
%files -n libubsan
%{_prefix}/%{_lib}/libubsan.so.1*

%files -n libubsan-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libubsan.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libubsan.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libubsan.a
%endif
%doc rpm.doc/changelogs/libsanitizer/ChangeLog*
%{!?_licensedir:%global license %%doc}
%license libsanitizer/LICENSE.TXT
%endif

%if %{build_libtsan}
%files -n libtsan
%{_prefix}/%{_lib}/libtsan.so.2*

%files -n libtsan-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libtsan.a
%doc rpm.doc/changelogs/libsanitizer/ChangeLog*
%{!?_licensedir:%global license %%doc}
%license libsanitizer/LICENSE.TXT
%endif

%if %{build_liblsan}
%files -n liblsan
%{_prefix}/%{_lib}/liblsan.so.0*

%files -n liblsan-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/liblsan.a
%doc rpm.doc/changelogs/libsanitizer/ChangeLog*
%{!?_licensedir:%global license %%doc}
%license libsanitizer/LICENSE.TXT
%endif

%if %{build_go}
%files go
%ghost %{_prefix}/bin/go
%attr(755,root,root) %{_prefix}/bin/go.gcc
%{_prefix}/bin/gccgo
%ghost %{_prefix}/bin/gofmt
%attr(755,root,root) %{_prefix}/bin/gofmt.gcc
%{_mandir}/man1/gccgo.1*
%{_mandir}/man1/go.1*
%{_mandir}/man1/gofmt.1*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/go1
%attr(755,root,root) %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/cgo
%attr(755,root,root) %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/buildid
%attr(755,root,root) %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/test2json
%attr(755,root,root) %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/vet
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgo.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgo.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/64/libgolibbegin.a
%endif
%ifarch %{multilib_64_archs}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgo.so
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgo.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/32/libgolibbegin.a
%endif
%ifarch sparcv9 ppc %{multilib_64_archs}
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgo.so
%endif
%ifarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgo.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgolibbegin.a
%endif
%doc rpm.doc/go/*

%files -n libgo
%attr(755,root,root) %{_prefix}/%{_lib}/libgo.so.21*
%doc rpm.doc/libgo/*

%files -n libgo-devel
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/%{_lib}/go
%dir %{_prefix}/%{_lib}/go/%{gcc_major}
%{_prefix}/%{_lib}/go/%{gcc_major}/%{gcc_target_platform}
%ifarch %{multilib_64_archs}
%ifnarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/go
%dir %{_prefix}/lib/go/%{gcc_major}
%{_prefix}/lib/go/%{gcc_major}/%{gcc_target_platform}
%endif
%endif
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libgolibbegin.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libgolibbegin.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgobegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgolibbegin.a
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgo.so
%endif

%files -n libgo-static
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%ifarch sparcv9 ppc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib32/libgo.a
%endif
%ifarch sparc64 ppc64 ppc64p7
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/lib64/libgo.a
%endif
%ifnarch sparcv9 sparc64 ppc ppc64 ppc64p7
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/libgo.a
%endif
%endif

%files -n libgccjit
%{_prefix}/%{_lib}/libgccjit.so.*
%doc rpm.doc/changelogs/gcc/jit/ChangeLog*

%files -n libgccjit-devel
%{_prefix}/%{_lib}/libgccjit.so
%{_prefix}/include/libgccjit*.h
%{_infodir}/libgccjit.info*
%doc rpm.doc/libgccjit-devel/*
%doc gcc/jit/docs/examples

%files plugin-devel
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/gtype.state
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/include
%dir %{_prefix}/libexec/gcc
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}
%dir %{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}
%{_prefix}/libexec/gcc/%{gcc_target_platform}/%{gcc_major}/plugin

%files gdb-plugin
%{_prefix}/%{_lib}/libcc1.so*
%dir %{_prefix}/lib/gcc
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}
%dir %{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/libcc1plugin.so*
%{_prefix}/lib/gcc/%{gcc_target_platform}/%{gcc_major}/plugin/libcp1plugin.so*
%doc rpm.doc/changelogs/libcc1/ChangeLog*

%changelog
* Sun Mar 09 2025 huang-xiaoquan <huangxiaoquan1@huawei> - 12.3.1-75
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC: fix residual ssa_name issue

* Sun Mar 09 2025 liyancheng <412998149@qq.com> - 12.3.1-74
- Type:Bugfix
- DESC: disable malloc support when struct_layout_optimize_level > 1

* Thu Feb 27 2025 huzife <634763349@qq.com> - 12.3.1-73
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Avoid doing sfc with struct_split and compressing dead fields.

* Sat Feb 22 2025 huzife <634763349@qq.com> - 12.3.1-72
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Fix errors in ipa-struct-sfc(issue: IBMY84, IBN2JO, IBN42Q)

* Wed Feb 19 2025 liyancheng <412998149@qq.com> - 12.3.1-71
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Add hip10a, fix hip11 and hip10c addrcost table.

* Mon Feb 17 2025 Hu,Lin1 <lin1.hu@intel.com> - 12.3.1-70
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patches from openeuler/gcc.

* Wed Feb 12 2025 huang-xiaoquan <huangxiaoquan1@huawei.com> - 12.3.1-69
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Reduce ipa-inline warning output.

* Mon Feb 10 2025 huzife <634763349@qq.com> - 12.3.1-68
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Add struct static field compression.

* Sat Feb 08 2025 liyancheng <412998149@qq.com> - 12.3.1-67
- Type:Sync 2a2a8e0017d817439532400d5c24f7bbe701b9f6 from 24.09
- ID:NA
- SUG:NA
- DESC:set default configuration for the ppc64le.

* Tue Dec 31 2024 liyancheng <412998149@qq.com> - 12.3.1-66
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Don't use local_detect_cpu when cross build and fix hip09 costs.

* Mon Dec 30 2024 liyancheng <412998149@qq.com> - 12.3.1-65
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Update the gate of cspgo.

* Mon Dec 30 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-64
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Sync patches from openeuler/gcc.

* Thu Dec 26 2024 rfwang07 <wangrufeng5@huawei.com> - 12.3.1-63
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Fix function missing return value.

* Wed Dec 18 2024 Mingchuan Wu <wumingchuan1992@foxmail.com> - 12.3.1-62
- Type:Sync
- ID:NA
- SUG:NA
- DESC: Sync patches from openeuler/gcc.

* Mon Dec 16 2024 huzife <634763349@qq.com> - 12.3.1-61
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Add checking for ssa_name in if-split

* Mon Dec 16 2024 huang-xiaoquan <huangxiaoquan1@huawei.com> - 12.3.1-60
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Adjust the same gate to use struct option.

* Wed Dec 11 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-59
- Type:Sync
- ID:NA
- SUG:NA
- DESC: Sync patches from openeuler/gcc.

* Tue Dec 10 2024 huzife <634763349@qq.com> - 12.3.1-58
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC: Fix bugs in cmlt, replace tmp pattern split.

* Mon Dec 09 2024 liyancheng <412998149@qq.com> - 12.3.1-57
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC: Fix bugs in strcut-reorg and ipa-prefetch.

* Sat Dec 07 2024 huzife <634763349@qq.com> - 12.3.1-56
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC: Fix bugs for if-split.

* Wed Dec 04 2024 liyancheng <412998149@qq.com> - 12.3.1-55
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC: Fix bugs for cspgo.

* Wed Dec 04 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-54
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patches from openeuler/gcc.

* Wed Dec 04 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-53
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patches from openeuler/gcc.

* Wed Dec 04 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-52
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Bugfix add no var recored check for ssa_name in struct reorg.

* Wed Dec 04 2024 Mingchuan Wu <wumingchuan1992@foxmail.com> - 12.3.1-51
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync [late-SLP], [tracer-opt], [bugfix4hip09] patches from openeuler/gcc.

* Thu Nov 28 2024 swcompiler <lc@wxiat.com> - 12.3.1-50
- Type: Sw64
- DESC:
- Add sw64 architecture support.

* Wed Nov 27 2024 liyancheng <412998149@qq.com> - 12.3.1-49
- Type: Sync
- DESC: Add if split optimization

* Wed Nov 27 2024 Peng Fan <fanpeng@loongson.cn> - 12.3.1-48
- Type: BUGFIX
- DESC:
- LoongArch: Allow attributes in non-gnu namespaces.

* Wed Nov 27 2024 liyancheng <412998149@qq.com> - 12.3.1-47
- Type: Sync
- DESC: Add cfgo-pgo optimization

* Wed Nov 27 2024 liyancheng <412998149@qq.com> - 12.3.1-46
- Type: Sync
- DESC: Add context sensitive PGO

* Mon Nov 25 2024 Peng Fan <fanpeng@loongson.cn> - 12.3.1-45
- Type: BUGFIX
- DESC:
- Fix indentation and numbering errors in makefile.

* Thu Nov 21 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 12.3.1-44
- Type: Sync
- DESC: Sync patches for fixing building issues with clang
- Source: https://gitee.com/openeuler/gcc/pulls/239

* Thu Nov 21 2024 YunQiang Su <yunqiang@isrc.iscas.ac.cn> - 12.3.1-43
- Type: Sync
- DESC: RISC-V: Install libstdc++/libcc1 etc to /lib64 instead of lib

* Thu Nov 21 2024 Feiyang Liu <liufeiyang6@huawei.com> - 12.3.1-42
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Sync backport patch varasm-Handle-private-COMDAT from upstream.

* Thu Nov 21 2024 liyancheng <412998149@qq.com> - 12.3.1-41
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Multi-version lto symbol parse and lto units ipa-inline extension

* Thu Nov 21 2024 liyancheng <412998149@qq.com> - 12.3.1-40
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add missing header file for x86

* Thu Nov 21 2024 huangzifeng <huangzifeng6@huawei.com> - 12.3.1-39
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patches from openeuler/gcc

* Thu Nov 21 2024 huangzifeng <huangzifeng6@huawei.com> - 12.3.1-38
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patches from branch openEuler-24.09

* Wed Nov 20 2024 Hu,Lin1 <lin1.hu@inte.com> - 12.3.1-37
- Type:Sync
- ID:NA
- SUG:NA
- DESC: Sync some patch from src-openEuler/gcc's openeuler-24.09

* Tue Nov 19 2024 eastb233 <xiezhiheng@huawei.com> - 12.3.1-36
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Recover CVE-2023-4039

* Mon Nov 18 2024 eastb233 <xiezhiheng@huawei.com> - 12.3.1-35
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Apply SME patches

* Mon Nov 18 2024 eastb233 <xiezhiheng@huawei.com> - 12.3.1-34
- Type:Revert
- ID:NA
- SUG:NA
- DESC:Revert CVE-2023-4039 to apply SME patches

* Tue Nov 5 2024 Peng Fan <fanpeng@loongson.cn> - 12.3.1-33
- Type: Sync
- DESC:
- LoongArch: Sync patch from upstream
- Tweaks OSDIR are consistent with most other distributions.

* Fri Sep 20 2024 fuanan <fuanan3@h-partners.com> - 12.3.1-32
- Type:Sync
- ID:NA
- SUG:NA
- DESC:Sync patch for CVE-2023-4039

* Thu Jul 11 2024 huyubiao <huyubiao@huawei.com> - 12.3.1-31
- Type:SPEC
- ID:NA
- SUG:NA
- DESC:disable isl

* Fri May 17 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-30
- Type: Sync
- DESC: Sync bug fix patch from openeuler/gcc

* Sat May 11 2024 tiancheng-bao <baotiancheng1@huawei.com> - 12.3.1-29
- Type: Sync
- DESC: Sync bug fix patch from openeuler/gcc

* Mon Apr 29 2024 huang-xiaoquan <huangxiaoquan1@huawei.com> - 12.3.1-28
- Type: BUGFIX
- DESC: StructReorderFields-Fix-gimple-call-not-rewritten.

* Fri Apr 26 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-27
- Type: BUGFIX
- DESC: Update the configure file for BOLT.

* Fri Apr 26 2024 Zheng Chenhui <zhengchenhui1@huawei.com> - 12.3.1-26
- Type: Revert
- DESC: Revert Intel patches.

* Wed Apr 24 2024 Wang Ding <wangding16@huawei.com> - 12.3.1-25
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Tue Apr 23 2024 laokz <zhangkai@iscas.ac.cn> - 12.3.1-24
- Type: SPEC
- DESC: riscv64 enable libasan, libusan package

* Mon Apr 22 2024 Peng Fan <fanpeng@loongson.cn> - 12.3.1-23
- DESC: Add LoongArch 3A6000 support

* Fri Apr 12 2024 Zhengchen Hui <zhengchenhui1@huawei.com> - 12.3.1-22
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Thu Apr 11 2024 Zhengchen Hui <zhengchenhui1@huawei.com> - 12.3.1-21
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Thu Apr 11 2024 Zhenyu Zhao <zhaozhenyu17@huawei.com> - 12.3.1-20
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Mon Apr 1 2024 Peng Fan <fanpeng@loongson.cn> 12.3.1-19
- Type: SPEC
- DESC: fix libcc1 file path for LoongArch.

* Wed Mar 27 2024 Peng Fan <fanpeng@loongson.cn> 12.3.1-18
- Type: Sync
- DESC: Sync patch from gcc upstream
- LoongArch: support libsanitizer
- separate LoongArch's patch

* Mon Jan 15 2024 laokz <zhangkai@iscas.ac.cn> 12.3.1-17
- Type: SPEC
- DESC: riscv64 -march default to rv64gc

* Mon Sep 11 2023 dingguangya <dingguangya1@huawei.com> 12.3.1-16
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Thu Sep 07 2023 zhaozhenyu <zhaozhenyu17@huawei.com> 12.3.1-15
- Type: SPEC
- DESC: Enable Strip for aarch64 and x86_64 

* Wed Sep 06 2023 eastb233 <xiezhiheng@huawei.com> 12.3.1-14
- Type: Bugfix
- DESC: Package simdmath.h and simdmath_f.h

* Tue Sep 05 2023 liyancheng <412998149@qq.com> 12.3.1-13
- Type: Bugfix
- DESC: Remove installed but unpacked files

* Tue Sep 05 2023 zhaozhenyu <zhaozhenyu17@huawei.com> 12.3.1-12
- Type: SPEC
- DESC: Enable Strip for gcc

* Tue Sep 05 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.1-11
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Mon Sep 04 2023 dingguangya <dingguangya1@huawei.com> 12.3.1-10
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Tue Aug 29 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.1-9
- Type: Sync
- DESC: Sync patch from openeuler/gcc part 2

* Tue Aug 29 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.1-8
- Type: Sync
- DESC: Sync patch from openeuler/gcc

* Fri Aug 11 2023 Hongyu Wang <hongyu.wang@intel.com> 12.3.1-7
- Type:Sync
- i386: Only enable small loop unrolling in backend [PR 107692].

* Fri Aug 11 2023 Hongyu Wang <hongyu.wang@intel.com> 12.3.1-6
- Type:Sync
- Enable small loop unrolling for O2.

* Fri Aug 11 2023 Cui,Lili <lili.cui@intel.com> 12.3.1-5
- Type:Sync
- Add attribute hot judgement for INLINE_HINT_known_hot hint.

* Mon Jul 17 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.1-4
- Type:SPEC
- DESC:Enable libquadmath on kunpeng

* Fri Jul 14 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.1-3
- Type:Sync
- DESC:Enable libquadmath on kunpeng

* Tue Jul 11 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.1-2
- Type:SPEC
- DESC:Set version to 12.3.1

* Tue Jun 13 2023 huangxiaoquan <huangxiaoquan1@huawei.com> 12.3.0-1
- Type:Init
- DESC:Init GCC 12.3.0 repository
