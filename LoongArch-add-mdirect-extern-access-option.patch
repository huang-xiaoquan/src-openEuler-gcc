From 22f6d3fad184d87f3dac7634537fdbc24846bab9 Mon Sep 17 00:00:00 2001
From: Xi Ruoyao <xry111@xry111.site>
Date: Thu, 1 Sep 2022 18:38:14 +0800
Subject: [PATCH 016/124] LoongArch: add -mdirect-extern-access option

As a new target, LoongArch does not use copy relocation as it's
problematic in some circumstances.  One bad consequence is we are
emitting GOT for all accesses to all extern objects with default
visibility.  The use of GOT is not needed in statically linked
executables, OS kernels etc.  The GOT entry just wastes space, and the
GOT access just slow down the execution in those environments.

Before -mexplicit-relocs, we used "-Wa,-mla-global-with-pcrel" to tell
the assembler not to use GOT for extern access.  But with
-mexplicit-relocs, we have to opt the logic in GCC.

The name "-mdirect-extern-access" is learnt from x86 port.

gcc/ChangeLog:

	* config/loongarch/genopts/loongarch.opt.in: Add
	-mdirect-extern-access option.
	* config/loongarch/loongarch.opt: Regenerate.
	* config/loongarch/loongarch.cc
	(loongarch_symbol_binds_local_p): Return true if
	TARGET_DIRECT_EXTERN_ACCESS.
	(loongarch_option_override_internal): Complain if
	-mdirect-extern-access is used with -fPIC or -fpic.
	* doc/invoke.texi: Document -mdirect-extern-access for
	LoongArch.

gcc/testsuite/ChangeLog:

	* gcc.target/loongarch/direct-extern-1.c: New test.
	* gcc.target/loongarch/direct-extern-2.c: New test.

Signed-off-by: Peng Fan <fanpeng@loongson.cn>
Signed-off-by: ticat_fp <fanpeng@loongson.cn>
---
 gcc/config/loongarch/genopts/loongarch.opt.in     |  4 ++++
 gcc/config/loongarch/loongarch.cc                 |  6 ++++++
 gcc/config/loongarch/loongarch.opt                |  4 ++++
 gcc/doc/invoke.texi                               | 15 +++++++++++++++
 .../gcc.target/loongarch/direct-extern-1.c        |  6 ++++++
 .../gcc.target/loongarch/direct-extern-2.c        |  6 ++++++
 6 files changed, 41 insertions(+)
 create mode 100644 gcc/testsuite/gcc.target/loongarch/direct-extern-1.c
 create mode 100644 gcc/testsuite/gcc.target/loongarch/direct-extern-2.c

diff --git a/gcc/config/loongarch/genopts/loongarch.opt.in b/gcc/config/loongarch/genopts/loongarch.opt.in
index ebdd9538d..e10618777 100644
--- a/gcc/config/loongarch/genopts/loongarch.opt.in
+++ b/gcc/config/loongarch/genopts/loongarch.opt.in
@@ -184,3 +184,7 @@ Enum(cmodel) String(@@STR_CMODEL_EXTREME@@) Value(CMODEL_EXTREME)
 mcmodel=
 Target RejectNegative Joined Enum(cmodel) Var(la_opt_cmodel) Init(CMODEL_NORMAL)
 Specify the code model.
+
+mdirect-extern-access
+Target Var(TARGET_DIRECT_EXTERN_ACCESS) Init(0)
+Avoid using the GOT to access external symbols.
diff --git a/gcc/config/loongarch/loongarch.cc b/gcc/config/loongarch/loongarch.cc
index 77e3a1053..c9187bf81 100644
--- a/gcc/config/loongarch/loongarch.cc
+++ b/gcc/config/loongarch/loongarch.cc
@@ -1610,6 +1610,9 @@ loongarch_weak_symbol_p (const_rtx x)
 bool
 loongarch_symbol_binds_local_p (const_rtx x)
 {
+  if (TARGET_DIRECT_EXTERN_ACCESS)
+    return true;
+
   if (SYMBOL_REF_P (x))
     return (SYMBOL_REF_DECL (x)
 	    ? targetm.binds_local_p (SYMBOL_REF_DECL (x))
@@ -6093,6 +6096,9 @@ loongarch_option_override_internal (struct gcc_options *opts)
   if (loongarch_branch_cost == 0)
     loongarch_branch_cost = loongarch_cost->branch_cost;
 
+  if (TARGET_DIRECT_EXTERN_ACCESS && flag_shlib)
+    error ("%qs cannot be used for compiling a shared library",
+	   "-mdirect-extern-access");
 
   switch (la_target.cmodel)
     {
diff --git a/gcc/config/loongarch/loongarch.opt b/gcc/config/loongarch/loongarch.opt
index 639523421..96c811c85 100644
--- a/gcc/config/loongarch/loongarch.opt
+++ b/gcc/config/loongarch/loongarch.opt
@@ -191,3 +191,7 @@ Enum(cmodel) String(extreme) Value(CMODEL_EXTREME)
 mcmodel=
 Target RejectNegative Joined Enum(cmodel) Var(la_opt_cmodel) Init(CMODEL_NORMAL)
 Specify the code model.
+
+mdirect-extern-access
+Target Var(TARGET_DIRECT_EXTERN_ACCESS) Init(0)
+Avoid using the GOT to access external symbols.
diff --git a/gcc/doc/invoke.texi b/gcc/doc/invoke.texi
index 2a5592516..9ec937b84 100644
--- a/gcc/doc/invoke.texi
+++ b/gcc/doc/invoke.texi
@@ -1007,6 +1007,7 @@ Objective-C and Objective-C++ Dialects}.
 -memcpy  -mno-memcpy -mstrict-align -mno-strict-align @gol
 -mmax-inline-memcpy-size=@var{n} @gol
 -mexplicit-relocs -mno-explicit-relocs @gol
+-mdirect-extern-access -mno-direct-extern-access @gol
 -mcmodel=@var{code-model}}
 
 @emph{M32R/D Options}
@@ -24649,6 +24650,20 @@ GCC build-time by detecting corresponding assembler support:
 @code{-mno-explicit-relocs} otherwise.  This option is mostly useful for
 debugging, or interoperation with assemblers different from the build-time
 one.
+
+@item -mdirect-extern-access
+@itemx -mno-direct-extern-access
+@opindex mdirect-extern-access
+Do not use or use GOT to access external symbols.  The default is
+@option{-mno-direct-extern-access}: GOT is used for external symbols with
+default visibility, but not used for other external symbols.
+
+With @option{-mdirect-extern-access}, GOT is not used and all external
+symbols are PC-relatively addressed.  It is @strong{only} suitable for
+environments where no dynamic link is performed, like firmwares, OS
+kernels, executables linked with @option{-static} or @option{-static-pie}.
+@option{-mdirect-extern-access} is not compatible with @option{-fPIC} or
+@option{-fpic}.
 @end table
 
 @node M32C Options
diff --git a/gcc/testsuite/gcc.target/loongarch/direct-extern-1.c b/gcc/testsuite/gcc.target/loongarch/direct-extern-1.c
new file mode 100644
index 000000000..85c6c1e8a
--- /dev/null
+++ b/gcc/testsuite/gcc.target/loongarch/direct-extern-1.c
@@ -0,0 +1,6 @@
+/* { dg-do compile } */
+/* { dg-options "-mexplicit-relocs -mdirect-extern-access" } */
+/* { dg-final { scan-assembler-not "got" } } */
+
+extern int x;
+int f() { return x; }
diff --git a/gcc/testsuite/gcc.target/loongarch/direct-extern-2.c b/gcc/testsuite/gcc.target/loongarch/direct-extern-2.c
new file mode 100644
index 000000000..58d8bd68a
--- /dev/null
+++ b/gcc/testsuite/gcc.target/loongarch/direct-extern-2.c
@@ -0,0 +1,6 @@
+/* { dg-do compile } */
+/* { dg-options "-mno-explicit-relocs -mdirect-extern-access" } */
+/* { dg-final { scan-assembler-not "la.global" } } */
+
+extern int x;
+int f() { return x; }
-- 
2.33.0

