From 48f0d47eb6dc2c799c845a25cfabd586bd176378 Mon Sep 17 00:00:00 2001
From: chenxiaolong <chenxiaolong@loongson.cn>
Date: Tue, 5 Dec 2023 14:44:35 +0800
Subject: [PATCH 082/188] LoongArch: Add asm modifiers to the LSX and LASX
 directives in the doc.

gcc/ChangeLog:

	* doc/extend.texi:Add modifiers to the vector of asm in the doc.
	* doc/md.texi:Refine the description of the modifier 'f' in the doc.
---
 gcc/doc/extend.texi | 46 +++++++++++++++++++++++++++++++++++++++++++++
 gcc/doc/md.texi     |  2 +-
 2 files changed, 47 insertions(+), 1 deletion(-)

diff --git a/gcc/doc/extend.texi b/gcc/doc/extend.texi
index c793c9c5d..bcb9329c2 100644
--- a/gcc/doc/extend.texi
+++ b/gcc/doc/extend.texi
@@ -11424,10 +11424,56 @@ The list below describes the supported modifiers and their effects for LoongArch
 @item @code{d} @tab Same as @code{c}.
 @item @code{i} @tab Print the character ''@code{i}'' if the operand is not a register.
 @item @code{m} @tab Same as @code{c}, but the printed value is @code{operand - 1}.
+@item @code{u} @tab Print a LASX register.
+@item @code{w} @tab Print a LSX register.
 @item @code{X} @tab Print a constant integer operand in hexadecimal.
 @item @code{z} @tab Print the operand in its unmodified form, followed by a comma.
 @end multitable
 
+References to input and output operands in the assembler template of extended
+asm statements can use modifiers to affect the way the operands are formatted
+in the code output to the assembler.  For example, the following code uses the
+'w' modifier for LoongArch:
+
+@example
+test-asm.c:
+
+#include <lsxintrin.h>
+
+__m128i foo (void)
+@{
+__m128i  a,b,c;
+__asm__ ("vadd.d %w0,%w1,%w2\n\t"
+   :"=f" (c)
+   :"f" (a),"f" (b));
+
+return c;
+@}
+
+@end example
+
+@noindent
+The compile command for the test case is as follows:
+
+@example
+gcc test-asm.c -mlsx -S -o test-asm.s
+@end example
+
+@noindent
+The assembly statement produces the following assembly code:
+
+@example
+vadd.d $vr0,$vr0,$vr1
+@end example
+
+This is a 128-bit vector addition instruction, @code{c} (referred to in the
+template string as %0) is the output, and @code{a} (%1) and @code{b} (%2) are
+the inputs.  @code{__m128i} is a vector data type defined in the  file
+@code{lsxintrin.h} (@xref{LoongArch SX Vector Intrinsics}).  The symbol '=f'
+represents a constraint using a floating-point register as an output type, and
+the 'f' in the input operand represents a constraint using a floating-point
+register operand, which can refer to the definition of a constraint
+(@xref{Constraints}) in gcc.
 
 @lowersections
 @include md.texi
diff --git a/gcc/doc/md.texi b/gcc/doc/md.texi
index b58da0787..a2e839073 100644
--- a/gcc/doc/md.texi
+++ b/gcc/doc/md.texi
@@ -2750,7 +2750,7 @@ $r1h
 @item LoongArch---@file{config/loongarch/constraints.md}
 @table @code
 @item f
-A floating-point register (if available).
+A floating-point or vector register (if available).
 @item k
 A memory operand whose address is formed by a base register and
 (optionally scaled) index register.
-- 
2.43.0

