From 764518a35e90a3e13c469275da9c3c7002fe1982 Mon Sep 17 00:00:00 2001
From: liuhongt <hongtao.liu@intel.com>
Date: Fri, 8 Sep 2023 09:22:43 +0800
Subject: [PATCH 11/28] Remove constraint modifier % for
 fcmaddcph/fmaddcph/fcmulcph since there're not commutative.

gcc/ChangeLog:

	PR target/111306
	PR target/111335
	* config/i386/sse.md (int_comm): New int_attr.
	(fma_<complexopname>_<mode><sdc_maskz_name><round_name>):
	Remove % for Complex conjugate operations since they're not
	commutative.
	(fma_<complexpairopname>_<mode>_pair): Ditto.
	(<avx512>_<complexopname>_<mode>_mask<round_name>): Ditto.
	(cmul<conj_op><mode>3): Ditto.

gcc/testsuite/ChangeLog:

	* gcc.target/i386/pr111306.c: New test.

(cherry picked from commit f197392a16ffb1327f1d12ff8ff05f9295e015cb)
---
 gcc/config/i386/sse.md                   | 16 ++++++++---
 gcc/testsuite/gcc.target/i386/pr111306.c | 36 ++++++++++++++++++++++++
 2 files changed, 48 insertions(+), 4 deletions(-)
 create mode 100644 gcc/testsuite/gcc.target/i386/pr111306.c

diff --git a/gcc/config/i386/sse.md b/gcc/config/i386/sse.md
index 3af159896..f25dd5f2b 100644
--- a/gcc/config/i386/sse.md
+++ b/gcc/config/i386/sse.md
@@ -6318,6 +6318,14 @@
 	[(UNSPEC_COMPLEX_FMA_PAIR "fmaddc")
 	 (UNSPEC_COMPLEX_FCMA_PAIR "fcmaddc")])
 
+(define_int_attr int_comm
+	[(UNSPEC_COMPLEX_FMA "")
+	 (UNSPEC_COMPLEX_FMA_PAIR "")
+	 (UNSPEC_COMPLEX_FCMA "")
+	 (UNSPEC_COMPLEX_FCMA_PAIR "")
+	 (UNSPEC_COMPLEX_FMUL "%")
+	 (UNSPEC_COMPLEX_FCMUL "")])
+
 (define_int_attr conj_op
 	[(UNSPEC_COMPLEX_FMA "")
 	 (UNSPEC_COMPLEX_FCMA "_conj")
@@ -6431,7 +6439,7 @@
 (define_insn "fma_<complexopname>_<mode><sdc_maskz_name><round_name>"
   [(set (match_operand:VF_AVX512FP16VL 0 "register_operand" "=&v")
 	(unspec:VF_AVX512FP16VL
-	  [(match_operand:VF_AVX512FP16VL 1 "<round_nimm_predicate>" "%v")
+	  [(match_operand:VF_AVX512FP16VL 1 "<round_nimm_predicate>" "<int_comm>v")
 	   (match_operand:VF_AVX512FP16VL 2 "<round_nimm_predicate>" "<round_constraint>")
 	   (match_operand:VF_AVX512FP16VL 3 "<round_nimm_predicate>" "0")]
 	   UNSPEC_COMPLEX_F_C_MA))]
@@ -6495,7 +6503,7 @@
 (define_insn "fma_<complexpairopname>_<mode>_pair"
  [(set (match_operand:VF1_AVX512VL 0 "register_operand" "=&v")
        (unspec:VF1_AVX512VL
-	 [(match_operand:VF1_AVX512VL 1 "vector_operand" "%v")
+	 [(match_operand:VF1_AVX512VL 1 "vector_operand" "<int_comm>v")
 	  (match_operand:VF1_AVX512VL 2 "bcst_vector_operand" "vmBr")
 	  (match_operand:VF1_AVX512VL 3 "vector_operand" "0")]
 	  UNSPEC_COMPLEX_F_C_MA_PAIR))]
@@ -6562,7 +6570,7 @@
   [(set (match_operand:VF_AVX512FP16VL 0 "register_operand" "=&v")
 	(vec_merge:VF_AVX512FP16VL
 	  (unspec:VF_AVX512FP16VL
-	    [(match_operand:VF_AVX512FP16VL 1 "nonimmediate_operand" "%v")
+	    [(match_operand:VF_AVX512FP16VL 1 "nonimmediate_operand" "<int_comm>v")
 	     (match_operand:VF_AVX512FP16VL 2 "nonimmediate_operand" "<round_constraint>")
 	     (match_operand:VF_AVX512FP16VL 3 "register_operand" "0")]
 	     UNSPEC_COMPLEX_F_C_MA)
@@ -6586,7 +6594,7 @@
 (define_insn "<avx512>_<complexopname>_<mode><maskc_name><round_name>"
   [(set (match_operand:VF_AVX512FP16VL 0 "register_operand" "=&v")
 	  (unspec:VF_AVX512FP16VL
-	    [(match_operand:VF_AVX512FP16VL 1 "nonimmediate_operand" "%v")
+	    [(match_operand:VF_AVX512FP16VL 1 "nonimmediate_operand" "<int_comm>v")
 	     (match_operand:VF_AVX512FP16VL 2 "nonimmediate_operand" "<round_constraint>")]
 	     UNSPEC_COMPLEX_F_C_MUL))]
   "TARGET_AVX512FP16 && <round_mode512bit_condition>"
diff --git a/gcc/testsuite/gcc.target/i386/pr111306.c b/gcc/testsuite/gcc.target/i386/pr111306.c
new file mode 100644
index 000000000..541725ebd
--- /dev/null
+++ b/gcc/testsuite/gcc.target/i386/pr111306.c
@@ -0,0 +1,36 @@
+/* { dg-do run } */
+/* { dg-options "-O2 -mavx512fp16 -mavx512vl" } */
+/* { dg-require-effective-target avx512fp16 } */
+
+#define AVX512FP16
+#include "avx512f-helper.h"
+
+__attribute__((optimize("O2"),noipa))
+void func1(_Float16 *a, _Float16 *b, int n, _Float16 *c) {
+  __m512h rA = _mm512_loadu_ph(a);
+  for (int i = 0; i < n; i += 32) {
+    __m512h rB = _mm512_loadu_ph(b + i);
+    _mm512_storeu_ph(c + i, _mm512_fcmul_pch(rB, rA));
+  }
+}
+
+void
+test_512 (void)
+{
+  int n = 32;
+  _Float16 a[n], b[n], c[n];
+  _Float16 exp[n];
+  for (int i = 1; i <= n; i++) {
+    a[i - 1] = i & 1 ? -i : i;
+    b[i - 1] = i;
+  }
+
+  func1(a, b, n, c);
+  for (int i = 0; i < n / 32; i += 2) {
+    if (c[i] != a[i] * b[i] + a[i+1] * b[i+1]
+	|| c[i+1] != a[i] * b[i+1] - a[i+1]*b[i])
+      __builtin_abort ();
+    }
+}
+
+
-- 
2.31.1

