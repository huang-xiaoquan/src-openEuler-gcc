From a23da8527f517e439ab634d9995b44740cbbc05b Mon Sep 17 00:00:00 2001
From: "Hu, Lin1" <lin1.hu@intel.com>
Date: Wed, 6 Nov 2024 15:42:13 +0800
Subject: [PATCH 12/14] i386: Zero extend 32-bit address to 64-bit with option
 -mx32 -maddress-mode=long. [PR 117418]

-maddress-mode=long let Pmode = DI_mode, so zero extend 32-bit address to
64-bit and uses a 64-bit register as a pointer for avoid raise an ICE.

gcc/ChangeLog:

	PR target/117418
	* config/i386/i386-expand.cc (ix86_expand_builtin): Convert
	pointer's mode according to Pmode.

gcc/testsuite/ChangeLog:

	PR target/117418
	* gcc.target/i386/pr117418-1.c: New test.

(cherry picked from commit 2272cd2508f1854c880082f792de15e76ec09a99)
---
 gcc/config/i386/i386-expand.cc             | 12 +++++++++++
 gcc/testsuite/gcc.target/i386/pr117418-1.c | 24 ++++++++++++++++++++++
 2 files changed, 36 insertions(+)
 create mode 100644 gcc/testsuite/gcc.target/i386/pr117418-1.c

diff --git a/gcc/config/i386/i386-expand.cc b/gcc/config/i386/i386-expand.cc
index bc2e6198007..52e32749928 100644
--- a/gcc/config/i386/i386-expand.cc
+++ b/gcc/config/i386/i386-expand.cc
@@ -12730,6 +12730,9 @@ ix86_expand_builtin (tree exp, rtx target, rtx subtarget,
       op1 = expand_normal (arg1);
       op2 = expand_normal (arg2);
 
+      if (GET_MODE (op1) != Pmode)
+	op1 = convert_to_mode (Pmode, op1, 1);
+
       if (!address_operand (op2, VOIDmode))
 	{
 	  op2 = convert_memory_address (Pmode, op2);
@@ -12765,6 +12768,9 @@ ix86_expand_builtin (tree exp, rtx target, rtx subtarget,
       emit_label (ok_label);
       emit_insn (gen_rtx_SET (target, pat));
 
+      if (GET_MODE (op0) != Pmode)
+	op0 = convert_to_mode (Pmode, op0, 1);
+
       for (i = 0; i < 8; i++)
 	{
 	  op = gen_rtx_MEM (V2DImode,
@@ -12789,6 +12795,9 @@ ix86_expand_builtin (tree exp, rtx target, rtx subtarget,
 	if (!REG_P (op0))
 	  op0 = copy_to_mode_reg (SImode, op0);
 
+	if (GET_MODE (op2) != Pmode)
+	  op2 = convert_to_mode (Pmode, op2, 1);
+
 	op = gen_rtx_REG (V2DImode, GET_SSE_REGNO (0));
 	emit_move_insn (op, op1);
 
@@ -12826,6 +12835,9 @@ ix86_expand_builtin (tree exp, rtx target, rtx subtarget,
 	if (!REG_P (op0))
 	  op0 = copy_to_mode_reg (SImode, op0);
 
+	if (GET_MODE (op3) != Pmode)
+	  op3 = convert_to_mode (Pmode, op3, 1);
+
 	/* Force to use xmm0, xmm1 for keylow, keyhi*/
 	op = gen_rtx_REG (V2DImode, GET_SSE_REGNO (0));
 	emit_move_insn (op, op1);
diff --git a/gcc/testsuite/gcc.target/i386/pr117418-1.c b/gcc/testsuite/gcc.target/i386/pr117418-1.c
new file mode 100644
index 00000000000..4839b139b79
--- /dev/null
+++ b/gcc/testsuite/gcc.target/i386/pr117418-1.c
@@ -0,0 +1,24 @@
+/* PR target/117418 */
+/* { dg-do compile { target { ! ia32 } } } */
+/* { dg-options "-maddress-mode=long -mwidekl -mx32" } */
+/* { dg-require-effective-target maybe_x32  } */
+/* { dg-final { scan-assembler-times "aesdec128kl" 1 } } */
+/* { dg-final { scan-assembler-times "aesdec256kl" 1 } } */
+/* { dg-final { scan-assembler-times "aesenc128kl" 1 } } */
+/* { dg-final { scan-assembler-times "aesenc256kl" 1 } } */
+/* { dg-final { scan-assembler-times "encodekey128" 1 } } */
+/* { dg-final { scan-assembler-times "encodekey256" 1 } } */
+
+typedef __attribute__((__vector_size__(16))) long long V;
+V a;
+
+void
+foo()
+{
+    __builtin_ia32_aesdec128kl_u8 (&a, a, &a);
+    __builtin_ia32_aesdec256kl_u8 (&a, a, &a);
+    __builtin_ia32_aesenc128kl_u8 (&a, a, &a);
+    __builtin_ia32_aesenc256kl_u8 (&a, a, &a);
+    __builtin_ia32_encodekey128_u32 (0, a, &a); 
+    __builtin_ia32_encodekey256_u32 (0, a, a, &a); 
+}
-- 
2.31.1

