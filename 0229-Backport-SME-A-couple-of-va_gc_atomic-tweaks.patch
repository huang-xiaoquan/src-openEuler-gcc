From 12dd36f06e13ee9cd684c00732caa684f49b3610 Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Thu, 29 Jun 2023 08:48:17 +0100
Subject: [PATCH 130/157] [Backport][SME] A couple of va_gc_atomic tweaks
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=4e9f6c14280699997a633cefd3fb315b2bd4762c

The only current user of va_gc_atomic is Ada's:

    vec<Entity_Id, va_gc_atomic>

It uses the generic gt_pch_nx routines (with gt_pch_nx being the
“note pointers” hooks), such as:

    template<typename T, typename A>
    void
    gt_pch_nx (vec<T, A, vl_embed> *v)
    {
      extern void gt_pch_nx (T &);
      for (unsigned i = 0; i < v->length (); i++)
	gt_pch_nx ((*v)[i]);
    }

It then defines gt_pch_nx routines for Entity_Id &.

The problem is that if we wanted to take the same approach for
an array of unsigned ints, we'd need to define:

    inline void gt_pch_nx (unsigned int &) { }

which would then be ambiguous with:

    inline void gt_pch_nx (unsigned int) { }

The point of va_gc_atomic is that the elements don't need to be GCed,
and so we have:

    template<typename T>
    void
    gt_ggc_mx (vec<T, va_gc_atomic, vl_embed> *v ATTRIBUTE_UNUSED)
    {
      /* Nothing to do.  Vectors of atomic types wrt GC do not need to
	 be traversed.  */
    }

I think it's therefore reasonable to assume that no pointers will
need to be processed for PCH either.

The patch also relaxes the array_slice constructor for vec<T, va_gc> *
so that it handles all embedded vectors.

gcc/
	* vec.h (gt_pch_nx): Add overloads for va_gc_atomic.
	(array_slice): Relax va_gc constructor to handle all vectors
	with a vl_embed layout.

gcc/ada/
	* gcc-interface/decl.cc (gt_pch_nx): Remove overloads for Entity_Id.
---
 gcc/ada/gcc-interface/decl.cc | 11 -----------
 gcc/vec.h                     | 22 ++++++++++++++++++----
 2 files changed, 18 insertions(+), 15 deletions(-)

diff --git a/gcc/ada/gcc-interface/decl.cc b/gcc/ada/gcc-interface/decl.cc
index 1c7a71684..7193b55c7 100644
--- a/gcc/ada/gcc-interface/decl.cc
+++ b/gcc/ada/gcc-interface/decl.cc
@@ -163,17 +163,6 @@ struct GTY((for_user)) tree_entity_vec_map
   vec<Entity_Id, va_gc_atomic> *to;
 };
 
-void
-gt_pch_nx (Entity_Id &)
-{
-}
-
-void
-gt_pch_nx (Entity_Id *x, gt_pointer_operator op, void *cookie)
-{
-  op (x, NULL, cookie);
-}
-
 struct dummy_type_hasher : ggc_cache_ptr_hash<tree_entity_vec_map>
 {
   static inline hashval_t
diff --git a/gcc/vec.h b/gcc/vec.h
index fc3b10c85..592d3f7e0 100644
--- a/gcc/vec.h
+++ b/gcc/vec.h
@@ -1383,6 +1383,13 @@ gt_pch_nx (vec<T, A, vl_embed> *v)
     gt_pch_nx ((*v)[i]);
 }
 
+template<typename T>
+void
+gt_pch_nx (vec<T, va_gc_atomic, vl_embed> *)
+{
+  /* No pointers to note.  */
+}
+
 template<typename T, typename A>
 void
 gt_pch_nx (vec<T *, A, vl_embed> *v, gt_pointer_operator op, void *cookie)
@@ -1400,6 +1407,13 @@ gt_pch_nx (vec<T, A, vl_embed> *v, gt_pointer_operator op, void *cookie)
     gt_pch_nx (&((*v)[i]), op, cookie);
 }
 
+template<typename T>
+void
+gt_pch_nx (vec<T, va_gc_atomic, vl_embed> *, gt_pointer_operator, void *)
+{
+  /* No pointers to note.  */
+}
+
 
 /* Space efficient vector.  These vectors can grow dynamically and are
    allocated together with their control data.  They are suited to be
@@ -2268,12 +2282,12 @@ public:
   array_slice (vec<OtherT> &v)
     : m_base (v.address ()), m_size (v.length ()) {}
 
-  template<typename OtherT>
-  array_slice (const vec<OtherT, va_gc> *v)
+  template<typename OtherT, typename A>
+  array_slice (const vec<OtherT, A, vl_embed> *v)
     : m_base (v ? v->address () : nullptr), m_size (v ? v->length () : 0) {}
 
-  template<typename OtherT>
-  array_slice (vec<OtherT, va_gc> *v)
+  template<typename OtherT, typename A>
+  array_slice (vec<OtherT, A, vl_embed> *v)
     : m_base (v ? v->address () : nullptr), m_size (v ? v->length () : 0) {}
 
   iterator begin () { return m_base; }
-- 
2.33.0

